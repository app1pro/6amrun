/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import * as React from 'react';
import { AppState, Alert } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import AuthStack from './screens/AuthStack';
import AppTabStack from './bottomtabs';
import SplashScreen from './components/SplashScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { USER_SAVE_KEY, STORAGE_KEY, NOTIF_TOKEN_KEY } from '../app.json';
import { AuthContext } from './utils/authContext';
import store from './utils/onlineReducer';
import { Host } from 'react-native-portalize';
import { Provider } from 'react-redux';
import NotifService from './utils/NotifService';

// const AuthContext = React.createContext();

const RootStack = createStackNavigator();

function App ({ navigation })
{
    const [state, dispatch] = React.useReducer(
        (prevState, action) => {
            switch (action.type) {
                case 'RESTORE_TOKEN':
                return {
                    ...prevState,
                    userToken: action.token,
                    isLoading: false,
                };
                case 'SIGN_IN':
                return {
                    ...prevState,
                    isSignout: false,
                    userToken: action.token,
                };
                case 'SIGN_OUT':
                return {
                    ...prevState,
                    isSignout: true,
                    userToken: null,
                };
            }
        },
        {
            isLoading: true,
            isSignout: false,
            userToken: null,
        }
    );

    const appState = React.useRef(AppState.currentState);

    const onRegister = (token) => {
        // this.setState({registerToken: token.token, fcmRegistered: true});
        AsyncStorage.setItem(NOTIF_TOKEN_KEY, JSON.stringify(token));
    }
    
    const onNotif = (notif) => {
        Alert.alert(notif.title, notif.message);
    }
    
    React.useEffect(() => {
        // Fetch the token from storage then navigate to our appropriate place
        const bootstrapAsync = async () => {
            let userToken;

            try {
                userToken = await AsyncStorage.getItem(USER_SAVE_KEY);
            } catch (e) {
                // Restoring token failed
            }

            // After restoring token, we may need to validate it in production apps

            // This will switch to the App screen or Auth screen and this loading
            // screen will be unmounted and thrown away.
            dispatch({ type: 'RESTORE_TOKEN', token: userToken });
        };

        bootstrapAsync();

        const notif = new NotifService(
            onRegister.bind(this),
            onNotif.bind(this),
        );

        this.subscription = AppState.addEventListener("change", _handleAppStateChange);

        return () => {
            this.subscription.remove();
        };
    }, []);

    const _handleAppStateChange = (nextAppState) => {
        const { currentUser, onlineUsers } = store.getState();

        if ( appState.current.match(/inactive|background/) && nextAppState === "active" ) {
            // console.log("App has come to the foreground!");
            // if (currentUser && currentUser.shopify_cid) {
            //     store.dispatch({
            //         type: 'EMIT_USER', 
            //         user_id: currentUser.shopify_cid, 
            //         group_id: currentUser.id_shop
            //     });
            // }

            try {
                AsyncStorage.getItem(STORAGE_KEY)
                .then((user_data_json) => {
                    let { loggedUser } = JSON.parse(user_data_json) || {};
        
                    if (loggedUser && loggedUser.shopify_cid) {
                        store.dispatch({ type: 'ADD_CURRENT_USER', data: loggedUser });
                        store.dispatch({
                            type: 'EMIT_USER', 
                            user_id: loggedUser.shopify_cid, 
                            group_id: loggedUser.id_shop
                        });
                    }
                });
            } catch (e) {
                // Restoring token failed
            }
        }
    
        appState.current = nextAppState;
        console.log("AppState", appState.current);
    };

    const authContext = React.useMemo(
        () => ({
            signIn: async data => {
                // In a production app, we need to send some data (usually username, password) to server and get a token
                // We will also need to handle errors if sign in failed
                // After getting token, we need to persist the token using `AsyncStorage`
                // In the example, we'll use a dummy token

                dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
            },
            signOut: () => store.dispatch({ type: 'DISCONECT' }) && dispatch({ type: 'SIGN_OUT' }),
            signUp: async data => {
                // In a production app, we need to send user data to server and get a token
                // We will also need to handle errors if sign up failed
                // After getting token, we need to persist the token using `AsyncStorage`
                // In the example, we'll use a dummy token

                dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
            },
        }),
        []
    );

    return (
        <AuthContext.Provider value={authContext}>
        <Provider store={store}>
        <Host>
          <NavigationContainer>
                <RootStack.Navigator headerMode="none">
                {state.isLoading ? (
                    // We haven't finished checking for the token yet
                    <RootStack.Screen name="Splash" component={SplashScreen} />
                ) : state.userToken == null ? (
                    // No token found, user isn't signed in
                    <RootStack.Screen
                    name="SignIn"
                    component={AuthStack}
                    options={{
                        title: 'Sign in',
                    // When logging out, a pop animation feels intuitive
                        animationTypeForReplace: state.isSignout ? 'pop' : 'push',
                    }}
                    />
                ) : (
                    // User is signed in
                    <RootStack.Screen name="HomeStack" component={AppTabStack} />
                )}
                </RootStack.Navigator>
          </NavigationContainer>
        </Host>
        </Provider>
        </AuthContext.Provider>
    );
};

export default App;