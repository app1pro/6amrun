
import React from "react";
import { View, Text, Image, Alert, StyleSheet, TouchableOpacity } from "react-native";
import UserAvatar from "react-native-user-avatar";
import Moment from 'react-moment';
import HTML from 'react-native-render-html';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ImageView from 'react-native-image-viewing';
import clip from "text-clipper";
import MyReplyItem from './MyReplyItem';
import UserBadges from "../../components/UserBadges";
import OptionsComment from "../../components/OptionsComment";
import { getMediumThumb, getSmallThumb } from "../../utils/global";

export default class MyCommentItem extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isLoadMoreRepliesShow: true,
            imageCurrentIndex: 0,
            isImageViewVisible: false,
            isContentCollapsed: false,
            replyPage: 1,
        };
        this.showImage = this.showImage.bind(this);
        this.setModalViewer = this.setModalViewer.bind(this);
        this._renderCommentContent = this._renderCommentContent.bind(this);
    }

    showImage(image) {
        this.setState({
            imageCurrentIndex: this.props.images.findIndex(_item => _item.id == image.id),
            isImageViewVisible: true,
        })
    }

    setModalViewer(visible) {
        this.setState({isImageViewVisible: visible});
    }

    _onPressUser = () => {
        this.props.onPressUser(this.props.author);
    }

    _onPressReply = () => {
        this.props.onPressReply({parent_id: this.props.id, mention: this.props.author, name: this.props.author.name});
    }

    _onPressSetContentCollapsed = () => {
        this.setState((previousState) => ({
            isContentCollapsed: !previousState.isContentCollapsed
        }))
    }

    _renderCommentContent = (_content) => {
        const maxCharacters = 180;
        let clippedHtml =  clip( _content, maxCharacters, { html: true, maxLines: 3, indicator: '... (Read more)'} )

        if (this.state.isContentCollapsed || _content.length < maxCharacters) {
            return (
                <HTML
                    source={{ html: _content }}
                    emSize={15}
                    baseFontStyle={{ fontSize: 15 }}
                    tagsStyles={{
                        p: { marginBottom: 0 }
                    }}
                    parentWrapper={View}
                />
            )
        }
        return (
            <TouchableOpacity
                onPress={() => this._onPressSetContentCollapsed()}
                keyboardShouldPersistTaps={true}
            >
                <HTML
                    source={{ html: clippedHtml }}
                    emSize={15}
                    baseFontStyle={{ fontSize: 15 }}
                    tagsStyles={{
                        p: { marginBottom: 0 }
                    }}
                    parentWrapper={View}
                />
            </TouchableOpacity>
        )
    }

    _onPressLoadMoreReply = () => {
        this.props.onPressLoadMoreReply(this.props.id, this.state.replyPage);

        this.setState(prevState => ({
            replyPage: prevState.replyPage + 1,
        }));
    }

    render() {
        const isLikeButtonClicked = this.props.loggedUser !== undefined && this.props.likes.findIndex(_item => _item.user_id == this.props.loggedUser.id) > -1;
        let replyCount = this.props.replies_count - this.props.replies.length;
        let imagesViewer;

        if (this.props.images !== undefined && this.props.images.length > 0) {
            imagesViewer = this.props.images.map(_image => ({
                id: _image.id,
                uri: getMediumThumb(_image.url),
            }))
        }

        return this.props.id ? (
            <View style={styles.row}>
                <View style={styles.panelLeft}>
                    {this.props.author && (this.props.author.name || this.props.author.avatar) ? (
                        <TouchableOpacity onPress={this._onPressUser} >
                            <UserAvatar size={40} name={this.props.author.name} src={getSmallThumb(this.props.author.avatar)} />
                        </TouchableOpacity>
                    ) : null}
                </View>
                <View style={styles.panelRight}>
                    <View style={styles.itemDetails}>
                        <UserBadges badgeList={this.props.badgeList} badgeIds={this.props.author.badge_id} />
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity onPress={this._onPressUser} style={{marginRight: 5}}>
                                <Text style={[styles.itemName]}>{this.props.author.name}</Text>
                            </TouchableOpacity>
                            {this.props.author.is_staff && (
                                <View style={styles.wrapStaffBadge}>
                                    <Text style={styles.staffText}>{this.props.settingsForum.staff_text}</Text>
                                </View>
                            )}
                        </View>
                        <View>{this._renderCommentContent(this.props.content)}</View>
                        {this.props.images.length ? (
                            <View style={{ marginTop: 16 }}>
                                {
                                    this.props.images.map((image, index) => (
                                        <TouchableOpacity onPress={() => this.showImage(image)} key={'image_' + image.id}>
                                            <Image source={{ uri: image.url }} style={styles.commentImage} />
                                        </TouchableOpacity>
                                    ))
                                }
                                <ImageView
                                    images={imagesViewer}
                                    imageIndex={this.state.imageCurrentIndex}
                                    animationType={'slide'}
                                    visible={this.state.isImageViewVisible}
                                    onRequestClose={() => this.setModalViewer(false)}
                                />
                            </View>
                        ) : null}
                    </View>

                    <View style={styles.rightWidget}>
                        <OptionsComment {...this.props} />
                    </View>

                    <View style={styles.itemFooter}>
                        <TouchableOpacity onPress={() => this.props.onPressPostLike(this.props.id)} >
                            <Text style={[ styles.likeButton, {color: isLikeButtonClicked ? '#09af00' : '#007bff'}]}>Like</Text>
                        </TouchableOpacity>
                        <Text style={styles.bullet}>•</Text>
                        <TouchableOpacity onPress={() => this._onPressReply(this.props.name)} >
                            <Text style={styles.replyButton}>Reply</Text>
                        </TouchableOpacity>
                        <Text style={styles.bullet}>•</Text>
                        <Moment fromNow ago element={Text} style={[styles.itemTime]}>{this.props.created}</Moment>
                        {this.props.like_count ? (
                            <View style={styles.likeWrapper}>
                                <View style={styles.likeIcon}>
                                    <View style={styles.likeBubble}>
                                        <FontAwesome5 name="thumbs-up" solid size={10} color="#fff" />
                                    </View>
                                    <Text>{this.props.like_count}</Text>
                                </View>
                            </View>
                        ) : null}
                    </View>

                    {this.props.replies.length ? (
                        <View style={{ marginTop: 5 }}>
                            {this.props.replies.map(item => (
                                <MyReplyItem
                                    key={'comment_' + item.id}
                                    id={item.id}
                                    parent_id={this.props.id}
                                    content={item.content}
                                    created={item.created}
                                    author={item.author}
                                    images={item.images || []}
                                    likes={item.likes || []}
                                    like_count={item.like_count}
                                    like_shopify_cid={item.like_shopify_cid}
                                    loggedUser={this.props.loggedUser}
                                    shop={this.props.shop}
                                    badgeList={this.props.badgeList}
                                    settingsForum={this.props.settingsForum}
                                    onPressUser={this.props.onPressUser}
                                    onPressPostLike={this.props.onPressPostLike}
                                    onPressReply={this.props.onPressReply}
                                    editComment={this.props.editComment}
                                    deleteComment={this.props.deleteComment}
                                    accessedForum={this.props.accessedForum}
                                    accessToken={this.props.accessToken}
                                    openSendReport={this.props.openSendReport}
                                />
                            ))}
                        </View>
                    ) : null}

                    {replyCount > 0 ? (
                        <View>
                            <TouchableOpacity onPress={this._onPressLoadMoreReply}>
                                <View style={styles.loadMore}>
                                    <Text style={{ color: '#007bff', fontSize: 15 }}>{replyCount > 1 ? `Load more ${replyCount} replies` : `Load more ${replyCount} reply`}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    ) : null}

                </View>
            </View>
        ) : null;
    }
}

const styles = StyleSheet.create({
    row: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 8,
        paddingHorizontal: 10,
        marginBottom: 5,
    },
    panelLeft: {
        height: 45,
        width: 45,
        paddingRight: 5,
    },
    panelRight: {
        flex: 1,
        flexDirection: 'column',
    },
    wrapStaffBadge: {
        borderRadius: 4,
        backgroundColor: '#dce0e2',
        alignItems: 'center'
    },
    staffText: {
        fontSize: 12,
        color: '#5d707f',
        paddingHorizontal: 7,
        paddingVertical: 2
    },
    rightWidget: {
        position: 'absolute',
        top: 0,
        right: 0,
        zIndex: 99,
    },
    itemDetails: {
        flex: 1,
        flexDirection: 'column',
        paddingTop: 8,
        paddingBottom: 10,
        paddingHorizontal: 8,
        backgroundColor: 'rgba(0,0,0,.07)',
        borderRadius: 8,
    },
    itemName: {
        color: '#007bff',
        fontSize: 14,
        paddingBottom: 3,
        fontWeight: 'bold',
    },
    itemContent: {
        fontSize: 15,
    },
    itemTime: {
        fontSize: 15,
        color: '#999',
    },
    iconTrash: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    likeButton: {
        fontSize: 15
    },
    replyButton: {
        color: '#007bff',
        fontSize: 15
    },
    bullet: {
        fontSize: 15,
        color: '#939393',
        paddingHorizontal: 5
    },
    loadMore: {
        padding: 10,
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
    },
    itemFooter: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 5,
        paddingHorizontal: 5
    },
    likeWrapper: {
        flex: 1,
        alignItems: 'flex-end',
        textAlign: 'right',
        marginTop: -10,
    },
    likeIcon: {
        flexDirection: 'row',
        paddingVertical: 3,
        paddingHorizontal: 5,
        borderWidth: 1,
        borderColor: '#e0e0e0',
        borderRadius: 50,
        backgroundColor: "#fff",
    },
    likeBubble: {
        backgroundColor: "#007bff",
        paddingVertical: 4,
        paddingHorizontal: 5,
        marginRight: 5,
        borderRadius: 50,
        height: 20,
    },
    commentImage : {
        width: 180,
        height: 101,
    }
});

