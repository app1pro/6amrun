
import React from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import UserAvatar from "react-native-user-avatar";
import { getSmallThumb } from "../../utils/global";

const styles = StyleSheet.create({
    header: {
        padding: 10,
    },
    newPost: {
        flex: 1,
        flexDirection: 'row',
        paddingHorizontal: 12,
        paddingVertical: 10,
        borderRadius: 8,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    panelLeft: {
        height: 36,
        width: 36,
        paddingTop: 0,
    },
    panelRight: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 15,
    },
    itemDetails: {
        paddingVertical: 8,
    },
    itemInputText: {
        fontSize: 15,
        color: '#999',
    },
});

export default class StoriesHeader extends React.PureComponent {
    _onPress = () => {
        this.props.onPressAdd();
    }

    render() {
        return (
            <View style={styles.header}>
                <TouchableOpacity onPress={this._onPress}>
                    <View style={styles.newPost}>
                        <View style={styles.panelLeft}>
                            {this.props.authenUser.name && (
                                <UserAvatar size={36} name={this.props.authenUser.name} src={getSmallThumb(this.props.authenUser.avatar)} />
                            )}
                        </View>
                        <View style={styles.panelRight}>
                            <View style={styles.itemDetails}>
                                <Text style={[styles.itemInputText]}>How are you today?</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

