import React from "react";
import {Platform, View, Text, Alert, FlatList, RefreshControl, TouchableOpacity } from "react-native";
import {name as APP_NAME, STORAGE_KEY, USER_SAVE_KEY, BASE_URL_API} from '../../../app.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../../components/Loader';
import StoryList from '../../components/StoryList';
import UserProfilesHeader from '../../components/UserProfilesHeader';
import ModalAddMessage from "../../components/ModalAddMessage";
import ModalReport from '../../components/ModalReport';
// import EntypoIcon from 'react-native-vector-icons/Entypo';
import axios from 'axios';
// import moment from "moment";
import runes from "runes";

export default class UserProfiles extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.state.params.title || 'Stories',
            /*
            headerRight: (
                <TouchableOpacity onPress={navigation.getParam('onPressAdd')}>
                    <View style={{ padding: 10 }} >
                        <EntypoIcon name="new-message" size={20} color="#0066ff" />
                    </View>
                </TouchableOpacity>
            ),*/
        }
    };

    constructor(props){
        super(props);
        this.state ={
            refreshing: false,
            isLoading: true,
            isLoadingMore: false,
            userId: null,
            accessToken: null,
            loggedUser: {},
            currentUser: {},
            blockedUsers: [],
            settingsForum: {},
            dataList: [],
            badges: [],
            page: 1,
            modalVisible: false,
            modalReportVisible: false,
            report_object_id: null,
            report_type: null,
            isBlocked: false,
            showCategory: null
        }
    }

    componentDidMount() {
        const { userId } = this.props.route.params;
        this.setState({ userId })

        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData && this.userData.customer) {
                    // console.log('AsyncStorage', this.userData);
                    let isBlocked = this.userData.blockedUsers
                    && this.userData.blockedUsers.length
                    && this.userData.blockedUsers.filter(data => 
                        (data.user_id === this.userData.loggedUser && data.entity_id === this.state.userId)
                        || (data.user_id === this.state.userId && data.entity_id === this.userData.loggedUser)
                    )

                    this.setState({
                        isLoading: false,
                        accessedForum: this.userData.customer,
                        loggedUser: this.userData.loggedUser,
                        badges: this.userData.badges,
                        settingsForum: this.userData.settings,
                        blockedUsers: this.userData.blockedUsers,
                        isBlocked: isBlocked,
                    });

                    this._loadUserData();
                    this._onRefresh();
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _loadUserData = () => {
        this.setState({refreshing: true});

        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.get(BASE_URL_API + '/users/'+this.state.userId+'.json', configHeaders)
        .then(res => {
            let data = res.data;
            // this.setState({ refreshing: false, isLoadingMore: false });

            if (data && data.user !== undefined) {
                this.setState({
                    currentUser: data.user,
                });
            } else {
                console.log('Load User Profiles fail', data.error);
                if (data.error !== undefined) {
                    Alert.alert(data.error.toString());
                }
            }
        })
        .catch(error => {
            console.log('nearby', error.response)
        })
    }

    _onRefresh = () => {
        this.setState({refreshing: true});

        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.get(BASE_URL_API + '/topics.json?user_id='+this.state.userId+'&page=' + this.state.page, configHeaders)
        .then(res => {
            let data = res.data;
            this.setState({ refreshing: false, isLoadingMore: false });

            if (data && data.topics !== undefined && data.topics.length) {
                this.setState(previousState => ({
                    dataList: [...previousState.dataList, ...data.topics],
                    page: previousState.page + 1,
                    showCategory: data.show_category,
                }));
            } else {
                console.log('Reload fail', data.error);
                if (data.error !== undefined) {
                    Alert.alert(data.error.toString());
                }
            }
        })
        .catch(error => {
            console.log('nearby', error.response)
        })
    }

    _loadMoreResults = () => {
        if ( this.state.isLoading || this.state.refreshing || this.state.isLoadingMore) {
            return;
        }

        this.setState({isLoadingMore: true});
        this._onRefresh();
    }

    _onPressItem = (_post) => {
        return this.props.navigation.navigate('StoryDetail', {postId: _post.id});
    }

    _subString(_string, max_length = 20) {
        if (_string === undefined || _string === null || !_string) {
            return;
        }
        if (_string.length > max_length) {
            // Runes
            return runes.substr(_string, 0, max_length)+'...(See more)'
        }
        return _string
    }

    _onPressLike(_item) {
        let {dataList} = this.state
        dataList.map(_post => {
            if (_post.id == _item.post_id) {
                _post.reactions.push(_item)
            }
            return _post
        })
        this.setState({ dataList })
    }

    /**
     * Dislike a post
     * @param {reaction element} _item
     */
    _onPressDislike(_item) {
        let {dataList} = this.state
        dataList.map(_post => {
            if (_post.id == _item.post_id) {
                _post.reactions = _post.reactions.filter(_reaction => _reaction.user_id != _item.user_id)
            }
            return _post
        })

        this.setState({ dataList })
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    setModalReportVisible(visible) {
        this.setState({modalReportVisible: visible});
    }

    openSendReport(data) {
        this.setState({
            report_object_id: data.object_id,
            report_type: data.type,
        })

        setTimeout( () => this.setModalReportVisible(true), 100)
    }

    onBlockUser() {
        let postData = {
            user_id: this.state.loggedUser.id,
            entity_id: this.state.currentUser.id,
        };

        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.post(BASE_URL_API + '/users/block.json', postData, configHeaders)
        .then(res => {
            // console.log('Sending data', res.data);
            let data = res.data;
            
            if (data && data.success === true) {
                this.setState({
                    isBlocked: true,
                });

                // Save blocked Users to storage.
                AsyncStorage.getItem(STORAGE_KEY)
                .then((user_data_json) => {
                    this.userData = JSON.parse(user_data_json);
                    this.userData.blockedUsers.push({user_id: this.state.loggedUser.id, entity_id: this.state.currentUser.id});
                    AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(this.userData));
                });
            } else {
                if (data.error !== undefined) {
                    Alert.alert('Sending data fail', data.error.toString());
                } else {
                    Alert.alert('Sending data fail!', `Something went wrong!`);
                }
            }
        })
        .catch(error => {
            console.log('onBlockUser', error.response);
        })
    }

    render() {
        if (this.state.currentUser === null || this.state.isBlocked) {
            return (
                <View style={{flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around'}}>
                    <Text style={{ color: '#999', fontWeight: 'bold' }}>This Content Isn't Available</Text>
                </View>
            );
        }

        return (
          <View style={{flex: 1}}>
            <Loader loading={this.state.isLoading} />
            <StoryList
                data={this.state.dataList}
                extraData={this.state}
                loggedUser={this.state.loggedUser}
                accessedForum={this.state.accessedForum}
                accessToken={this.state.accessToken}
                navigation={this.props.navigation}
                updateData={dataList => this.setState({ dataList }) }
                badges={this.state.badges}
                settingsForum={this.state.settingsForum}
                showCategory={this.state.showCategory}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                  />
                }
                ListEmptyComponent={!this.state.isLoading && !this.state.refreshing && (
                    <View style={{ padding: 15, alignContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: '#999' }}>No stories here</Text>
                    </View>
                )}
                onEndReachedThreshold={0.25}
                onEndReached={({ distanceFromEnd }) => {
                    // if (distanceFromEnd > 0) {
                        this._loadMoreResults();
                    // }
                }}
                ListHeaderComponent={
                    !this.state.refreshing ? (
                        <UserProfilesHeader
                            authenUser={this.state.loggedUser}
                            currentUser={this.state.currentUser}
                            settingsForum={this.state.settingsForum}
                            onBlockUser={() => this.onBlockUser()}
                            openSendReport={_item => this.openSendReport(_item)}
                            setModalVisible={_state => this.setModalVisible(_state)}
                        />
                    ) : null
                }
            />

            <ModalAddMessage
                modalVisible={this.state.modalVisible}
                setModalVisible={_state => this.setModalVisible(_state)}
                accessedForum={this.state.accessedForum}
                accessToken={this.state.accessToken}
                loggedUser={this.state.loggedUser}
                currentUser={this.state.currentUser}
            />

            <ModalReport
                modalVisible={this.state.modalReportVisible}
                setModalVisible={_state => this.setModalReportVisible(_state)}
                accessedForum={this.state.accessedForum}
                accessToken={this.state.accessToken}
                loggedUser={this.state.loggedUser}
                object_id={this.state.report_object_id}
                type={this.state.report_type}
            />

          </View>
        );
    }
}
