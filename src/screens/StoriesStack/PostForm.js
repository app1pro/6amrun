import React, {Component} from 'react';
import {
    Text, TextInput, Platform,
    Image, TouchableOpacity,
    KeyboardAvoidingView,
    ActivityIndicator,
    View, Alert, StyleSheet,
    SafeAreaView,
    ScrollView} from 'react-native';
import {BASE_URL_API, MAX_FILE_SIZE, USER_SAVE_KEY, STORAGE_KEY} from '../../../app.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationActions } from '@react-navigation/compat';
import axios from 'axios';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// import ImagePicker from 'react-native-image-picker';
import {actions, getContentCSS, RichEditor, RichToolbar} from 'react-native-new-rich-editor';
import ImagePicker from 'react-native-image-crop-picker';
import VideoPlayer from 'react-native-video-player';
import ModalUserList from '../../components/ModalUserList';
import EmbedView from '../../components/EmbedView';
import Dialog from "react-native-dialog";

const genRanHex = size => [...Array(size)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');

export default class PostForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            accessToken: null,
            accessedForum: {},
            settingsForum: {},
            loggedUser: {},
            postId: null,
            categoryId: null,
            dialogVisible: false,
            modalVisible: false,
            content: null,
            progress: 0,
            localImages: [],
            localVideos: [],
            localPoll: null,
            embeds: {},
            videoURLtmp: null,
        }
    }

    setHeaderRight = () => {
        if (!!this.state.content && this.state.content.trim() && !this.state.isLoading) {
            return (
                <TouchableOpacity onPress={() => this.submitPost()} >
                    <View style={{ paddingHorizontal: 15, paddingVertical: 5 }}>
                        <Ionicons name="md-send" size={25} color="#0066ff" />
                    </View>
                </TouchableOpacity>
            )
        } else {
            return (
                <View style={{ paddingHorizontal: 15, paddingVertical: 5 }}>
                    <Ionicons name="md-send" size={25} color="gray" />
                </View>
            )
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        let title = 'Create Post';

        this.focusListener = this.props.navigation.addListener('focus',
            () => {
                // console.log('focus is called');
                //your logic here.
                if (typeof this.props.route.params === 'object') {
                    const { categoryId, categoryName, post } = this.props.route.params;
                    this.setState({ categoryId });

                    if (categoryName) {
                        title = 'Create Post in: ' + categoryName
                    } else if (post) {
                        title = 'Edit Post'
                    }

                    if (post) {
                        this.setState({
                            postId: post.id,
                            content: post.content,
                            embeds: (post.embeds !== undefined && post.embeds.length > 0) ? post.embeds[0] : {},
                            localImages: post.images.map(item => {
                                item.localIdentifier = genRanHex(64)
                                return item;
                            }),
                            localVideos: post.videos.map(item => {
                                item.localIdentifier = genRanHex(64)
                                return item;
                            }),
                            });
                    }
                };

                navigation.setOptions({
                    title: title,
                    headerRight: () => this.setHeaderRight(),
                })
            }
        );

        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData && this.userData.customer) {
                    // console.log('AsyncStorage', this.userData);
                    this.setState({
                        accessedForum: this.userData.customer,
                        settingsForum: this.userData.settings,
                        loggedUser: this.userData.loggedUser,
                    });
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    handleDialogOpen = (params) => {
        this.setState({ dialogVisible: true });
    }

    handleDialogCancel = (params) => {
        this.setState({ dialogVisible: false });
    }

    handleDialogOk = (params) => {
        this.setState(previousState => ({
            embeds: { url: previousState.videoURLtmp },
            dialogVisible: false
        }));
        this.setState({ dialogVisible: false });
    }

    uniqueImages = (_images) => {
        // array1.concat(array2).filter((value, pos, arr) =>  arr.findIndex(item => item.id == value.id) === pos )
        return _images.filter((value, pos, arr) =>  arr.findIndex(item => item.path.replace(/^.*[\\\/]/, '') == value.path.replace(/^.*[\\\/]/, '')) === pos )
    }

    _onPressPhoto (event) {
        ImagePicker.openPicker({
            multiple: true,
            maxFiles: 10,
        }).then(images => {
            this.setState(previousState => ({
                localImages: this.uniqueImages(previousState.localImages.concat(images)),
            }));
        })
        .catch(error => {
            // E_PERMISSION_MISSING
            // console.log('imagePicker error', error.code, error.message)
            if (error.code == 'E_PERMISSION_MISSING') {
                Alert.alert('Cannot access your gallery', 'You have to go to Settings to enable it.');
            }
        })
    }

    _onPressVideo (event) {
        ImagePicker.openPicker({
            multiple: false,
            mediaType: "video",
        }).then((video) => {
            // console.log(video);
            this.setState(previousState => ({
                localVideos: [ video ],
            }));
        })
        .catch(error => {
            // E_PERMISSION_MISSING
            // console.log('imagePicker error', error.code, error.message)
            if (error.code == 'E_PERMISSION_MISSING') {
                Alert.alert('Cannot access your gallery', 'You have to go to Settings to enable it.');
            }
        })
    }

    async submitPost () {
        this.setState({ isLoading: true });
        let api_save_post_url;

        const form = new FormData();
        let { content, localImages, localVideos } = this.state;
        if (content !== null && content !== '') {
            content = content.trim()
        }

        // let _userData = {
        //     user_id: this.props.loggedUser.id,
        //     content: content,
        //     images: localImages,
        //     videos: localVideos,
        // }

        form.append('user_id', this.state.loggedUser.id);
        form.append('content', content);

        if (this.state.categoryId) {
            form.append('category_id', this.state.categoryId);
        }

        localImages.forEach(item => {
            if (item.url) {
                form.append('old_images[]', item.id);
            } else {
                let filename = item.filename || item.path.replace(/^.*[\\\/]/, '');

                form.append('images[]', {
                    uri: item.path,
                    type: item.mime,
                    name: filename,
                    size: item.size,
                });
            }
        });

        localVideos.forEach(item => {
            if (item.url) {
                form.append('old_videos[]', item.id);
            } else {
                let filename = item.filename || item.path.replace(/^.*[\\\/]/, '');

                form.append('videos[]', {
                    uri: item.path,
                    type: item.mime,
                    name: filename,
                    size: item.size,
                });
            }
        });

        if (this.state.embeds && this.state.embeds.url) {
            form.append('embeds[]', JSON.stringify(this.state.embeds));
        }

        const configHeaders = {
            headers: {
                'Content-Type': 'multipart/form-data',
                Shop: this.state.accessedForum.shop,
                token: this.state.accessToken
            },
            onUploadProgress: progressEvent => {
                var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                // console.log('onUploadProgress', percentCompleted)
                this.setState({ progress: percentCompleted });
            },
        };

        if (this.state.postId) {
            form.append('id', this.state.postId);
            api_save_post_url = BASE_URL_API + '/topics/edit/' + this.state.postId + '.json';
        } else {
            api_save_post_url = BASE_URL_API + '/topics/create.json';
        }

        axios.post(api_save_post_url, form, configHeaders)
        .then(res => {
            this.setState({ isLoading: false });

            // let data = res.data;
            if (res.data.topic !== undefined && !!res.data.topic) {
                const _postId = this.state.postId;

                this.setState({
                    content: null,
                    postId: null,
                    categoryId: null,
                    localImages: [],
                    localVideos: [],
                    embeds: {},
                });

                this.richtext.setContentHTML('')

                // this.props.updateTopicsData(res.data.topic);

                if (!this.state.categoryId) {
                    if (_postId) {
                        // return this.props.navigation.navigate('StoryDetail', { postId: _postId });
                        const navigateAction = NavigationActions.navigate({
                            routeName: 'StoryDetail',
                            params: {postId: _postId},
                            key: 'post_' + genRanHex(24)
                        });

                        return this.props.navigation.dispatch(navigateAction);
                    } else {
                        return this.props.navigation.push('Stories');
                    }
                } else {
                    return this.props.navigation.navigate('Categories', {
                        screen: 'CategoriesDetail',
                        params: { categoryId: this.state.categoryId }
                    });
                }
            } else {
                Alert.alert(res.data.error);
            }
        })
        .catch(error => {
            // Alert.alert(error.response.data.data.error);
            console.log('error', error)
            this.setState({
                isLoading: false,
            });
        });
    }

    onPressMention = (_user) => {
        var mention = `<span class="mention" data-id="${_user.id}" contenteditable="false">${_user.name}</span> `;
        this.richtext.insertRawHTML(mention);
        this.setModalVisible(false); // insertRawHTML
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    onContentChange = (content) => {
        this.setState({ content });

        setTimeout( () => {
            this.props.navigation.setOptions({
                headerRight: () => this.setHeaderRight(),
            })
        }, 100)
    }

    removeImage(_localPath) {
        this.setState(previousState => ({
            localImages: previousState.localImages.filter(_item => _item.path.replace(/^.*[\\\/]/, '') !== _localPath)
        }))
    }

    removeVideo(_localPath) {
        this.setState(previousState => ({
            localVideos: previousState.localVideos.filter(_item => _item.path.replace(/^.*[\\\/]/, '') !== _localPath)
        }))
    }

    render() {
        return (
            <KeyboardAvoidingView style={styles.container} behavior={Platform.OS === "ios" ? "padding" : "height"} keyboardVerticalOffset={90} >
                <SafeAreaView style={{ flex: 1 }}>
                    {this.state.isLoading && (
                        <View style={[ styles.progressBar, { width: this.state.progress + '%' } ]}></View>
                    )}

                    <View style={{ flex: 1 }}>
                        <ScrollView style={{ flex: 1 }} keyboardDismissMode={'none'} keyboardShouldPersistTaps="always">

                        <RichEditor
                            ref={(r) => this.richtext = r}
                            containerStyle={{ borderWidth: 1, borderColor: 'transparent' }}
                            placeholder={"What's on your mind?"}
                            editorStyle={{ cssText }}
                            defaultParagraphSeparator="p"
                            pasteAsPlainText={true}
                            initialContentHTML={this.state.content}
                            onChange={content => this.onContentChange( content )}
                        />

                        {this.state.localImages.length > 0 && (
                        <View style={styles.mediaBox}>
                            <ScrollView horizontal={true}>
                                <View style={styles.scrollImagesBox}>
                                {this.state.localImages.map((_item, _index) => (
                                    <View style={styles.imageBox} key={'image_' + _index}>
                                        <Image source={{ uri: _item.url ? _item.url : _item.path}} style={{width: 80, height: 80}} />
                                        <View style={styles.imgClose}>
                                            <TouchableOpacity onPress={() => this.removeImage(_item.path.replace(/^.*[\\\/]/, '')) }>
                                                <View>
                                                    <Ionicons name="md-close" size={18} color="white" />
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                ))}
                                </View>
                            </ScrollView>
                        </View>
                        )}

                        {this.state.localVideos.length > 0 && (
                            <View style={styles.mediaBox}>
                                {this.state.localVideos.map((_item, _index) => (
                                <View key={'video_' + _item.id} style={ { flex: 1, backgroundColor: 'black' }}>
                                    <VideoPlayer
                                        video={{ uri: _item.url ? _item.url : _item.path }}
                                        videoWidth={600}
                                        videoHeight={400}
                                    />
                                    <View style={styles.imgClose}>
                                        <TouchableOpacity onPress={() => this.removeVideo(_item.path.replace(/^.*[\\\/]/, '')) }>
                                            <View>
                                                <Ionicons name="md-close" size={18} color="white" />
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                ))}
                            </View>
                        )}

                        {this.state.embeds && !!this.state.embeds.url && (
                            <View style={styles.mediaBox}>
                                <EmbedView url={this.state.embeds.url} />
                                <View style={styles.imgClose}>
                                        <TouchableOpacity onPress={() => this.setState({embeds: {} }) }>
                                            <View>
                                                <Ionicons name="md-close" size={18} color="white" />
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                            </View>
                        )}
                        </ScrollView>

                        <View style={styles.rowFooter}>
                            <TouchableOpacity style={styles.button} onPress={this._onPressPhoto.bind(this)} keyboardShouldPersistTaps={true}>
                                    <FontAwesome5 name="image" size={25} color="#44bc64" />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.button} onPress={this._onPressVideo.bind(this)} >
                                <FontAwesome5 name="video" solid size={25} color="#9c6efb" />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.button} onPress={this.handleDialogOpen}>
                                <FontAwesome5 name="youtube" brand size={25} color="#fe6324" color_old="#fe6324" />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.button} onPress={ event => this.setModalVisible(!this.state.modalVisible) }>
                                <FontAwesome5 name="at" brand size={25} color="#007bff" />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.button} onPress={null}>
                                <FontAwesome5 name="product-hunt" brand size={25} color="#d0d0d0" color_old="#1576f2" />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.button} onPress={null}>
                                <FontAwesome5 name="poll" solid size={25} color="#d0d0d0" color_old="#f1bb22" />
                            </TouchableOpacity>
                        </View>
                    </View>
                </SafeAreaView>

                <Dialog.Container visible={this.state.dialogVisible} onBackdropPress={this.handleDialogCancel}>
                    <Dialog.Title>Title</Dialog.Title>
                    <Dialog.Description>
                        Please enter your youtube or vimeo URL then click 'OK' button.
                    </Dialog.Description>
                    <Dialog.Input
                        placeholder={'https://vimeo //youtube...'}
                        onChangeText={(videoURLtmp) => this.setState({ videoURLtmp })}
                    />
                    <Dialog.Button label="Cancel" onPress={this.handleDialogCancel} />
                    <Dialog.Button label="Ok" onPress={this.handleDialogOk} />
                </Dialog.Container>

                <ModalUserList
                    modalVisible={this.state.modalVisible}
                    setModalVisible={_state => this.setModalVisible(_state)}
                    onPressMention={this.onPressMention}
                    accessedForum={this.state.accessedForum}
                    accessToken={this.state.accessToken}
                    loggedUser={this.state.loggedUser}
                />

            </KeyboardAvoidingView>
        );
    }
}

const cssText = `
p { margin: 0; margin-bottom: 5px; }
span.mention {
    background-color: rgba(96, 160, 206, 0.25);
    border-radius: 6px;
    padding: 1px 3px;
  }
`;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    progressBar: {
        height: 3,
        position: 'absolute',
        top: 0,
        zIndex: 9,
        backgroundColor: '#0066ff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.30,
        shadowRadius: 1.41,
        elevation: 2,
    },
    header: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 15,
        backgroundColor: '#ffffff',
        borderBottomWidth: 1,
        borderBottomColor: '#e9e9e9',
    },
    panelLeft: {
        width: '50%',
        flexDirection: 'row',
    },
    panelRight: {
        // flex: 1,
        width: '50%',
        alignContent: 'flex-end',
        alignItems: 'flex-end',
    },
    headerActions: {
        flexDirection: 'row',
    },
    editorBox: {
        flex: 1,
        borderWidth: 1,
    },
    inputContent: {
        // flex: 1,
        // borderWidth: 0,
        // borderColor: '#99c2ff',
        fontSize: 16,
        // backgroundColor: '#f0f5f5',
    },
    rowFooter: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      backgroundColor: '#fff',
      borderTopWidth: 1,
      borderTopColor: '#d0d0d0',
    },
    mediaBox: {
        paddingVertical: 15,
        marginHorizontal: -5,
        backgroundColor:"#f9f9f9",
    },
    imageBox: {
        marginHorizontal: 5,
    },
    scrollImagesBox: {
        flexDirection: 'row',
    },
    button: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
        marginVertical: 8,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 10,
    },
    buttonText: {
        fontSize: 18,
        color:'#ffffff',
        textAlign: 'center',
    },
    buttonCancel: {
        fontSize: 18,
        color: '#666',
    },
    imgClose: {
        position: 'absolute',
        right: 3,
        top: 3,
        width: 20,
        height: 20,
        alignContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'white',
        backgroundColor: 'rgba(0, 0, 0, .5)',
    }
});
