import React from "react";
import {View, Text, Image, Alert, RefreshControl, ActivityIndicator, Dimensions } from "react-native";
import {name as APP_NAME, USER_SAVE_KEY, STORAGE_KEY, BASE_URL_API, NOTIF_TOKEN_KEY, NOTIFICATION_API_URL, NOTIFICATION_OAUTH2_TOKEN} from '../../../app.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../../components/Loader';
import StoryList from '../../components/StoryList';
import StoriesHeader from './StoriesHeader';
import PostOrder from '../../components/PostOrder';
import axios from 'axios';
import { getMediumThumb } from "../../utils/global";

const screenWidth = Dimensions.get('window').width

export default class Stories extends React.Component {

    constructor(props){
        super(props);
        this.state ={
            refreshing: false,
            isLoading: true,
            isLoadingMore: false,
            accessToken: null,
            registerToken: null,
            fcmRegistered: false,
            accessedForum: {},
            settingsForum: {},
            loggedUser: {},
            dataList: [],
            badges: [],
            page: 1,
            sortOrder: null,
            showCategory: null,
            imgHeight: 100,
        }
    }

    componentDidMount() {
        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            // AsyncStorage.getItem(NOTIF_TOKEN_KEY)
            // .then((data) => {
            //     let _notification = JSON.parse(data);
            //     console.log('_notification', _notification);
            // });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData && this.userData.customer) {
                    this.setState({
                        isLoading: false,
                        accessedForum: this.userData.customer,
                        settingsForum: this.userData.settings,
                        badges: this.userData.badges,
                        loggedUser: this.userData.loggedUser,
                    });

                    if (this.userData.settings.banner) {
                        this.updateImgSize(getMediumThumb(this.userData.settings.banner))
                    }

                    this._onRefresh();
                    this._getNotifcations();
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _getNotifcations = () => {
        const configHeaders = { headers: { authorization: 'Bearer ' + NOTIFICATION_OAUTH2_TOKEN } };
        axios.get(NOTIFICATION_API_URL + '/notifications/user-unread/' + this.state.loggedUser.id + '/count', configHeaders)
        .then(res => {
            let data = res.data;

            // console.log('data _getNotifcations', data);
        })
        .catch(error => {
            if (error.response !== undefined) {
                // Alert.alert('Get notifications fail', error.response.data.message.toString());
            } else {
                // Alert.alert('Get notifications fail', error.toString());
            }
        })
    }

    _onRefresh = () => {
        this.setState({refreshing: true});

        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        const queries = 'page=' + this.state.page + '&sort=' + this.state.sortOrder;
        axios.get(BASE_URL_API + '/topics.json?' + queries, configHeaders)
        .then(res => {
            let data = res.data;
            this.setState({ refreshing: false, isLoadingMore: false });

            if (data && data.topics !== undefined && data.topics.length) {
                this.setState(previousState => ({
                    dataList: [...previousState.dataList, ...data.topics],
                    page: previousState.page + 1,
                    showCategory: data.show_category,
                }));
            } else {
                console.log('Reload fail', data.error);
                if (data.error !== undefined) {
                    Alert.alert(data.error.toString());
                }
            }
        })
        .catch(error => {
            this.setState({ refreshing: false })
            Alert.alert('Get data fail', error.response.data.message.toString());
        })
    }

    _onRefreshNew = () => {
        if ( this.state.isLoading || this.state.refreshing || this.state.isLoadingMore) {
            return;
        }

        this.setState({
            dataList: [],
            page: 1,
        });

        setTimeout(() => {
            this._onRefresh();
        }, 200)
    }

    _loadMoreResults = () => {
        if ( this.state.isLoading || this.state.refreshing || this.state.isLoadingMore) {
            return;
        }

        this.setState({isLoadingMore: true});
        this._onRefresh();
    }

    _updateTopicsData = (_topic) => {
        let { dataList } = this.state;
        dataList.unshift(_topic);
        this.setState({ dataList });
    }

    // _subString(_string, max_length = 20) {
    //     if (_string === undefined || _string === null || !_string) {
    //         return;
    //     }
    //     if (_string.length > max_length) {
    //         // Runes
    //         return runes.substr(_string, 0, max_length)+'...(See more)'
    //     }
    //     return _string
    // }

    pressAddNew() {
        return this.props.navigation.navigate('PostForm');
    }

    onOrderChange = (_value) => {
        this.setState({
            sortOrder: _value,
            dataList: [],
            page: 1,
        });

        setTimeout(() => {
            this._onRefresh();
        }, 200)
    }

    updateImgSize = (img) => {
        Image.getSize(img, (width, height) => {
            // calculate image width and height 
            const scaleFactor = width / (screenWidth - 20);
            const imageHeight = height / scaleFactor;
            // setImgWidth(screenWidth)
            this.setState({ imgHeight: imageHeight })
        })
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Loader loading={this.state.isLoading} />
                <StoryList
                    data={this.state.dataList}
                    extraData={this.state}
                    loggedUser={this.state.loggedUser}
                    accessedForum={this.state.accessedForum}
                    accessToken={this.state.accessToken}
                    navigation={this.props.navigation}
                    updateData={dataList => this.setState({ dataList }) }
                    badges={this.state.badges}
                    settingsForum={this.state.settingsForum}
                    showCategory={this.state.showCategory}
                    refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefreshNew}
                    />
                    }
                    ListEmptyComponent={!this.state.isLoading && !this.state.refreshing && (
                        <View style={{ padding: 15, alignContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: '#999' }}>No stories here</Text>
                        </View>
                    )}
                    onEndReachedThreshold={0.25}
                    onEndReached={({ distanceFromEnd }) => {
                        // if (distanceFromEnd > 0) {
                            this._loadMoreResults();
                        // }
                    }}
                    ListHeaderComponent={
                        <>
                            {this.state.settingsForum && this.state.settingsForum.banner && !this.state.settingsForum.hide_banner && (
                                <View style={{ marginTop: 10, marginHorizontal: 10 }}>
                                    <Image source={{ uri: getMediumThumb(this.state.settingsForum.banner)}} style={{ width: '100%', height: this.state.imgHeight }}/>
                                </View>
                            )}
                            <StoriesHeader authenUser={this.state.loggedUser} onPressAdd={event => this.pressAddNew()} />
                            <PostOrder onChange={ this.onOrderChange } />
                        </>
                    }
                />

                {this.state.isLoadingMore && (
                    <ActivityIndicator animating={this.state.isLoadingMore} />
                )}
            </View>
        );
    }
}
