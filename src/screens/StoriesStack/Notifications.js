import React from "react";
import {View, Alert, Text, SafeAreaView, FlatList, RefreshControl, TouchableOpacity, StyleSheet } from "react-native";
import {name as APP_NAME, USER_SAVE_KEY, STORAGE_KEY, BASE_URL_API, NOTIFICATION_API_URL, NOTIFICATION_OAUTH2_TOKEN} from '../../../app.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import UserAvatar from "react-native-user-avatar";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import Loader from '../../components/Loader';
import Moment from 'react-moment';
import axios from 'axios';
import { getSmallThumb } from "../../utils/global";

export default class Notifications extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            refreshing: false,
            isLoading: true,
            accessToken: null,
            accessedForum: {},
            loggedUser: {},
            dataList: [],
        };
        this.swipeRows = [];
        this.prevOpenedSwipeRow = null;
    }

    componentDidMount() {
        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData && this.userData.customer) {
                    this.setState({
                        isLoading: false,
                        accessedForum: this.userData.customer,
                        loggedUser: this.userData.loggedUser,
                    });
                    this._onRefresh();
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _onRefresh = () => {
        if (this.prevOpenedSwipeRow) {
            this.prevOpenedSwipeRow.close();
        }

        const {navigation} = this.props;
        this.setState({refreshing: true});

        const configHeaders = { headers: { authorization: 'Bearer ' + NOTIFICATION_OAUTH2_TOKEN } };
        axios.get(NOTIFICATION_API_URL + '/notifications/user/' + this.state.loggedUser.id, configHeaders)
        .then(res => {
            let data = res.data;
            this.setState({refreshing: false, isLoadingMore: false})

            if (data && data.status === 'success' && data.data.length) {
                this.setState({
                    dataList: data.data,
                });
            }
        })
        .catch(error => {
            if (error.response !== undefined) {
                Alert.alert('Get notifications fail', error.response.data.message.toString());
            } else {
                Alert.alert('Get notifications fail', error.toString());
            }
        });
    }

    _onPressItem = (_item) => {
        this._onPressRead(_item._id);

        let regex = /[?&]([^=#]+)=([^&#]*)/g;
        let params = {};
        let match;

        while ((match = regex.exec(_item.url))) {
            params[match[1]] = match[2];
        }
        const { comment_id, replied_to_id } = params;

        switch (_item.type) {
            case 'new-post-like':
            case 'new-post-reply':
            case 'new-post-mention':
                this.props.navigation.navigate('CommentDetail', {commentId: comment_id, repliedId: replied_to_id});
                break;
            default:
                this.props.navigation.navigate('StoryDetail', {postId: _item.topic_id});
                break;
        }
    }

    _onPressRead = (_item_id = null) => {
        const configHeaders = { headers: { authorization: 'Bearer ' + NOTIFICATION_OAUTH2_TOKEN } };
        let update_api_url;

        if (_item_id) {
            update_api_url = NOTIFICATION_API_URL + '/notifications/' + _item_id + '/read'
        } else {
            update_api_url = NOTIFICATION_API_URL + '/notifications/user/' + this.state.loggedUser.id + '/read'
        }

        axios.put(update_api_url, null, configHeaders)
        .then(res => {
            let data = res.data;
            let { dataList } = this.state;

            if (data && data.status === 'success') {
                if (_item_id) {
                    dataList.map(item => {
                        if (item._id === _item_id) {
                            item.readn = 1;
                            return item;
                        }
                    });
                } else {
                    dataList.map(item => {
                        item.readn = 1;
                        return item;
                    });
                }

                this.setState({dataList});
            }
        })
        .catch(error => {
            if (error.response !== undefined) {
                Alert.alert('Mark as Read notifications fail', error.response.data.message.toString());
            } else {
                Alert.alert('Mark as Read notifications fail', error.toString());
            }
        });
    }

    _markAllAsReadAlert = () => {
        Alert.alert(
            `Do you want to mark all notifications as read?`,
            null,
            [
                { text: "Cancel", style: 'cancel'},
                { text: "OK", onPress: () => this._onPressRead() }
            ],
            { cancelable: false }
        );
    }

    _deleteNotification = (_item_id = null) => {
        const configHeaders = { headers: { authorization: 'Bearer ' + NOTIFICATION_OAUTH2_TOKEN } };
        let delete_api_url;

        if (_item_id) {
            delete_api_url = NOTIFICATION_API_URL + '/notifications/' + _item_id
        } else {
            delete_api_url = NOTIFICATION_API_URL + '/notifications/user/' + this.state.loggedUser.id
        }

        axios.delete(delete_api_url, configHeaders)
        .then(res => {
            let data = res.data;
            let { dataList } = this.state;

            if (data && data.status === 'success') {
                if (_item_id) {
                    this.swipeRows = this.swipeRows.filter((item, index) => this.swipeRows[index] != _item_id);
                    dataList = dataList.filter(item => item._id !== _item_id);
                } else {
                    this.swipeRows = [];
                    dataList = [];
                }

                this.setState({dataList});
            }
        })
        .catch(error => {
            if (error.response !== undefined) {
                Alert.alert('Delete notifications fail', error.response.data.message.toString());
            } else {
                Alert.alert('Delete notifications fail', error.toString());
            }
        });
    }

    _deleteAlert = (_item_id = null) => {
        let scope = _item_id ? 'this' : 'all';
        let quantity = _item_id ? 'notification' : 'notifications';

        Alert.alert(
            `Do you want to delete ${scope} ${quantity}?`,
            null,
            [
                { text: "Cancel", style: 'cancel'},
                { text: "OK", onPress: () => this._deleteNotification(_item_id) }
            ],
            { cancelable: false }
        );
    }

    _headerOptions = () => {
        if (!this.state.dataList.length) {
            return null;
        }

        let markAllAsReadEnable = false;

        this.state.dataList.forEach(item => {
            if (!item.readn) {
                markAllAsReadEnable = true;
                return;
            }
        })

        return (
            <View style={styles.rowHeader}>
                <View style={styles.iconWrapper}>
                    {markAllAsReadEnable ? (
                        <TouchableOpacity onPress={() => this._markAllAsReadAlert()}>
                            <View style={[styles.iconHeader, { borderRightWidth: 1, borderColor: '#ccc' }]}>
                                <FontAwesome5 name="check" regular size={16} color="green" />
                            </View>
                        </TouchableOpacity>
                    )
                    : null}
                    <TouchableOpacity onPress={() => this._deleteAlert()}>
                        <View style={styles.iconHeader}>
                            <FontAwesome5 name="trash-alt" regular size={15} color="red" />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    _rightActions = (progress, _item) => {
        return (
            <>
                <TouchableOpacity onPress={() => this._deleteAlert(_item._id)}>
                    <View style={styles.iconRight}>
                        <FontAwesome5 name="trash-alt" regular size={20} color="red" />
                    </View>
                </TouchableOpacity>
                {!_item.readn ? (
                    <TouchableOpacity onPress={() => this._onPressRead(_item._id)}>
                        <View style={styles.iconRight}>
                            <FontAwesome5 name="check" regular size={20} color="gray" />
                        </View>
                    </TouchableOpacity>
                ) : null}
            </>
        )
    }

    _onCloseSwipeRow = (_index) => {
        if (this.prevOpenedSwipeRow && this.prevOpenedSwipeRow !== this.swipeRows[_index]) {
            this.prevOpenedSwipeRow.close();
        }
        this.prevOpenedSwipeRow = this.swipeRows[_index];
    }

    _parse_array = (_string, _html_array) => {
        var start, end,
            open_tag = '<em>',
            close_tag = '</em>';

        start = _string.indexOf(open_tag);
        if (start > -1) {
            end =_string.indexOf(close_tag, end);

            if (end > -1) {
                var _substring = _string.substring(0, start);
                _html_array.push(_substring);

                var _name = _string.substring(start + open_tag.length, end);
                _html_array.push(<Text key={'index_' + _html_array.length} style={styles.authorName}>{_name}</Text>);
                var _remaining = _string.substring(end + close_tag.length, _string.length);
                if (_remaining.length) {
                    return this._parse_array(_remaining, _html_array);
                }
            } else {
                _html_array.push(_string);
            }
        } else {
            _html_array.push(_string);
        }

        return _html_array;
    }

    _renderItem = ({item}) => {
        let actionIcon;
        let html_array = [];
        let _content = this._parse_array(item.content, html_array);

        switch (item.type) {
            case 'new-topic-like':
            case 'new-post-like':
                actionIcon = <FontAwesome5 name="thumbs-up" solid size={12} color="#007bff" />
                break;
            case 'new-topic-reply':
            case 'new-post-reply':
                actionIcon = <FontAwesome5 name="comment-alt" solid size={12} color="#ffc107" />
                break;
            case 'new-topic-mention':
            case 'new-post-mention':
            default:
                actionIcon = <FontAwesome5 name="at" solid size={12} color="#28a745" />
                break;
        }

        return (
            <Swipeable
                ref={ref => this.swipeRows[item._id] = ref}
                renderRightActions={(progress) => this._rightActions(progress, item)}
                friction={2}
                onSwipeableOpen={() => {
                    if (this.swipeRows[item._id]) {
                        this._onCloseSwipeRow(item._id);
                    }
                }}
            >
                <TouchableOpacity onPress={() => this._onPressItem(item)}>
                    <View style={[styles.dataRow, {backgroundColor: item.readn ? '#fff' : '#efefef'}]}>
                        <View style={styles.panelLeft}>
                            <View style={{ width: 50 }}>
                                <UserAvatar size={50} name={item.author.name} src={getSmallThumb(item.author.avatar)} />
                            </View>
                        </View>
                        <View style={styles.panelRight}>
                            <View>
                                <Text style={{ fontSize: 15 }}>{_content}</Text>
                            </View>
                            <View style={{ marginTop: 5, flexDirection: 'row' }}>
                                <View style={{ marginRight: 5, marginTop: 2 }}>
                                    {actionIcon}
                                </View>
                                {item.created && (
                                    <Moment fromNow ago element={Text} style={styles.textMuted}>{item.created}</Moment>
                                )}
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </Swipeable>
        )
    };

    render() {
        return (
            <>
                <Loader loading={this.state.isLoading} />
                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={this.state.dataList}
                        extraData={this.state}
                        keyExtractor={(item, index) => 'item_' + item._id}
                        renderItem={this._renderItem}
                        refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                        }
                        ItemSeparatorComponent={() => (<View style={styles.reparator} />)}
                        ListEmptyComponent={
                            !this.state.isLoading && !this.state.refreshing ? (
                                <View style={{ padding: 15, alignContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: '#999' }}>No notifications</Text>
                                </View>
                            ) : null
                        }
                        ListHeaderComponent={this._headerOptions()}
                    />
                </SafeAreaView>
            </>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    reparator: {
        height: .5,
        backgroundColor: '#ccc',
    },
    dataRow: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        backgroundColor: '#ffffff',
    },
    panelLeft: {
        height: 'auto',
        width: 60,
    },
    panelRight: {
        flex: 1,
    },
    textMuted: {
        color: '#909090',
    },
    iconRight: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 20,
        borderLeftWidth: .5,
        borderColor: '#ccc'
    },
    rowHeader: {
        padding: 10,
        marginBottom: 0,
        borderBottomWidth: 1,
        borderBottomColor: '#e0e0e0',
    },
    iconWrapper: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        backgroundColor: '#fff',
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#ccc',
    },
    iconHeader: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: '700',
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    authorName: {
        fontWeight: '700',
    }
});
