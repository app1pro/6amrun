import React, { useState } from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import UserAvatar from "react-native-user-avatar";
import Moment from 'react-moment';
import HTML from 'react-native-render-html';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ImageView from 'react-native-image-viewing';
import clip from "text-clipper";
import UserBadges from "../../components/UserBadges";
import OptionsComment from "../../components/OptionsComment";
import { getMediumThumb, getSmallThumb } from "../../utils/global";

export default function MyReplyItem ( props ) {
    const [imageCurrentIndex, setImageIndex] = useState(0);
    const [isImageViewVisible, setModalViewer] = useState(false);
    const [isContentCollapsed, setContentCollapsed] = useState(false);

    const showImage = (image) => {
        setImageIndex( props.images.findIndex(_item => _item.id == image.id) );
        setModalViewer( true );
    }

    let imagesViewer;

    if (props.images !== undefined && props.images.length > 0) {
        imagesViewer = props.images.map(_image => ({
            id: _image.id,
            uri: getMediumThumb(_image.url),
        }))
    }

    _renderReplyContent = (_content) => {
        const maxCharacters = 180;
        const tagsStyles = {
            p: { marginBottom: 5 }
        };
        const classesStyles = {
            'mention': { backgroundColor: 'rgba(96, 160, 206, 0.25)', borderRadius: 6, paddingVertical: 3 }
        };

        let clippedHtml =  clip( _content, maxCharacters, { html: true, maxLines: 3, indicator: '... (Read more)'} )

        if (isContentCollapsed || _content.length < maxCharacters) {
            return (
                <HTML
                    source={{ html: _content }}
                    emSize={15}
                    baseFontStyle={{ fontSize: 15 }}
                    tagsStyles={tagsStyles}
                    classesStyles={classesStyles}
                    parentWrapper={View}
                />
            )
        }
        return (
            <TouchableOpacity
                onPress={() => setContentCollapsed(!isContentCollapsed)}
                keyboardShouldPersistTaps={true}
            >
                <HTML
                    source={{ html: clippedHtml }}
                    emSize={15}
                    baseFontStyle={{ fontSize: 15 }}
                    tagsStyles={tagsStyles}
                    classesStyles={classesStyles}
                    parentWrapper={View}
                />
            </TouchableOpacity>
        )
    }

    const isLikeButtonClicked = props.loggedUser !== 'undefined' && props.likes.findIndex(_item => _item.user_id == props.loggedUser.id) > -1;

    return (
        <View style={ styles.row }>
            <View style={styles.panelLeft}>
                <TouchableOpacity onPress={() => props.onPressUser(props.author)} >
                    <UserAvatar size={25} name={props.author.name} src={getSmallThumb(props.author.avatar)} />
                </TouchableOpacity>
            </View>
            <View style={styles.panelRight}>
                <View style={styles.itemDetails}>
                        <UserBadges badgeList={props.badgeList} badgeIds={props.author.badge_id} />
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => props.onPressUser(props.author)} style={{marginRight: 5}}>
                                <Text style={[styles.itemName]}>{props.author.name}</Text>
                            </TouchableOpacity>
                            {props.author.is_staff && (
                                <View style={styles.wrapStaffBadge}>
                                    <Text style={styles.staffText}>{props.settingsForum.staff_text}</Text>
                                </View>
                            )}
                        </View>
                    <View>
                        {props.content.length > 0 ? _renderReplyContent(props.content) : null}
                    </View>
                    {props.images.length ? (
                        <View style={{ marginTop: 16 }}>
                            {
                                props.images.map((image, index) => (
                                    <TouchableOpacity onPress={() => showImage(image)} key={'image_' + image.id}>
                                        <Image source={{ uri: image.url }} style={styles.commentImage} />
                                    </TouchableOpacity>
                                ))
                            }
                            <ImageView
                                images={imagesViewer}
                                imageIndex={imageCurrentIndex}
                                animationType={'slide'}
                                visible={isImageViewVisible}
                                onRequestClose={() => setModalViewer(false)}
                            />
                        </View>
                    ) : null}
                </View>

                {props.loggedUser && props.author && props.loggedUser.id == props.author.id ? (
                    <View style={styles.rightWidget}>
                        <OptionsComment {...props} />
                    </View>
                ) : null}

                <View style={styles.itemFooter}>
                    <TouchableOpacity onPress={() => props.onPressPostLike(props.id)} >
                        <Text style={[ styles.likeButton, {color: isLikeButtonClicked ? '#09af00' : '#007bff'}]}>Like</Text>
                    </TouchableOpacity>
                    <Text style={styles.bullet}>•</Text>
                    <TouchableOpacity onPress={() => props.onPressReply({parent_id: props.parent_id, mention: props.author, name: props.author.name})} >
                        <Text style={styles.replyButton}>Reply</Text>
                    </TouchableOpacity>
                    <Text style={styles.bullet}>•</Text>
                    <Moment fromNow ago element={Text} style={[styles.itemTime]}>{props.created}</Moment>
                    {props.like_count ? (
                        <View style={styles.likeWrapper}>
                            <View style={styles.likeIcon}>
                                <View style={styles.likeBubble}>
                                    <FontAwesome5 name="thumbs-up" solid size={10} color="#fff" />
                                </View>
                                <Text>{props.like_count}</Text>
                            </View>
                        </View>
                    ) : null}
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    row: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        paddingTop: 8,
        paddingHorizontal: 0,
    },
    panelLeft: {
        height: 35,
        width: 35,
        paddingRight: 10,
    },
    panelRight: {
        flex: 1,
        flexDirection: 'column',
        marginBottom: 10,
        position: 'relative',
    },
    wrapStaffBadge: {
        borderRadius: 4,
        backgroundColor: '#dce0e2',
        alignItems: 'center'
    },
    staffText: {
        fontSize: 12,
        color: '#5d707f',
        paddingHorizontal: 7,
        paddingVertical: 2
    },
    rightWidget: {
        position: 'absolute',
        top: 0,
        right: 0,
        zIndex: 99,
    },
    itemDetails: {
        flex: 1,
        flexDirection: 'column',
        paddingTop: 8,
        paddingBottom: 10,
        paddingHorizontal: 8,
        backgroundColor: 'rgba(0,0,0,.07)',
        borderRadius: 8,
    },
    itemName: {
        color: '#007bff',
        fontSize: 15,
        paddingBottom: 3,
        fontWeight: 'bold',
    },
    itemContent: {
        fontSize: 15,
    },
    itemTime: {
        fontSize: 15,
        color: '#999',
    },
    iconTrash: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    likeButton: {
        // color: '#007bff',
        fontSize: 15
    },
    replyButton: {
        color: '#007bff',
        fontSize: 15
    },
    bullet: {
        color: '#939393',
        paddingHorizontal: 5
    },
    loadMore: {
        padding: 10,
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
    },
    itemFooter: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 5,
        paddingHorizontal: 5
    },
    likeWrapper: {
        flex: 1,
        alignItems: 'flex-end',
        textAlign: 'right',
        marginTop: -10,
    },
    likeIcon: {
        flexDirection: 'row',
        paddingVertical: 3,
        paddingHorizontal: 5,
        borderWidth: 1,
        borderColor: '#e0e0e0',
        borderRadius: 50,
        backgroundColor: "#fff",
    },
    likeBubble: {
        backgroundColor: "#007bff",
        paddingVertical: 4,
        paddingHorizontal: 5,
        marginRight: 5,
        borderRadius: 50,
        height: 20,
    },
    commentImage : {
        width: 180,
        height: 101,
    }
});
