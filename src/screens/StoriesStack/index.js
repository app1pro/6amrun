
import * as React from 'react';

import {View, Text, TouchableOpacity } from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Stories from './Stories';
import UserProfiles from './UserProfiles';
import StoryDetail from './StoryDetail';
import Search from './Search';
import PostForm from './PostForm';
import Notifications from './Notifications';
import CommentDetail from './CommentDetail';
import BellButton from '../../components/BellButton';
import { createStackNavigator } from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

const StoryStack = createStackNavigator();

function StoriesStack({navigation, route}) {
    React.useLayoutEffect(() => {
        const routeName = getFocusedRouteNameFromRoute(route);
        if (routeName === "StoryDetail" || routeName === "PostForm" || routeName === "CommentDetail"){
            navigation.setOptions({tabBarVisible: false});
        }else {
            navigation.setOptions({tabBarVisible: true});
        }
    }, [navigation, route]);

    return(
      <StoryStack.Navigator>
        <StoryStack.Screen name="Stories" component={Stories}
        options={({ navigation, route }) => ({
            title: 'Home',
            headerLeft: () => (
                <TouchableOpacity style={{ paddingHorizontal: 15, paddingVertical: 5 }} onPress={() => navigation.navigate('Search')}>
                    <FontAwesome5 name="search" size={20} color="#333" />
                </TouchableOpacity>
            ),
            headerRight: () => (
                <TouchableOpacity style={{ paddingHorizontal: 15, paddingVertical: 5 }} onPress={() => navigation.navigate('Notifications')}>
                    <BellButton
                        icon={<FontAwesome5 name="bell" solid size={20} color="#333" />}
                     />
                </TouchableOpacity>
            ),
        })} />
        <StoryStack.Screen name="StoryDetail" component={StoryDetail}
            options={{ title: 'Post Detail' }}
        />
        <StoryStack.Screen name="UserProfiles" component={UserProfiles} 
            options={{ title: 'User Profile' }}
        />
        <StoryStack.Screen name="Search" component={Search} />
        <StoryStack.Screen name="PostForm" component={PostForm} />
        <StoryStack.Screen name="Notifications" component={Notifications}
        options={({ navigation, route }) => ({
            headerRight: () => (
                <TouchableOpacity style={{ paddingHorizontal: 15, paddingVertical: 5 }} onPress={() => navigation.navigate('Stories')}>
                    <FontAwesome5 name="times" solid size={20} color="#333" />
                </TouchableOpacity>
            ),
        })} />
        <StoryStack.Screen name="CommentDetail" component={CommentDetail} 
            options={{ title: 'Comment Detail' }}
        />
      </StoryStack.Navigator>
    );
}

export default StoriesStack;
