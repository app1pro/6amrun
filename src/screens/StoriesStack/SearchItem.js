import React, { Component, useState } from "react";
import { View, Text, TouchableOpacity, StyleSheet, Image } from "react-native";
import UserAvatar from "react-native-user-avatar";
import Moment from 'react-moment';
import HTML from 'react-native-render-html';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import clip from "text-clipper";
import CategoryNotch from "../../components/CategoryNotch";
import OnlineUserDot from '../../components/OnlineUserDot';
import { getSmallThumb } from "../../utils/global";

function ProductItem(props) {
    const truncateString = (str, max) => {
        if (str.length > max) {
            const suffix = "...";
            return `${str.substr(0, str.substr(0, max - suffix.length).lastIndexOf(' '))}${suffix}`;
        }
        return str;
    }

    return (
        <View style={styles.item} key={props.product_id}>
            <Image source={{ uri: props.product_image}} style={styles.image}/>
            <View style={styles.productDetails}>
                <Text style={styles.title}>{props.product_title}</Text>
                <Text style={styles.desc}>{truncateString(props.product_desc, 120)}</Text>
                {!!props.product_price && (
                    <Text style={styles.price}>${props.product_price}</Text>
                )}
            </View>
        </View>
    );
}

export default function SearchItem (props) {
    const _onPressUser = () => {
        props.onPressUser(props);
    }

    const _onPressItem = () => {
        props.onPressItem(props);
    }

    const renderPostContent = (content) => {
        const maxCharacters = 180;
        let clippedHtml =  clip( content, maxCharacters, { html: true, maxLines: 3, indicator: '... (Read more)'} )

        if (content === null) {
            return null
        }

        if (content.length < maxCharacters) {
            return (
                <HTML
                    source={{ html: content }}
                    emSize={15}
                    baseFontStyle={{ fontSize: 15 }}
                    tagsStyles={{
                        p: { marginBottom: 0 }
                    }}
                    parentWrapper={View}
                />
            )
        }
        return (
                <HTML
                    source={{ html: clippedHtml }}
                    emSize={15}
                    baseFontStyle={{ fontSize: 15 }}
                    tagsStyles={{
                        p: { marginBottom: 0 }
                    }}
                    parentWrapper={View}
                />
        )
    }

    return (
        <View style={styles.sectionWrap}>
            {props.showCategory && props.category ? (
                <CategoryNotch {...props.category}/>
            ) : null}
            <View style={styles.section}>
                <View style={styles.sectionBody}>
                    <TouchableOpacity onPress={() => _onPressItem(props)}>
                        <View style={styles.sectionBodyTop}>
                            <View>
                                {renderPostContent(props.content)}
                            </View>
                            {props.images.length || props.embeds.length || props.videos.length ? (
                                /** render images/embeds/videos here */
                                <View style={styles.media}>
                                    {/* <View style={style.videos}>
                                        </View>
                                    <View style={style.embed}>
                                    </View> */}
                                    {/* <View style={styles.images}>
                                    </View> */}
                                </View>
                            ) : null}
                        </View>
                        {props.products.length ? (
                            <View style={styles.sectionBodyBottom}>
                                <View style={styles.products}>
                                    {props.products.map(_item => (
                                        <ProductItem key={'product_' + _item.id} {..._item} />
                                    ))}
                                </View>
                            </View>
                        ) : null}
                    </TouchableOpacity>
                </View>
                <View style={styles.sectionBottom}>
                    <View style={styles.sectionBottomLeft}>
                        {props.author ? (
                        <View style={styles.authorIcon}>
                            <TouchableOpacity onPress={() => _onPressUser()} >
                                <UserAvatar size={25} name={props.author.name} src={getSmallThumb(props.author.avatar)} />
                                <OnlineUserDot shopify_cid={props.author.shopify_cid} size={8} offset={'70%'} />
                            </TouchableOpacity>
                        </View>
                        ) : null}
                        {props.author ? (
                        <View style={styles.authorName}>
                            <TouchableOpacity onPress={() => _onPressUser()} >
                                <Text style={styles.itemName}>{props.author.name}</Text>
                            </TouchableOpacity>
                        </View>
                        ) : null}
                        <Text style={styles.bullet}>•</Text>
                        <Moment fromNow ago element={Text} style={styles.itemTime}>{props.created}</Moment>
                    </View>
                    <View style={styles.sectionBottomRight}>
                        {props.like_count ? (
                                <View style={styles.likeCount}>
                                    <View style={styles.likeBubble}>
                                        <FontAwesome5 name="thumbs-up" solid size={10} color="#fff" />
                                    </View>
                                    <Text style={styles.numbers}>&nbsp;{props.like_count}</Text>
                                </View>
                        ) : null}
                        {props.no_posts ? (
                            <View style={styles.noPostsCount}>
                                <FontAwesome5 name="comment-alt" size={18} color="gray" />
                                <Text style={styles.numbers}>&nbsp;{props.no_posts}</Text>
                            </View>
                        ) : null}
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    sectionWrap: {
        marginTop: 10,
    },
    section: {
        borderColor: '#e9e9e9',
        borderWidth: 1,
        borderRadius: 8,
        backgroundColor: '#fff',
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    sectionBody: {
        flexDirection: 'column',
        paddingBottom: 8,

    },
    sectionBodyTop: {
        flex: 1,
        flexDirection: 'row',
        paddingBottom: 20,
    },
    sectionBodyBottom: {
    },
    content: {
        flex: 1,
    },
    media: {
        width: 135,
    },
    sectionBottom: {
        flex: 1,
        flexDirection: 'row',
    },
    sectionBottomLeft: {
        flex: 1,
        flexDirection: 'row',
    },
    sectionBottomRight: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    authorIcon: {
        width: 25,
        height: 25,
        padding: 0,
        marginTop: -5,
    },
    authorName: {
        paddingLeft: 5,
    },
    bullet: {
        fontSize: 14,
        color: '#939393',
        paddingLeft: 5,
    },
    itemName: {
        color: '#007bff',
        fontSize: 14,
        fontWeight: '500',
    },
    itemTime: {
        paddingLeft: 5,
        fontSize: 14,
        color: '#999',
    },
    likeBubble: {
        backgroundColor: "#007bff",
        paddingVertical: 4,
        paddingHorizontal: 5,
        borderRadius: 50,
        height: 20,
    },
    noPostsCount: {
        flexDirection: 'row',
        paddingLeft: 10,
    },
    likeCount: {
        flexDirection: 'row',
    },
    numbers: {
        fontSize: 14,
    },
    /** ProductItem style */
    item: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: 'rgba(0, 0, 0, .2)',
    },
    image: {
        width: 65,
        height: 100
    },
    productDetails: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 15,
    },
    title: {
        color: "#007bff",
        fontWeight: "bold",
        marginBottom: 4,
        fontSize: 15,
    },
    desc: {
        color: "#6c757d",
        marginBottom: 5,
        fontSize: 15
    },
    price: {
        color: "#ff0000",
    },
    /** ProductItem style */

});