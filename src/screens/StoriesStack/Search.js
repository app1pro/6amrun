import React, { useRef, createRef } from "react";
import {View, Text, Image, TextInput, FlatList, RefreshControl, TouchableOpacity, StyleSheet, Alert } from "react-native";
import {name as APP_NAME, USER_SAVE_KEY, STORAGE_KEY, BASE_URL_API} from '../../../app.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Loader from '../../components/Loader';
import LoadingIcon from '../../components/LoadingIcon';
import axios from 'axios';
// import moment from "moment";
import SearchItem from "./SearchItem";

export default class Search extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            refreshing: false,
            isLoading: true,
            isLoadingMore: false,
            accessToken: null,
            accessedForum: {},
            settingsForum: {},
            badges: [],
            showCategory: null,
            loggedUser: {},
            dataList: [],
            searchString: '',
            page: 1,
        }
    }

    componentDidMount() {
        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData && this.userData.customer) {
                    console.log('AsyncStorage', this.userData);
                    this.setState({
                        isLoading: false,
                        accessedForum: this.userData.customer,
                        settingsForum: this.userData.settings,
                        badges: this.userData.badges,
                        loggedUser: this.userData.loggedUser,
                        settingsForum: this.userData.settings,
                    });
                    this._onRefresh();
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.get(BASE_URL_API + '/topics/search.json?page=' + this.state.page + '&search=' + this.state.searchString, configHeaders)
        .then(res => {
            let data = res.data;
            this.setState({ refreshing: false, isLoadingMore: false });

            if (data && data.topics !== undefined && data.topics.length) {
                this.setState(previousState => ({
                    dataList: [...previousState.dataList, ...data.topics],
                    page: previousState.page + 1,
                    showCategory: data.show_category,
                }));
            } else {
                console.log('Reload fail', data.error);
                if (data.error !== undefined) {
                    Alert.alert(data.error.toString());
                }
            }
        })
        .catch(error => {
            this.setState({ refreshing: false })
            Alert.alert('Get data fail', error.response.data.message.toString());
        })
    }

    _loadMoreResults = () => {
        if ( this.state.isLoading || this.state.refreshing || this.state.isLoadingMore) {
            return;
        }

        this.setState({isLoadingMore: true});
        this._onRefresh();
    }

    _onPressUser = (_post) => {
        return this.props.navigation.navigate('UserProfiles', {userId: _post.author.id});
    }

    _onPressItem = (_post) => {
        return this.props.navigation.navigate('StoryDetail', {postId: _post.id});
    }

    _renderItem = ({item}) => {

        if (item.products.length) {
            item.products.map(_product => {
                if (_product.product_url.indexOf('//') === -1) {
                    _product.product_url = 'https://' + this.state.accessedForum.domain + '/products/' + _product.product_url
                }

                return _product;
            })
        }

        return (
            <SearchItem
                id={item.id}
                content={item.content}
                created={item.created}
                author={item.author}
                likes={item.likes}
                no_posts={item.no_posts}
                like_count={item.like_count}
                images={item.images || []}
                embeds={item.embeds || []}
                videos={item.videos || []}
                products={item.products || []}
                category={item.category}
                onPressItem={(_user) => this._onPressItem(_user)}
                onPressUser={(_user) => this._onPressUser(_user)}
                badges={this.state.badges}
                settingsForum={this.state.settingsForum}
                showCategory={this.state.showCategory}
            />
        )
    };

    _onPressSearch = () => {
        if (this.state.isLoading || this.state.refreshing || this.state.isLoadingMore) {
            return;
        }

        this.setState({
            refreshing: true,
            dataList: [],
            page: 1,
        });

        setTimeout(() => {
            this._onRefresh();
        }, 200)

    }

    render() {
        return (
            <View style={styles.container}>
                <Loader loading={this.state.isLoading} />
                <View style={styles.header}>
                    <View style={styles.panelLeft}>
                        {/* <TextInput style={{ height: 40, }} placeholder="Search members" maxLength={40} /> */}
                        <TextInput style={{ height: 35, padding: 3 }} placeholder="Search topic" onChangeText={(text) => {this.setState({searchString: String(text)})}} maxLength={40} />
                    </View>
                    {/* <TouchableOpacity onPress={() => console.log('Pressed')} style={styles.panelRight}> */}
                    <TouchableOpacity onPress={() => this._onPressSearch()} style={styles.panelRight}>
                        <FontAwesome5 name="search" size={20} color="#333" />
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={this.state.dataList}
                    keyExtractor={(item, index) => 'item_' + item.id}
                    renderItem={this._renderItem}
                    onEndReachedThreshold={0.25}
                    onEndReached={({ distanceFromEnd }) => {
                        // if (distanceFromEnd > 0) {
                            this._loadMoreResults();
                        // }
                    }}
                    ListFooterComponent={
                        <View style={{flex: 1, height: 20}}/>
                    }
                    ListEmptyComponent={
                        (this.state.isLoading || this.state.refreshing || this.state.isLoadingMore) ? (
                            <View style={styles.notFound}>
                                <Text style={styles.textNotFound}>Loading...</Text>
                            </View>
                        ) : (
                            <View style={styles.notFound}>
                                <Text style={styles.textNotFound}>Topic not found!</Text>
                            </View>
                        )
                    }
                />

                {this.state.isLoadingMore && (
                    <LoadingIcon />
                )}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        paddingHorizontal: 5,
    },
    header: {
        flexDirection: 'row',
        paddingHorizontal: 5,
        paddingVertical: 5,
        // marginHorizontal: 5,
        marginVertical: 10,
        borderRadius: 50,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    panelLeft: {
        flex: 1,
        paddingLeft: 10,
    },
    panelRight: {
        width: 22,
        paddingTop: 0,
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    itemInputText: {
        fontSize: 15,
        color: '#999',
    },
    notFound: {
        alignItems: 'center',
        paddingVertical: 15,
    },
    textNotFound: {
        fontSize: 16,
        fontWeight: '500',
        color: 'gray'
    },
});