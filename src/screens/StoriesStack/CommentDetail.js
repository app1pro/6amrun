import React, { Component } from "react";
import { Text, Alert, View, ScrollView, StyleSheet, TouchableOpacity } from "react-native";
import {name as APP_NAME, USER_SAVE_KEY, STORAGE_KEY, BASE_URL_API} from '../../../app.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import MyCommentItem from "./MyCommentItem";
import Loader from '../../components/Loader';
import NewComment from "../../components/NewComment";

export default class CommentDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            isLoading: true,
            accessToken: null,
            accessedForum: {},
            loggedUser: {},
            settingsForum: {},
            badges: [],
            comment: {},
            post: {},
            topic: {},
            postId: null,
            initialFocus: false,
            modalVisible: false,
            actionType: '',
        };
    }

    componentDidMount() {
        this.props.navigation.setOptions({
            'title': 'Comment detail'
        });

        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData && this.userData.customer) {
                    this.setState({
                        postId: this.props.route.params.commentId,
                        accessedForum: this.userData.customer,
                        loggedUser: this.userData.loggedUser,
                        settingsForum: this.userData.settings,
                        badges: this.userData.badges,
                    });
                    this._onRefresh();
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _onRefresh = () => {
        this.setState({refreshing: true});

        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        const query = 'user_id=' + this.state.loggedUser.id + '&post_id=' + this.state.postId;
        axios.get(BASE_URL_API + '/posts/single.json?' + query, configHeaders)
        .then(res => {
            let data = res.data;
            this.setState({refreshing: false, isLoadingMore: false})

            if (data && data.post !== undefined) {
                this.setState({
                    comment: data.post,
                    topic: data.topic
                });
            }

            this.setState({
                isLoading: false
            });
        })
        .catch(error => {
            this.setState({
                isLoading: false
            });
            console.log(error.response);
            Alert.alert('ERROR!', error.response.data.message);
        });
    }

    _onPressUser = (_item) => {
        return this.props.navigation.navigate('UserProfiles', {userId: _item.id});
    }

    _onPressPostLike(_item_id) {
        // Alert.alert('liked', _item_id + '')
        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.post(BASE_URL_API + '/posts/like.json', {post_id: _item_id, user_id: this.state.loggedUser.id}, configHeaders)
        .then(res => {
            let { data } = res;
            if (data.success) {
                let { comment } = this.state;

                if (comment.id == _item_id) {
                    comment.like_count = data.like_count;
                    comment.likes = data.likes;
                } else if (typeof comment.all_replies !== 'undefined') {
                    comment.all_replies = comment.all_replies.map(_reply => {
                        if (_reply.id == _item_id) {
                            _reply.like_count = data.like_count;
                            _reply.likes = data.likes;
                        }
                        return _reply;
                    });
                }
                this.setState({ comment });
            }
        })
        .catch(error => {
            console.log('like error', error.response)
        })
    }

    _onPressLoadMoreReply = (_item_id) => {
        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        var query = 'post_id=' + _item_id + '&page=1' + '&user_id=' + this.state.loggedUser.id;
        axios.get(BASE_URL_API + '/posts/get-replies.json?' + query, configHeaders)
        .then(res => {
            let { data } = res;
            let { comment } = this.state;

            if (data.replies.length && comment.id == _item_id) {
                comment.all_replies = data.replies;
                this.setState({ comment });
            }
        })
        .catch(error => {
            console.log('Comments error', error.response)
        })
    }

    _onPressViewPost = (_item) => {
        this.props.navigation.navigate('StoryDetail', {postId: _item.id});
    }

    _editComment = (_data) => {
        let { comment } = this.state;

        if (comment.id == _data.id) {
            comment.content = _data.content;
        } else if (typeof comment.all_replies !== undefined) {
            comment.all_replies = comment.all_replies.map(reply => {
                if (reply.id == _data.id) {
                    reply.content = _data.content;
                }
                return reply;
            })
        }

        this.setState({ comment });
    }

    _deleteComment = (_item_id) => {
        let { comment } = this.state;

        if (comment.id == _item_id) {
            comment = {};
        } else if (typeof comment.all_replies !== undefined) {
            comment.all_replies = comment.all_replies.filter(reply => reply.id !== _item_id)
        }

        this.setState({ comment });
    }

    _onPressReply = (_item) => {
        this.setState({
            post: _item,
            initialFocus: true,
        })

        if (_item.mention) {
            this.newCommentRef.insertMention(_item.mention);
        }
    }

    _updateCommentsData = (_data) => {
        let { comment } = this.state;

        if (typeof comment.all_replies !== 'undefined') {
            comment.all_replies.push(_data);
        } else {
            comment.all_replies = [_data];
        }
        // Wait for JSX render
        setTimeout( () => this.scrollView.scrollToEnd({ animated: true }), 100)

        this.setState({ comment });
    }

    _onCloseCreateComment = () => {
        this.setState({
            initialFocus: false,
        });
        if (this.state.post) {
            this.setState({
                post: null,
            })
        }
    }

    render() {
        let { comment, isLoading } = this.state;

        if (isLoading) {
            return <Loader loading={isLoading} />
        }

        if (!isLoading && !comment.id) {
            return (
                <View style={{ padding: 15, alignContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: '#999' }}>Content does not exist or has been deleted!</Text>
                </View>
            )
        }

        return (
            <View style={styles.container}>
                <ScrollView
                    ref={ref => this.scrollView = ref}
                    showsVerticalScrollIndicator={false}
                >
                    <TouchableOpacity onPress={() => this._onPressViewPost(this.state.topic)}>
                        <View style={[styles.row, styles.rowHeader]}>
                            <View style={styles.headerLeft}>
                                {this.state.topic.author && this.state.topic.author.name ? (
                                    <Text style={{ fontSize: 15 }}>Comments on <Text style={{ fontWeight: '700' }}>{this.state.topic.author.name}</Text>'s <Text style={{ fontWeight: '700' }}>post</Text></Text>
                                ) : null}
                            </View>
                            <View style={styles.headerRight}>
                                <Text style={styles.viewPostText}>View post</Text>
                            </View>
                        </View>
                    </TouchableOpacity>

                    <View>
                        <MyCommentItem
                            id={comment.id}
                            content={comment.content}
                            created={comment.created}
                            author={comment.author}
                            replies={comment.all_replies || []}
                            images={comment.images || []}
                            likes={comment.likes || []}
                            like_count={comment.like_count}
                            like_shopify_cid={comment.like_shopify_cid}
                            loggedUser={this.state.loggedUser}
                            shop={this.state.accessedForum.shop}
                            badgeList={this.state.badges}
                            settingsForum={this.state.settingsForum}
                            onPressUser={(data) => this._onPressUser(data)}
                            onPressPostLike={(data) => this._onPressPostLike(data)}
                            onPressLoadMoreReply={(data) => this._onPressLoadMoreReply(data)}
                            onPressReply={(_item) => this._onPressReply(_item)}
                            editComment={(data) => this._editComment(data)}
                            deleteComment={(data) => this._deleteComment(data)}
                            accessedForum={this.state.accessedForum}
                            accessToken={this.state.accessToken}
                        />
                    </View>
                    <View style={{ height: 55 }}></View>
                </ScrollView>

                <NewComment
                    ref={el => this.newCommentRef = el}
                    updateCommentsData={(_data) => this._updateCommentsData(_data)}
                    accessedForum={this.state.accessedForum}
                    accessToken={this.state.accessToken}
                    loggedUser={this.state.loggedUser}
                    postId={this.state.topic.id}
                    initialFocus={this.state.initialFocus}
                    comment={{
                        parent_id: comment.id,
                        name: this.state.post && this.state.post.name ? this.state.post.name : comment.author.name,
                        mention: this.state.post && this.state.post.mention ? this.state.post.mention : null
                    }}
                    actionType="reply"
                    onCloseCreateComment={() => this._onCloseCreateComment()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    rowHeader: {
        flexDirection: 'row',
        borderBottomColor: '#e0e0e0',
        borderBottomWidth: 1,
    },
    row: {
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
    headerLeft: {
        flex: 1,
        justifyContent: 'center',
        paddingRight: 5,
    },
    headerRight: {
        backgroundColor: '#e3e5eb',
        borderRadius: 8,
        paddingVertical: 10,
        paddingHorizontal: 15
    },
    viewPostText: {
        fontWeight: '700',
        fontSize: 15
    },
});