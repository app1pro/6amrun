import React from "react";
import {
    View, Text, Alert,
    KeyboardAvoidingView,
    StyleSheet,
    ActivityIndicator,
    TouchableOpacity,
    Share,
    SafeAreaView,
    ScrollView
} from "react-native";
import {USER_SAVE_KEY, STORAGE_KEY, BASE_URL_API} from '../../../app.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import UserAvatar from "react-native-user-avatar";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MyCommentItem from './MyCommentItem';
import StoryItemData from '../../components/StoryItemData';
import NewComment from '../../components/NewComment';
import UserBadges from '../../components/UserBadges';
import OptionsTopic from "../../components/OptionsTopic";
import { ModalLikesList } from '../../components/ModalLikesList';
import Moment from 'react-moment';
import axios from 'axios';
import { Portal } from 'react-native-portalize';
import ModalReport from '../../components/ModalReport';
import { getSmallThumb } from "../../utils/global";

export default class StoryDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            sending: false,
            accessToken: null,
            accessedForum: {},
            settingsForum: {},
            badges: [],
            postId: null,
            topicData: {},
            content: null,
            localImage: null,
            report_object_id: null,
            report_type: null,
            loggedUser: {},
            commentList: [],
            commentPage: 1,
            likesList: [],
            isLoadMoreShow: true,
            initialFocus: false,
            modalVisible: false,
            post: {},
        }

        this.scrollView = null;
        this.newCommentRef = React.createRef(null).current;
    }

    componentDidMount() {
        const { postId } = this.props.route.params;

        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData && this.userData.customer) {
                    this.setState({
                        postId: postId,
                        accessedForum: this.userData.customer,
                        loggedUser: this.userData.loggedUser,
                        settingsForum: this.userData.settings,
                        badges: this.userData.badges,
                    });
                    this._refreshData()
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message)
        }
    }

    _refreshData() {
        const options = {
            headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken }
        };
        var query = 'topic_id=' + this.state.postId + '&user_id=' + this.state.loggedUser.id;
        axios.get(BASE_URL_API + '/topics/single.json?' + query, options)
        .then(res => {
            let { data } = res;
            if (data && data.topic) {
                this.setState({
                    topicData: data.topic,
                    commentList: data.topic.posts
                })
            }

            this.setState({
                isLoading: false,
            })
        })
        .catch(error => {
            this.setState({
                isLoading: false,
            })
            console.log('error connect:', error.response)
        })
    }

    _onPressUser = (_item) => {
        return this.props.navigation.navigate('UserProfiles', {userId: _item.id});
    }

    _onPressShare = () => {
        Share.share({
            url: this.state.topicData.url
        })
        .then(result => console.log(result))
        .catch(errorMsg => console.log(errorMsg));
    }

    _onPressLike = (_item_id) => {
        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.post(BASE_URL_API + '/topics/like.json', {topic_id: _item_id, user_id: this.state.loggedUser.id}, configHeaders)
        .then(res => {
            let { data } = res;

            if (data.success) {
                let { topicData } = this.state;
                topicData.like_count = data.like_count;
                topicData.likes = data.likes;

                this.setState({topicData})
            }
        })
        .catch(error => {
            console.log('like error', error.response)
        })
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    _openSendReport(data) {
        this.setState({
            report_object_id: data.object_id,
            report_type: data.type,
        })

        setTimeout( () => this.setModalVisible(true), 100)
    }

    _onPressPostLike(_item_id) {
        // Alert.alert('liked', _item_id + '')
        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.post(BASE_URL_API + '/posts/like.json', {post_id: _item_id, user_id: this.state.loggedUser.id}, configHeaders)
        .then(res => {
            let { data } = res;
            if (data.success) {
                let { commentList } = this.state
                commentList.map(_post => {
                    if (_post.id == _item_id) {
                        _post.like_count = data.like_count;
                        _post.likes = data.likes;
                    } else if (typeof _post.replies !== 'undefined') {
                        _post.replies = _post.replies.map(_reply => {
                            if (_reply.id == _item_id) {
                                _reply.like_count = data.like_count;
                                _reply.likes = data.likes;
                            }
                            return _reply;
                        });
                    }
                    return _post;
                });
                this.setState({ commentList });
            }
        })
        .catch(error => {
            console.log('like error', error.response)
        })
    }

    _onPressLoadMoreComment = () => {
        // Alert.alert('liked', _item_id + '')
        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        var query = 'topic_id=' + this.state.topicData.id + '&page=' + this.state.commentPage + '&user_id=' + this.state.loggedUser.id;
        axios.get(BASE_URL_API + '/posts/comments.json?' + query, configHeaders)
        .then(res => {
            let { data } = res;
            if (data.comments.length) {
                this.setState(prevState => ({
                    commentList: [...data.comments, ...prevState.commentList],
                    commentPage: prevState.commentPage + 1,
                }));
            }
            if (!data.data_remains) {
                this.setState(prevState => ({
                    isLoadMoreShow: !prevState.isLoadMoreShow
                }));
            }
        })
        .catch(error => {
            console.log('Comments error', error.response)
        })
    }

    _onPressLoadMoreReply = (_item_id, _reply_page) => {
        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        var query = 'post_id=' + _item_id + '&page=' + _reply_page + '&user_id=' + this.state.loggedUser.id;
        axios.get(BASE_URL_API + '/posts/get-replies.json?' + query, configHeaders)
        .then(res => {
            let { data } = res;
            let { commentList } = this.state;

            if (data.replies.length) {
                commentList.map(comment => {
                    if (comment.id == _item_id) {
                        comment.replies = comment.replies ? comment.replies.concat(data.replies): data.replies;
                    }
                    return comment;
                });

                this.setState({ commentList });
            }
        })
        .catch(error => {
            console.log('Replies error', error.response)
        })
    }

    _updateCommentsData = (_data) => {
        let { commentList } = this.state;

        if (_data.parent_id) {
            commentList = commentList.map(comment => {
                if (comment.id == _data.parent_id) {
                    if (typeof comment.replies !== 'undefined') {
                        comment.replies.push(_data);
                    } else {
                        comment.replies = [_data];
                    }
                }
                return comment;
            })

            this.setState({ commentList });
        } else {
            commentList.push(_data);

            this.setState({ commentList });
            // Wait for JSX render
            setTimeout( () => this.scrollView.scrollToEnd({ animated: true }), 100)
        }
    }

    _onPressComment = () => {
        this.setState({
            initialFocus: true
        });
    }

    _onPressReply = (_item) => {
        this.setState({
            post: _item,
            initialFocus: true,
            actionType: 'reply',
        })

        if (_item.mention) {
            this.newCommentRef.insertMention(_item.mention);
        }
    }

    _onCloseCreateComment = () => {
        this.setState({
            initialFocus: false,
            actionType: '',
        });
        if (this.state.post) {
            this.setState({
                post: null,
            })
        }
    }

    _viewLikesList = (_item_id) => {
        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.get(BASE_URL_API + '/topics/likes.json?topic_id='+_item_id, configHeaders)
        .then(res => {
            let { data } = res;

            if (data.success) {
                this.setState({
                    likesList: data.likes
                });
                this.modalizeRef?.open();
            }
        })
        .catch(error => {
            console.log('likes error', error.response)
        })
    }

    _editComment = (_data) => {
        let { commentList } = this.state;

        commentList.map(comment => {
            if (comment.id == _data.id) {
                comment.content = _data.content;
            } else if (typeof comment.replies !== 'undefined') {
                comment.replies = comment.replies.map(reply => {
                    if (reply.id == _data.id) {
                        reply.content = _data.content;
                    }
                    return reply;
                })
            }
            return comment;
        })

        this.setState({ commentList });
    }

    _deleteComment = (_item_id) => {
        let { commentList } = this.state;

        commentList = commentList.map(comment => {
            if (typeof comment.replies !== 'undefined') {
                comment.replies = comment.replies.filter(reply => reply.id !== _item_id)
            }
            return comment;
        }).filter(comment => comment.id !== _item_id);

        this.setState({ commentList });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loadingScreen}>
                    <ActivityIndicator/>
                </View>
            )
        }

        let totalLoadedComment = this.state.topicData.no_posts - this.state.commentList.length;
        let renderLikeButton;
        let renderLikesList;
        let { topicData } = this.state;

        if (topicData.likes && topicData.likes.length > 0) {
            var likesList = topicData.likes.map(item => item.user_name);
            renderLikesList = likesList.join(', ');
            if (topicData.like_count > topicData.likes.length) {
                renderLikesList += ' and ' + (topicData.like_count - topicData.likes.length) + ' others';
            }
        }

        if (this.state.loggedUser !== undefined && topicData.likes.findIndex(_item => _item.user_id == this.state.loggedUser.id) > -1) {
            renderLikeButton = (
                <TouchableOpacity onPress={() => this._onPressLike(topicData.id)} >
                    <View style={styles.boxButton}>
                        <FontAwesome5 name="thumbs-up" size={15} color="#007bff" />
                        <Text style={[ styles.numbers, {color: '#007bff'} ]}>Like</Text>
                    </View>
                </TouchableOpacity>
            )
        } else {
            renderLikeButton = (
                <TouchableOpacity onPress={() => this._onPressLike(topicData.id)} >
                    <View style={styles.boxButton}>
                        <FontAwesome5 name="thumbs-up" size={15} color="gray" />
                        <Text style={[styles.numbers, {'color': '#000'}]}>Like</Text>
                    </View>
                </TouchableOpacity>
            )
        }

        return (
            <KeyboardAvoidingView style={styles.container} behavior={Platform.OS === "ios" ? "padding" : "height"} keyboardVerticalOffset={90} >
                <>
                <SafeAreaView style={{ flex: 1 }}>
                <ScrollView
                    ref={ref => this.scrollView = ref}
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}
                >
                    <View style={styles.row}>
                        <View style={styles.rowHeader}>
                            <View style={styles.panelLeft}>
                                <TouchableOpacity onPress={() => this._onPressUser(this.state.topicData.author)} >
                                    <UserAvatar size={40} name={this.state.topicData.author.name} src={getSmallThumb(this.state.topicData.author.avatar)} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.panelRight}>
                                <View style={styles.itemAuthor}>
                                    <UserBadges badgeList={this.state.badges} badgeIds={this.state.topicData.author.badge_id} />
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity onPress={() => this._onPressUser(this.state.topicData.author)} style={{ marginRight: 5 }}>
                                            <Text style={[styles.itemName]}>{this.state.topicData.author.name}</Text>
                                        </TouchableOpacity>
                                        {this.state.topicData.author.is_staff && (
                                            <View style={styles.wrapStaffBadge}>
                                                <Text style={styles.staffText}>{this.state.settingsForum.staff_text}</Text>
                                            </View>
                                        )}
                                        {this.state.topicData.pinned && (
                                            <View style={{ marginLeft: 5, justifyContent: 'center' }}>
                                                <FontAwesome5 name="thumbtack" solid size={13} color="gray" />
                                            </View>
                                        )}
                                    </View>
                                    <Moment fromNow element={Text} style={[styles.itemDescText]}>{this.state.topicData.created}</Moment>
                                </View>

                                <View style={styles.rightWidget}>
                                    <OptionsTopic
                                        topicData={this.state.topicData}
                                        navigation={this.props.navigation}
                                        accessedForum={this.state.accessedForum}
                                        accessToken={this.state.accessToken}
                                        loggedUser={this.state.loggedUser}
                                        openSendReport={_item => this._openSendReport(_item)}
                                        refreshTopicsData={() => this.props.navigation.replace('Stories')}
                                    />
                                </View>
                            </View>
                        </View>

                        <StoryItemData
                            content={this.state.topicData.content}
                            images={this.state.topicData.images}
                            videos={this.state.topicData.videos}
                            products={this.state.topicData.products}
                            embeds={this.state.topicData.embeds}
                            poll={this.state.topicData.poll}
                        />

                        <View style={styles.rowFooter}>
                            <View style={styles.boxRowHaft}>
                                {this.state.topicData.like_count ? (
                                    <TouchableOpacity onPress={() => this._viewLikesList(this.state.topicData.id)} >
                                        <View style={[styles.flexRowInside, styles.padVerticalMd]}>
                                            <View style={styles.likeBubble}>
                                                <FontAwesome5 name="thumbs-up" solid size={10} color="#fff" />
                                            </View>
                                            <Text style={styles.numbers}>{renderLikesList}</Text>
                                        </View>
                                    </TouchableOpacity>
                                ): null}
                            </View>

                            <View style={styles.boxRowHaft}>
                                <View style={[styles.boxColumnRight, styles.padVerticalMd]}>
                                    <Text style={styles.numbers}>{this.state.topicData.no_posts} Comments</Text>
                                </View>
                            </View>
                        </View>

                        <View style={[styles.rowFooter, styles.rowFooterBtn]}>
                            <View style={styles.boxRowHaft}>
                                {renderLikeButton}
                            </View>

                            <View style={styles.boxRowHaft}>
                                <TouchableOpacity onPress={() => this._onPressComment()} >
                                    <View style={styles.boxButton}>
                                        <FontAwesome5 name="comment-alt" size={15} color="gray" />
                                        <Text style={styles.numbers}>Comment</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.boxRowHaft}>
                                <TouchableOpacity onPress={this._onPressShare} >
                                    <View style={styles.boxButton}>
                                        <FontAwesome5 name="share-alt" size={15} color="gray" />
                                        <Text style={styles.numbers}>Share</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    <View style={{height: 1, backgroundColor: '#dedede'}} />

                    <View style={ styles.commentsBox }>
                        {this.state.topicData.no_posts > 2 && this.state.topicData.no_posts > totalLoadedComment && this.state.isLoadMoreShow ? (
                            <View>
                                <TouchableOpacity onPress={this._onPressLoadMoreComment}>
                                    <View style={styles.loadMore}>
                                        <Text style={{ color: '#007bff', fontSize: 16 }}>Load more previous comments</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        ) : null}

                        <View style={{ marginBottom: 10 }}>
                            {this.state.commentList.map(item => (
                                <MyCommentItem
                                    key={'comment_' + item.id}
                                    id={item.id}
                                    content={item.content}
                                    created={item.created}
                                    replies_count={item.replies_count}
                                    replies={item.replies || []}
                                    images={item.images || []}
                                    author={item.author || []}
                                    likes={item.likes || []}
                                    like_count={item.like_count}
                                    loggedUser={this.state.loggedUser}
                                    shop={this.state.accessedForum.shop}
                                    like_shopify_cid={item.like_shopify_cid}
                                    openSendReport={_item => this._openSendReport(_item)}
                                    onPressUser={(data) => this._onPressUser(data)}
                                    onPressPostLike={(data) => this._onPressPostLike(data)}
                                    onPressLoadMoreReply={(_item_id, _reply_page) => this._onPressLoadMoreReply(_item_id, _reply_page)}
                                    onPressReply={(_item) => this._onPressReply(_item)}
                                    badgeList={this.state.badges}
                                    settingsForum={this.state.settingsForum}
                                    editComment={(data) => this._editComment(data)}
                                    deleteComment={(data) => this._deleteComment(data)}
                                    accessedForum={this.state.accessedForum}
                                    accessToken={this.state.accessToken}
                                />
                            ))}
                        </View>
                    </View>
                    <View style={{ height: 55 }}></View>
                </ScrollView>

                <View>
                    <NewComment
                        ref={el => this.newCommentRef = el}
                        updateCommentsData={(_data) => this._updateCommentsData(_data)}
                        accessedForum={this.state.accessedForum}
                        accessToken={this.state.accessToken}
                        loggedUser={this.state.loggedUser}
                        postId={this.state.postId}
                        initialFocus={this.state.initialFocus}
                        comment={this.state.post || null}
                        actionType={this.state.actionType || ''}
                        cancelComment={this.state.cancelComment || false}
                        onCloseCreateComment={() => this._onCloseCreateComment()}
                    />
                </View>

                <ModalReport
                    modalVisible={this.state.modalVisible}
                    setModalVisible={_state => this.setModalVisible(_state)}
                    accessedForum={this.state.accessedForum}
                    accessToken={this.state.accessToken}
                    loggedUser={this.state.loggedUser}
                    object_id={this.state.report_object_id}
                    type={this.state.report_type}
                />
                </SafeAreaView>

                <Portal>
                    <ModalLikesList ref={el => this.modalizeRef = el} data={this.state.likesList} />
                </Portal>
                </>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        // minHeight: '100%',
        // flexDirection: 'column'
    },
    loadingScreen: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        padding: 10
    },
    row: {
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
    rowHeader: {
        flex: 1,
        flexDirection: 'row',
    },
    panelLeft: {
        height: 40,
        width: 40,
        paddingTop: 0,
    },
    panelRight: {
        flex: 1,
        flexDirection: 'row',
        position: 'relative'
    },
    padVerticalMd: {
        paddingVertical: 10,
    },
    wrapStaffBadge: {
        borderRadius: 4,
        backgroundColor: '#dce0e2',
        alignItems: 'center'
    },
    staffText: {
        fontSize: 12,
        color: '#5d707f',
        paddingHorizontal: 7,
        paddingVertical: 2
    },
    itemAuthor: {
        paddingLeft: 16,
        paddingVertical: 3,
    },
    rightWidget: {
        position: 'absolute',
        top: -3,
        right: -10,
        zIndex: 99,
    },
    itemName: {
        fontSize: 16,
        fontWeight: '500',
        color: '#007bff',
    },
    itemDescriptions: {
        flexDirection: 'row',
    },
    itemDescText: {
        fontSize: 15,
        color: '#999',
    },
    // body
    separator: {
        height: 1,
        width: '100%',
        backgroundColor: '#e9e9e9',
    },
    highShort: {
        height: 10,
    },
    commentsBox: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        paddingVertical: 0,
    },
    rowFooter: {
        width: '100%',
        flexDirection: 'row',
    },
    rowFooterBtn: {
        borderTopWidth: 1,
        borderTopColor: '#dedede',
        marginBottom: -5,
    },
    boxButton: {
        flexDirection: 'row',
        paddingVertical: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    boxColumnRight: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-end'
    },
    boxRowHaft: {
        flex: 1,
    },
    flexRowInside: {
        flex: 1,
        flexDirection: 'row',
    },
    numbers: {
        paddingTop: 1,
        paddingLeft: 6,
        fontSize: 15,
    },
    loadMore: {
        padding: 10,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    likeBubble: {
        backgroundColor: "#007bff",
        paddingVertical: 4,
        paddingHorizontal: 5,
        borderRadius: 50,
        height: 20,
    },
});
