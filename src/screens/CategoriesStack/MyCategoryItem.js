
import React, { Component } from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';


function CatIcon( props ) {
    if (!props.icon) return null;
    let arrIcon = props.icon.split(' ');
    if (arrIcon.length === 0) return null;
    let icon = arrIcon[1].substring(3);

    switch (arrIcon[0]) {
        case 'fas':
            return (
                <FontAwesome5 name={icon} solid size={props.size || 20} color={props.color} />
            )
        case 'fab':
            return (
                <FontAwesome5 name={icon} brand size={props.size || 20} color={props.color} />
            )
        default:
            return (
                <FontAwesome5 name={icon} size={props.size || 20} color={props.color} />
            )
    }
}

export default class MyCategoryItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            onCollapse: false,

        };
        this._onPressCollapseButton = this._onPressCollapseButton.bind(this);
    }

    _onPress = (_category : Object) => {
        this.props.onPressItem(_category);
    };

    _onPressCollapseButton() {
        this.setState(previousState => ({
            onCollapse: !previousState.onCollapse
        }));
    }

    render() {
        const { children } = this.props;
        const totalTopics = this.props.topics_count + children.reduce((total, item) => total + item.topics_count, 0);

        return (
            <View>
                <View style={styles.container}>
                    <View style={styles.containerInner}>
                        <View style={styles.row}>
                            {!!this.props.icon && (
                                <View style={styles.panelLeft}>
                                    <View style={styles.itemIcon}>
                                        <CatIcon color={ this.props.color } icon={ this.props.icon } />
                                    </View>
                                </View>
                            )}
                            <View style={styles.panelRight}>
                                <TouchableOpacity onPress={() => this._onPress(this.props)} style={{ flex: 1, flexDirection: 'row', paddingVertical: 12, paddingRight: children.length ? 5 : 12, justifyContent: 'space-between'}}>
                                    <View style={styles.itemDetails}>
                                        <Text style={[styles.itemName]}>{this.props.title}</Text>
                                    </View>
                                    {totalTopics ? (
                                        <View style={styles.badgeCount}>
                                            {this.state.onCollapse ? (
                                                <Text style={styles.badgeCountText}>{this.props.topics_count}</Text>
                                            ) : (
                                                <Text style={styles.badgeCountText}>{totalTopics}</Text>
                                            )}
                                        </View>
                                    ) : null}
                                </TouchableOpacity>
                                {children.length ? (
                                    <TouchableOpacity style={styles.collapseButton} onPress={this._onPressCollapseButton}>
                                        <FontAwesome5 name="caret-down" solid size={20} color="#222" />
                                    </TouchableOpacity>
                                ) : null}
                            </View>
                        </View>
                    </View>

                    {children.length && this.state.onCollapse ? (
                        <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#fff', paddingLeft: 30}}>
                        {
                            children.map((item, index) => (
                                <TouchableOpacity onPress={() => this._onPress(item)} key={'sub_category_' + index}>
                                    <View style={styles.containerInner}>
                                        <View style={styles.row}>
                                            {!!item.icon && (
                                                <View style={styles.panelLeft}>
                                                    <View style={styles.itemIcon}>
                                                        <CatIcon color={ item.color } icon={ item.icon } />
                                                    </View>
                                                </View>
                                            )}
                                            <View style={[styles.panelRight, {paddingVertical: 12, paddingRight: 12, justifyContent: "space-between"}]}>
                                                <View style={styles.itemDetails}>
                                                    <Text style={[styles.itemName]}>{item.title}</Text>
                                                </View>
                                                {(item.topics_count > 0) && (
                                                    <View style={styles.badgeCount}>
                                                        <Text style={styles.badgeCountText}>{item.topics_count}</Text>
                                                    </View>
                                                )}
                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            ))
                        }
                        </View>
                    ) : null}

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#e9e9e9',
        paddingLeft: 15,
    },
    containerInner: {
        flex: 1,
        flexDirection: 'row',
    },
    row: {
        flex: 1,
        flexDirection: 'row',
    },
    panelLeft: {
        height: 'auto',
        width: 35,
        paddingVertical: 15,
    },
    panelRight: {
        flex: 1,
        flexDirection: 'row',
        // marginLeft: 10,
        // width: '100%',
    },
    itemIcon: {
        paddingVertical: 0,
        alignItems: 'flex-start',
    },
    itemDetails: {
        paddingVertical: 4,
    },
    itemName: {
        fontSize: 16,
        // paddingBottom: 3,
    },
    badgeCount: {
        height: 24,
        width: 24,
        paddingHorizontal: 4,
        paddingVertical: 4,
        borderWidth: 1,
        borderColor: 'white',
        backgroundColor: '#007bff',
        borderRadius: 8,
        // position: 'absolute',
        // top: 8,
        // right:10,
        // zIndex: 99,
    },
    badgeCountText: {
        fontSize: 12,
        color: 'white',
        textAlign: 'center',

    },
    collapseButton: {
        backgroundColor: 'rgba(203, 203, 203, 0.18)',
        paddingVertical: 12,
        paddingHorizontal: 18,
    }
});

