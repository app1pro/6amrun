
import React from "react";
import {View, Text, FlatList, StyleSheet, Alert, AppState, RefreshControl, TouchableOpacity } from "react-native";
import {name as APP_NAME, API_TOKEN_KEY, USER_SAVE_KEY, STORAGE_KEY, BASE_URL_API} from '../../../app.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import MyCategoryItem from './MyCategoryItem';
import Loader from '../../components/Loader';

export default class Categories extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            appState: AppState.currentState,
            refreshing: false,
            isLoading: true,
            accessToken: null,
            accessedForum: {},
            currentUser: false,
            categoriesList: []
        }

		this.subscription = AppState.addEventListener('change', this._handleAppStateChange);
    }

    componentDidMount() {
        this.subscription.remove();

        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData !== undefined && this.userData.customer !== undefined) {
                    this.setState({
                      isLoading: false,
                      accessedForum: this.userData.customer,
                    });

                    this._onRefresh();
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            // Alert.alert('App has come to the foreground!');
//            this._onRefresh();
        }
        this.setState({appState: nextAppState});
    }

    _onRefresh = () => {
        this.setState({refreshing: true});

        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.get(BASE_URL_API + '/categories.json', configHeaders)
        .then(res => {
            // console.log(res);
            let data = res.data;
            this.setState({refreshing: false});

            if (data && data.categories !== undefined) {
                this.setState({
                    categoriesList: data.categories,
                });

                try {
                    AsyncStorage.getItem(STORAGE_KEY)
                    .then((user_data_json) => {
                        let storageData = JSON.parse(user_data_json);
                        storageData.categories = data.categories;
                        AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(storageData));
                    });
                } catch (error) {
                    console.log('AsyncStorage error: ' + error.message);
                }
            } else {
                console.log('Reload fail', data.error);
            }
        })
        .catch(error => {
            console.log(error);
        });
    }

    _onPressItem (_category: Object) {
        return this.props.navigation.navigate('CategoriesDetail', {categoryId: _category.id, title: _category.title});
    };

    _renderItem = ({item}) => (
        <MyCategoryItem
            id={item.id}
            title={item.title}
            slug={item.slug}
            icon={item.icon}
            color={item.color || '#666'}
            topics_count={item.topics_count}
            avatar={item.avatar}
            image={item.image}
            children={item.children}
            onPressItem={_category => this._onPressItem(_category)}
        />
    );

    render() {
        return (
          <View style={{ flex: 1, backgroundColor: '#fff'}}>
            <Loader loading={this.state.isLoading} />

            <FlatList
                data={this.state.categoriesList}
                extraData={this.state}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={this._renderItem}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                  />
                }
            />
          </View>
        );
    }
}
