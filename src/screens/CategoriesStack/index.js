
import * as React from 'react';
import Categories from './Categories';
import CategoriesDetail from './CategoriesDetail';
import StoryDetail from '../StoriesStack/StoryDetail';
import { createStackNavigator } from '@react-navigation/stack';

const ContactStack = createStackNavigator();

function CategoriesStack() {
    return (
      <ContactStack.Navigator>
        <ContactStack.Screen name="Categories" component={Categories} />
        <ContactStack.Screen name="CategoriesDetail" component={CategoriesDetail} options={({ route }) => ({ title: route.params.title })}/>
        <ContactStack.Screen name="StoryDetail" component={StoryDetail} />
      </ContactStack.Navigator>
    );
}

export default CategoriesStack;
