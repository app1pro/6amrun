import React, { useRef, createRef } from "react";
import {View, Text, Alert, Image, FlatList, RefreshControl, TouchableOpacity, StyleSheet, Dimensions } from "react-native";
import {name as APP_NAME, USER_SAVE_KEY, STORAGE_KEY, BASE_URL_API} from '../../../app.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../../components/Loader';
import LoadingIcon from '../../components/LoadingIcon';
import StoryList from '../../components/StoryList';
import StoriesHeader from '../StoriesStack/StoriesHeader';
import PostOrder from '../../components/PostOrder';
import axios from 'axios';
import SubCategories from "../../components/SubCategories";
import { getMediumThumb } from "../../utils/global";
// import moment from "moment";

const screenWidth = Dimensions.get('window').width

export default class CategoriesDetail extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            refreshing: false,
            isLoading: true,
            isLoadingMore: false,
            accessedForum: {},
            settingsForum: {},
            badges: [],
            loggedUser: {},
            categoryInfo: false,
            dataList: [],
            page: 1,
            categoryId: null,
            accessToken: null,
            sortOrder: null,
            imgHeight: 100,
        }
    }

    componentDidMount() {
        const { categoryId } = this.props.route.params;

        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData && this.userData.customer) {
                    // console.log('AsyncStorage', this.userData);
                    this.setState({
                        categoryId: categoryId,
                        isLoading: false,
                        accessedForum: this.userData.customer,
                        loggedUser: this.userData.loggedUser,
                        settingsForum: this.userData.settings,
                        badges: this.userData.badges,
                    });

                    this._loadCategoryInfo();
                    this._onRefresh();
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _loadCategoryInfo = () => {
        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.get(BASE_URL_API + '/categories/' + this.state.categoryId + '.json', configHeaders)
        .then(res => {
            let data = res.data;

            if (data && data.category !== undefined) {
                this.setState({
                    categoryInfo: data.category,
                });

                if (data.category.image) {
                    this.updateImgSize(getMediumThumb(data.category.image))
                }
            } else {
                if (data.error !== undefined) {
                    Alert.alert(data.error.toString());
                }
            }
        })
        .catch(error => {
            Alert.alert('Get category info fail', error.response.data.message.toString());
        })
    }

    _onRefresh = () => {
        this.setState({refreshing: true});

        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        const queries = 'page=' + this.state.page + '&category_id=' + this.state.categoryId + '&sort=' + this.state.sortOrder;
        axios.get(BASE_URL_API + '/topics.json?' + queries, configHeaders)
        .then(res => {
            let data = res.data;
            this.setState({ refreshing: false, isLoadingMore: false });

            if (data && data.topics !== undefined && data.topics.length) {
                this.setState(previousState => ({
                    dataList: [...previousState.dataList, ...data.topics],
                    page: previousState.page + 1
                }));
            } else {
                // console.log('Reload fail', data.error);
                if (data.error !== undefined) {
                    Alert.alert(data.error.toString());
                }
            }
        })
        .catch(error => {
            console.log('nearby', error.response)
        })
    }

    _onRefreshNew = () => {
        if ( this.state.isLoading || this.state.refreshing || this.state.isLoadingMore) {
            return;
        }

        this.setState({
            dataList: [],
            page: 1,
        });

        setTimeout(() => {
            this._onRefresh();
        }, 200)
    }

    _updateTopicsData = (_topic) => {
        let { dataList } = this.state;
        dataList.unshift(_topic);
        this.setState({ dataList });
    }

    _loadMoreResults = () => {
        if ( this.state.isLoading || this.state.refreshing || this.state.isLoadingMore) {
            return;
        }

        this.setState({isLoadingMore: true});
        this._onRefresh();
    }

    pressAddNew() {
        return this.props.navigation.navigate('PostForm', {categoryId: this.state.categoryInfo.id, categoryName: this.state.categoryInfo.title });
    }

    onOrderChange = (_value) => {
        this.setState({
            sortOrder: _value,
            dataList: [],
            page: 1,
        });

        setTimeout(() => {
            this._onRefresh();
        }, 200)
    }

    updateImgSize = (img) => {
        Image.getSize(img, (width, height) => {
            // calculate image width and height 
            const scaleFactor = width / (screenWidth - 20);
            const imageHeight = height / scaleFactor;
            // setImgWidth(screenWidth)
            this.setState({ imgHeight: imageHeight })
        })
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Loader loading={this.state.isLoading} />
                <StoryList
                    data={this.state.dataList}
                    extraData={this.state}
                    loggedUser={this.state.loggedUser}
                    accessedForum={this.state.accessedForum}
                    accessToken={this.state.accessToken}
                    navigation={this.props.navigation}
                    updateData={dataList => this.setState({ dataList }) }
                    badges={this.state.badges}
                    settingsForum={this.state.settingsForum}
                    refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefreshNew}
                    />
                    }
                    ListEmptyComponent={!this.state.isLoading && !this.state.refreshing && (
                        <View style={{ padding: 15, alignContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: '#999' }}>No stories here</Text>
                        </View>
                    )}
                    onEndReachedThreshold={0.25}
                    onEndReached={({ distanceFromEnd }) => {
                        // if (distanceFromEnd > 0) {
                            this._loadMoreResults();
                        // }
                    }}
                    ListHeaderComponent={
                        <>
                            {this.state.categoryInfo && this.state.categoryInfo.image !== undefined && !!this.state.categoryInfo.image && (
                                <View style={{ marginTop: 10, marginHorizontal: 10 }}>
                                    <Image source={{ uri: getMediumThumb(this.state.categoryInfo.image)}} style={{ width: '100%', height: this.state.imgHeight }}/>
                                </View>
                            )}
                            <SubCategories categoryList={this.state.categoryInfo && this.state.categoryInfo.children ? this.state.categoryInfo.children : null}/>
                            <StoriesHeader authenUser={this.state.loggedUser} onPressAdd={event => this.pressAddNew()} />
                            <PostOrder onChange={ this.onOrderChange } />
                        </>
                    }
                />

                {this.state.isLoadingMore && (
                    <LoadingIcon />
                )}
            </View>
        );
    }
}
