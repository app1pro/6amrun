
import React from "react";
import { Button, View, Text, Image, StyleSheet, TextInput, TouchableOpacity, ActivityIndicator, Alert, ScrollView } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {STORAGE_KEY, USER_SAVE_KEY, BASE_URL_API} from '../../../app.json';
import LinearGradient from 'react-native-linear-gradient';

export default class CheckOTP extends React.Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);

        this.state = {
            loading : false,
            security_code: null
        };
    }

    componentDidMount() {
        this.setState({
          loading: false,
        });
    }

    _onPressLogin (event) {
        return this.props.navigation.navigate('Login');
    }

    _onPressSubmit (event) {
        let { security_code } = this.state;
        if (!security_code) {
            Alert.alert('Please fill your Security code!');
            return false;
        }

        return this.props.navigation.navigate('NewPassword');
        
        this.setState({
          loading: true,
        });

        axios.post(BASE_URL_API + '/register.json', {
            security_code: security_code,
        })
        .then(res => {
            let data = res.data;
            this.setState({ loading: false });
            
            if (data && data.user !== undefined){
                AsyncStorage.setItem(USER_SAVE_KEY, username);
                AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(data))
                .then(() => {
                    return this.props.navigation.navigate('SetAvatar');
                });
            } else {
                if (data.error !== undefined) {
                    Alert.alert('Register fail', data.error.toString());
                } else {
                    Alert.alert('Register fail!', `Something went wrong!`);
                }
            }
        })
        .catch(error => {
            console.log('register fail', error.response)
            this.setState({ loading: false })
        })
    }
    
  render() {
    return (
        <View style={styles.container}>
            <View style={styles.logo} >
                <Image source={require('../../../assets/logo.png')} style={{width: 60, height: 60}} />
            </View>

            <Text style={styles.header}>Enter Your Security Code</Text>
            
            <View style={ styles.description }><Text>Enter the 6-digit verification code we sent to the email addess ...</Text></View>

            <View style={styles.inputWrapper}>
                <TextInput
                    style={styles.input}
                    placeholder="******"
                    maxLength={6}
                    keyboardType="number-pad"
                    value={this.state.security_code}
                    onChangeText={(security_code) => this.setState({ security_code })}
                />
            </View>
            
            <TouchableOpacity activeOpacity={.5} onPress={this._onPressSubmit.bind(this)} keyboardShouldPersistTaps={true}>
                <LinearGradient start={{x: 0.0, y: 1}} end={{x: 0.5, y: 0}} colors={['#499bea', '#0066ff']} style={styles.button}>
                    <Text style={styles.buttonText}>Submit</Text>
                </LinearGradient>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={.5} onPress={this._onPressLogin.bind(this)} keyboardShouldPersistTaps={true}>
                <Text style={styles.linkText}>Didn't receive your code? Check your spam filter or Get a new one.</Text>
            </TouchableOpacity>


            <View style={styles.loadingBox}>
                <ActivityIndicator
                animating={this.state.loading} />
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
        paddingHorizontal: 15,
    },
    logo: {
        marginTop: 30,
        marginBottom: 30,
    },
    header: {
        fontSize: 25,
        marginBottom: 30,
    },
    description: {
        marginBottom: 35,
    },
    label: {
        textAlign: 'left',
        alignItems: 'flex-start',
    },
    inputWrapper: {
        flexDirection: 'row',
        marginVertical: 10,
    },
    input:{
        width: '100%',
        padding: 15,
        textAlign: 'center',
        fontSize: 25,
    },
    captionText: {
        fontSize: 11,
        textAlign: 'right',
    },
    button: {
        width: 250,
        borderRadius: 20,
        backgroundColor:"#0066ff",
        paddingVertical: 10,
        marginVertical: 8,
        alignItems: "center",
        justifyContent: "center",
    },
    buttonText: {
        fontSize: 16,
        color:'#FFFFFF',
        textAlign: 'center',
    },
    link: {
        width: 250,
        paddingVertical: 8,
        marginVertical:8,
        alignItems: "center",
        justifyContent: "center",
    },
    linkText: {
        paddingVertical: 8,
        marginVertical: 8,
        fontSize: 16,
        color: '#333333',
        textAlign: 'center',
    },
    loadingBox: {
        marginTop: 30,
    }
});
