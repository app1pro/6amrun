
import * as React from 'react';

import SetForum from './SetForum';
import Login from './Login';
import FogotPassword from './FogotPassword';
import CheckOTP from './CheckOTP';
import NewPassword from './NewPassword';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

function AuthStack() {
    return (
      <Stack.Navigator>
        <Stack.Screen 
            name="SetForum"
            component={SetForum}
            options={{
                headerShown: false,
            }}
        />
        <Stack.Screen
            name="Login"
            component={Login} 
            options={{
                headerLeft: false,
            }}
        />
        <Stack.Screen name="FogotPassword" component={FogotPassword} />
        <Stack.Screen name="CheckOTP" component={CheckOTP} />
        <Stack.Screen name="NewPassword" component={NewPassword} />
      </Stack.Navigator>
    );
}

export default AuthStack;
