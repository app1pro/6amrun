
import React from "react";
import { Button, View, Text, Image, StyleSheet, TextInput, TouchableOpacity, ActivityIndicator, Alert, ScrollView } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {STORAGE_KEY, USER_SAVE_KEY, BASE_URL_API} from '../../../app.json';
import LinearGradient from 'react-native-linear-gradient';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#ecf0f1',
    },
    logo: {
        marginTop: 30,
        marginBottom: 30,
    },
    header: {
        fontSize: 25,
        marginBottom: 30,
    },
    inputWrapper: {
        marginBottom: 10,
    },
    input:{
        width: 250,
        padding: 10,
        paddingHorizontal: 15,
        borderWidth: 1,
        borderColor: '#0066ff',
        borderRadius: 20,
        fontSize: 16,
    },
    captionText: {
        fontSize: 11,
        textAlign: 'right',
    },
    button: {
        width: 250,
        borderRadius: 20,
        backgroundColor:"#0066ff",
        paddingVertical: 10,
        marginVertical: 8,
        alignItems: "center",
        justifyContent: "center",
    },
    buttonText: {
        fontSize: 16,
        color:'#FFFFFF',
        textAlign: 'center',
    },
    link: {
        width: 250,
        paddingVertical: 8,
        marginVertical:8,
        alignItems: "center",
        justifyContent: "center",
    },
    linkText: {
        paddingVertical: 8,
        marginVertical: 8,
        fontSize: 16,
        color: '#333333',
        textAlign: 'center',
    },
    loadingBox: {
        marginTop: 30,
    }
});

export default class Register extends React.Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);

        this.state = {
            loading : false,
            username: '',
            password: '',
            email: '',
            name: '',
            phone: ''
        };
    }

    componentDidMount() {
        this.setState({
          loading: false,
        });
    }

    _onPressLogin (event) {
        return this.props.navigation.navigate('Login');
    }

    _onPressSignUp (event) {
        let { username, password, name, phone } = this.state;
        if (!username || !password || !name) {
            Alert.alert('Please fill all of fields!');
            return false;
        }
        
        if (name.length < 5 || name.length > 30) {
            Alert.alert('Name must be from 5 to 30 characters!');
            return false;
        }

        if (username.length < 5 || username.length > 20) {
            Alert.alert('Username must be from 5 to 20 characters!');
            return false;
        }
        
        if (/\W/.test(username)) {
            Alert.alert('Username contain invalid characters! Allow letters, numbers, and underscores');
            return false;
        }

        if (password.length < 5 || password.length > 20) {
            Alert.alert('Password must be from 5 to 20 characters!');
            return false;
        }

        this.setState({
          loading: true,
        });

        username = username.toLowerCase();
        
        axios.post(BASE_URL_API + '/register.json', {
            username: username,
            password: password,
            name: name,
            phone: phone,
        })
        .then(res => {
            let data = res.data;
            this.setState({ loading: false });
            
            if (data && data.user !== undefined){
                AsyncStorage.setItem(USER_SAVE_KEY, username);
                AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(data))
                .then(() => {
                    return this.props.navigation.navigate('SetAvatar');
                });
            } else {
                if (data.error !== undefined) {
                    Alert.alert('Register fail', data.error.toString());
                } else {
                    Alert.alert('Register fail!', `Something went wrong!`);
                }
            }
        })
        .catch(error => {
            console.log('register fail', error.response)
            this.setState({ loading: false })
        })
    }
    
  render() {
    return (
        <ScrollView keyboardDismissMode="on-drag" contentContainerStyle={{flex:1}}>
            <View style={styles.container}>
                <View style={styles.logo} >
                    <Image source={require('../../../assets/logo.png')} style={{width: 60, height: 60}} />
                </View>

                <Text style={styles.header}>Register a new account</Text>
                
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={styles.input}
                        placeholder="Full name"
                        value={this.state.name}
                        onChangeText={(name) => this.setState({ name })}
                    />
                </View>
                
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={styles.input}
                        placeholder="Username"
                        value={this.state.username}
                        onChangeText={(username) => this.setState({ username })}
                    />
                </View>
            
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={styles.input}
                        placeholder="Password"
                        secureTextEntry={true}
                        value={this.state.password}
                        onChangeText={(password) => this.setState({ password })}
                        underlineColorAndroid="transparent"
                    />
                </View>
                
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={styles.input}
                        placeholder="Phone number"
                        value={this.state.phone}
                        onChangeText={(phone) => this.setState({ phone })}
                        keyboardType="phone-pad"
                    />
                </View>
                
                <TouchableOpacity activeOpacity={.5} onPress={this._onPressSignUp.bind(this)} keyboardShouldPersistTaps={true}>
                    <LinearGradient start={{x: 0.0, y: 1}} end={{x: 0.5, y: 0}} colors={['#499bea', '#0066ff']} style={styles.button}>
                        <Text style={styles.buttonText}>Register</Text>
                    </LinearGradient>
                </TouchableOpacity>

                <TouchableOpacity activeOpacity={.5} onPress={this._onPressLogin.bind(this)} keyboardShouldPersistTaps={true}>
                    <Text style={styles.linkText}>Have an account? Login!</Text>
                </TouchableOpacity>


                <View style={styles.loadingBox}>
                    <ActivityIndicator
                    animating={this.state.loading} />
                </View>
            </View>
      </ScrollView>
    );
  }
}
