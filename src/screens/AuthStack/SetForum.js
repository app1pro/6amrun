
import React from "react";
import {View, Text, Platform, StyleSheet, TextInput, TouchableOpacity, ActivityIndicator, Alert, ScrollView } from "react-native";
import {USER_SAVE_KEY, STORAGE_KEY, BASE_URL_API, FORUM_API_KEY, FORUM_SECRET, FORUM_DOMAIN} from '../../../app.json';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import CryptoJS from '../../utils/cryptoJS';

export default class SetForum extends React.Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);

        this.state = {
            loading : false,
            shop_domain: FORUM_DOMAIN,
        };
    }

    componentDidMount() {
        this.setState({
          loading: false,
        });
        
        try {
            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData && this.userData.customer) {
                    // console.log('AsyncStorage', this.userData);
                    return this.props.navigation.navigate('Login');
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }

        this._onPressLogin();
    }

    _onPressSignUp (event) {
        return this.props.navigation.navigate('Register');
    }

    getAccessToken (_customer, callback) {
        const timestamp = Math.floor(Date.now() / 1000);

        let query = [
            'client_id=' + _customer.api_key, 
            'platform=' + Platform.OS, 
            'shop=' + _customer.shop, 
            'timestamp=' + timestamp
        ];
        const hmac = CryptoJS.HmacSHA256(query.sort().join('&'), _customer.api_secret).toString();
    
        axios.get(BASE_URL_API + '/forums/access_token.json?' + query.sort().join('&') + '&hmac=' + hmac)
        .then(res => {
            let data = res.data;
            if (data && data.access_token !== undefined) {
                callback(data.access_token);
            } else {
                Alert.alert('Get Access Token fail!', `Something went wrong!`);
            }
        })
        .catch(error => {
            Alert.alert('Get Access Token fail', error.response.data.message.toString());
            this.setState({ loading: false })
        });
    }

    _onPressLogin (event) {
        let { shop_domain } = this.state;
        
        this.setState({
          loading: true,
        });

        if (!shop_domain) {
            this.setState({ loading: false });
            Alert.alert('Login fail', `Empty is not allowed!`);
            return;
        }
        
        shop_domain = shop_domain.toLowerCase();
        const timestamp = Math.floor(Date.now() / 1000);
        let query = 'client_id=' + FORUM_API_KEY + '&shop=' + shop_domain + '&timestamp=' + timestamp;
        const hmac = CryptoJS.HmacSHA256(query, FORUM_SECRET).toString();

        axios.get(BASE_URL_API + '/forums.json?' + query + '&hmac=' + hmac)
        .then(res => {
            let data = res.data;
            this.setState({ loading: false });
            
            if (data && data.customer !== undefined) {
                this.getAccessToken(data.customer, token => {
                    // console.log('token', token);
                    data.customer.token = token;

                    AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(data))
                    .then(() => {
                        return this.props.navigation.navigate('Login');
                    });
                });
            } else {
                if (data.error !== undefined) {
                    Alert.alert('Access fail', data.error.toString());
                } else {
                    Alert.alert('Access fail!', `Something went wrong!`);
                }
            }
        })
        .catch(error => {
            Alert.alert('Access fail', error.response.data.message.toString());
            this.setState({ loading: false })
        })
    }
    
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.header}>FORUM ENTRY</Text>
                
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={styles.input}
                        placeholder="Forum name or ID..."
                        value={this.state.shop_domain}
                        onChangeText={(shop_domain) => this.setState({ shop_domain })}
                    />
                </View>
            
                <TouchableOpacity activeOpacity={.5} onPress={this._onPressLogin.bind(this)} keyboardShouldPersistTaps={true}>
                    <LinearGradient start={{x: 0.0, y: 1}} end={{x: 0.5, y: 0}} colors={['#499bea', '#0066ff']} style={styles.button}>
                        <Text style={styles.buttonText}>Enter</Text>
                    </LinearGradient>
                </TouchableOpacity>

                <View style={styles.loadingBox}>
                    <ActivityIndicator
                    animating={this.state.loading} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    header: {
        fontSize: 25,
        marginBottom: 30,
        textAlign: 'center',
    },
    inputWrapper: {
        marginBottom: 10,
        paddingHorizontal: 15,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    input:{
        width: '100%',
        maxWidth: 400,
        padding: 12,
        borderBottomWidth: 0.5,
        borderColor: '#666',
        borderRadius: 5,
        fontSize: 16,
        backgroundColor: '#fff',
    },
    button: {
        width: 250,
        backgroundColor:"#0066ff",
        paddingVertical: 10,
        marginVertical: 8,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
    },
    buttonText: {
        fontSize: 16,
        color:'#FFFFFF',
        textAlign: 'center',
    },
    loadingBox: {
        marginTop: 30,
    }
});
