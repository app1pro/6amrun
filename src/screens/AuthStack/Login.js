
import React, { useState, useRef, createRef, useEffect } from "react";
import {View, Text, Image, StyleSheet, Platform, TouchableOpacity, ActivityIndicator, Alert, Linking, ScrollView } from "react-native";
import {
    STORAGE_KEY,
    USER_SAVE_KEY,
    BASE_URL_API,
    FORUM_API_KEY,
    FORUM_SECRET,
    NOTIF_TOKEN_KEY,
    USER_AGREEMENT_URL,
    PRIVACY_POLICY_URL
} from '../../../app.json';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import Loader from '../../components/Loader';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Portal } from 'react-native-portalize';
import { FacebookWebView } from '../../components/FacebookWebView';
import { AuthContext } from '../../utils/authContext';
import { useDispatch, useSelector } from 'react-redux';
// import CryptoJS from '../../utils/cryptoJS';

const genRanHex = size => [...Array(size)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');

const getJsonFromUrl = (query) => {
    if(query.indexOf('?') > -1) query = query.split('?')[1];
    var result = {};
    query.split("&").forEach(function(part) {
      var item = part.split("=");
      result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
}

export default function Login({navigation, route}) {

    const [loading, setLoading] = useState(false);
    const [fetching, setFetching] = useState(true);
    const [webViewUrl, setWebViewUrl] = useState(null);
    const [notificationToken, setNotificationToken] = useState(null);
    const [userData, setUserData] = useState(null);
    let modalizeRef2 = createRef(null).current;
    const dispatch = useDispatch();
    const { signIn } = React.useContext(AuthContext);

    const readData = async () => {
        try {
            let _notificationToken = await AsyncStorage.getItem(NOTIF_TOKEN_KEY);
            if (_notificationToken) {
                setNotificationToken( JSON.parse(_notificationToken) )
            }

            await AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                let _userData = JSON.parse(user_data_json);
                // console.log('_userData', _userData);
                if (_userData && _userData.customer) {
                    setFetching(false)
                    setUserData(_userData)
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    useEffect(() => {
        readData()
      }, [])

    const _onPressLoginSSO = () => {
        let random_code = genRanHex(64)
        let _webViewUrl = 'https://' + userData.customer.domain + '/account/login?checkout_url=' + userData.settings.path + '/authentication/index/' + random_code + '/platform/' + Platform.OS
        setWebViewUrl(_webViewUrl)

        modalizeRef2?.open();
    }

    const _onChangeUrl = url => {
        let base_forum_path = 'https://' + userData.customer.domain + userData.settings.path
        if (url.indexOf(base_forum_path + '/authentication/verified') > -1) {
            modalizeRef2?.close();
            let { session_id, customer_id } = getJsonFromUrl(url)

            setLoading(true);
            let postData = {
                session_id: session_id,
                customer_id: customer_id,
                platform: Platform.OS,
            };

            if (notificationToken !== null) {
                postData.device_token = notificationToken.token
            }

            const configHeaders = { headers: { Shop: userData.customer.shop, token: userData.customer.token } };
            axios.post(BASE_URL_API + '/verified.json', postData, configHeaders)
            .then(res => {
                // console.log('login', res.data);
                let data = res.data;
                setLoading(false);

                if (data && data.user !== undefined) {
                    userData.loggedUser = data.user;
                    userData.unread_notifications_count = data.unread_notifications_count;
                    userData.blockedUsers = data.blocked_users;

                    // AsyncStorage.setItem(USER_SAVE_KEY, this.state.customer.shop);
                    AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(userData))
                    .then(() => {
                        // Get access_token
                        AsyncStorage.setItem(USER_SAVE_KEY, userData.customer.token)
                        .then(() => {
                            dispatch({ type: 'ADD_CURRENT_USER', data: userData.loggedUser });
                            dispatch({ type: 'EMIT_USER', user_id: userData.loggedUser.shopify_cid, group_id: userData.customer.id });

                            if (userData.unread_notifications_count) {
                                dispatch({ type: 'UPDATE_NOTIFICATION', data: userData.unread_notifications_count });
                            }

                            signIn();
                        });
                    });
                } else {
                    if (data.error !== undefined) {
                        Alert.alert('Login fail', data.error.toString());
                    } else {
                        Alert.alert('Login fail!', `Something went wrong!`);
                    }
                }
            })
            .catch(error => {
                setLoading(false);
                Alert.alert('Login fail', error.response.data.message.toString());
            })
        }

    }

    if (fetching || !userData) {
        return (<Loader loading={fetching} />);
    }

    return (
        <View style={styles.container}>
            {userData.settings.logo && (
                <View style={styles.logo} >
                    <Image source={require('../../../assets/forum-icon-120px.png')} style={{width: 90, height: 90}} />
                </View>
            )}

            <View style={{ marginTop: 25 }}>
                <Text style={styles.forumName}>{userData.settings.name}</Text>
            </View>

            <Text style={styles.header}>Login to continue</Text>

            <View style={{ marginBottom: 30, paddingHorizontal: 20, maxWidth: 300, }}>
                <Text style={styles.description}>
                    By continuing, you agree to our
                    <TouchableOpacity style={styles.linking} onPress={ () => { Linking.openURL(USER_AGREEMENT_URL)}} >
                        <Text style={styles.linkingText}>User Agreement</Text></TouchableOpacity>
                    <Text> and </Text>
                    <TouchableOpacity style={styles.linking} onPress={ () => { Linking.openURL(PRIVACY_POLICY_URL)}} >
                        <Text style={styles.linkingText}>Privacy Policy</Text>
                    </TouchableOpacity>
                    .
                </Text>
            </View>

            <TouchableOpacity activeOpacity={.5} onPress={_onPressLoginSSO.bind(this)} keyboardShouldPersistTaps={true}>
                <LinearGradient start={{x: 0.0, y: 1}} end={{x: 0.5, y: 0}} colors={['#499bea', '#0066ff']} style={styles.button}>
                    <Text style={styles.buttonText}>Login via Store account <FontAwesome5 name="external-link-alt" size={16} color="white" solid /></Text>
                </LinearGradient>
            </TouchableOpacity>

            <View style={styles.mgTop}>
                <ActivityIndicator animating={loading} color="#00ed6b" />
            </View>

            <Portal>
                <FacebookWebView ref={el => modalizeRef2 = el} onChangeUrl={_onChangeUrl} url={webViewUrl} />
            </Portal>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    logo: {
        marginTop: 30,
        marginBottom: 0,
    },
    forumName: {
        fontSize: 25,
        marginBottom: 30,
    },
    header: {
        fontSize: 20,
        marginBottom: 30,
    },
    description: {
        fontSize: 15,
        color: '#777',
        textAlign: 'center',
    },
    linking: {
        padding: 0,
        marginBottom: -3,
    },
    linkingText: {
        color: '#0066ff',
    },
    button: {
        width: 250,
        backgroundColor:"#0066ff",
        paddingVertical: 10,
        marginVertical: 8,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
    },
    buttonText: {
        fontSize: 16,
        color: '#ffffff',
        textAlign: 'center',
    },
    backButton: {
        padding: 5,
        paddingHorizontal: 10,
        borderWidth: 0.5,
        borderColor: '#666',
        borderRadius: 20,
    },
    backButtonText: {
        fontSize: 15,
    },
    mgTop: {
        marginTop: 20,
    }
});
