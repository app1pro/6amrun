
import * as React from 'react';

import Settings from './Settings';
import EditProfile from './EditProfile';
import MembersList from './MembersList';
import BlockList from './BlockList';
import { createStackNavigator } from '@react-navigation/stack';

const SettingStack = createStackNavigator();

function SettingsStack({ navigation, route }) {
    return (
      <SettingStack.Navigator>
        <SettingStack.Screen name="Settings" component={Settings} />
        <SettingStack.Screen name="EditProfile" component={EditProfile} 
            options={{ title: 'Edit Profile' }}
        />
        <SettingStack.Screen name="MembersList" component={MembersList} 
            options={{ title: 'Member List' }}
        />
        <SettingStack.Screen name="BlockList" component={BlockList} 
            options={{ title: 'Block List' }}
        />
      </SettingStack.Navigator>
    );
}

export default SettingsStack;
