
import React from "react";
import {Platform, View, ScrollView, Image, Text, Linking, Alert, TouchableOpacity, StyleSheet } from "react-native";
import {name as APP_NAME, STORAGE_KEY, USER_SAVE_KEY, BASE_URL_API, BUG_REPORT_URL, APP_AUTHOR} from '../../../app.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import UserAvatar from 'react-native-user-avatar';
import Loader from '../../components/Loader';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import LinearGradient from 'react-native-linear-gradient';
import { AuthContext } from '../../utils/authContext';

export default class Settings extends React.Component {

    static contextType = AuthContext;

    constructor(props){
        super(props);
        this.state ={
            isLoading: true,
            accessedForum: {},
            settingsForum: {},
            loggedUser: {},
        }

        this.logOut = this._logOut.bind(this);
    }

    componentDidMount() {
        try {
            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData !== undefined && this.userData.loggedUser !== undefined) {
                    // console.log('AsyncStorage', this.userData);
                    this.setState({
                        isLoading: false,
                        accessedForum: this.userData.customer,
                        settingsForum: this.userData.settings,
                        loggedUser: this.userData.loggedUser,
                    });
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }

        this._refreshing = this.props.navigation.addListener('focus', () => {
            try {
                AsyncStorage.getItem(STORAGE_KEY)
                .then((user_data_json) => {
                    this.userData = JSON.parse(user_data_json);
                    if (this.userData !== undefined && this.userData.loggedUser !== undefined) {
                        this.setState({
                            loggedUser: this.userData.loggedUser,
                        });
                    }
                });
            } catch (error) {
                console.log('AsyncStorage error: ' + error.message);
            }
        });
    }

    componentWillUnmount() {
        this._refreshing();
    }

    _onPressLogout = () => {
        Alert.alert(
          'Logout',
          'Do you want to logout?',
          [
            {
              text: 'Cancel',
              style: 'cancel',
            },
            {
                text: 'OK',
                onPress: this.logOut
            },
          ],
          {cancelable: true},
        );
    }

    _toMyStory = () => {
        return this.props.navigation.navigate('UserProfiles', {userId: this.state.loggedUser.id} );
    }

    _onEditProfile = () => {
        return this.props.navigation.navigate('EditProfile');
    }

    _logOut = async () => {
        // const { signOut } = this.props.route.params;
        const { signOut } = this.context;
        this.setState({isLoading: true});

        const configHeaders = { headers: { Shop: this.userData.customer.shop, token: this.userData.customer.token } };
        await axios.post(BASE_URL_API + '/logout.json', {
            user_id: this.state.loggedUser.id,
            platform: Platform.OS,
        }, configHeaders)
        .then(res => {
            this.setState({isLoading: false});
            // console.log('logout', res.data);

            AsyncStorage.removeItem(USER_SAVE_KEY);
            signOut();
        })
        .catch(error => {
            this.setState({isLoading: false});
            Alert.alert('Logout fail', error.response.data.message.toString());
        });
    }

    render () {
        if (this.state.isLoading) {
            return (<Loader loading={this.state.isLoading} />);
        }

        return (
            <View style={styles.container}>
                <ScrollView>
                    <LinearGradient colors={['#499bea', '#0066ff']} style={styles.sectionHeader}>
                        <TouchableOpacity onPress={ this._toMyStory } >
                            <View>
                                <UserAvatar size={80} name={this.state.loggedUser.name} src={this.state.loggedUser.avatar} />
                            </View>
                        </TouchableOpacity>
                        <Text style={styles.sectionName}>
                          {this.state.loggedUser.name}
                        </Text>
                        <View style={styles.editButton}>
                            <TouchableOpacity onPress={ this._toMyStory } >
                                <View>
                                    <EntypoIcon name="chevron-small-right" size={20} color="#fff" />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </LinearGradient>

                        <View style={styles.navItem}>
                            <EntypoIcon name="user" size={16} color="#333" />
                            <Text style={styles.navItemName}>Email</Text>
                            <View style={styles.rightWidget}>
                                <Text>{this.state.loggedUser.email}</Text>
                            </View>
                        </View>

                    <View style={styles.separator} ></View>

                    <TouchableOpacity onPress={ () => { this.props.navigation.navigate('EditProfile') }} >
                        <View style={styles.navItem}>
                            <EntypoIcon name="v-card" size={16} color="#333" />
                            <Text style={styles.navItemName}>Edit profile</Text>
                            <View style={styles.rightWidget}>
                                <EntypoIcon name="chevron-small-right" size={20} color="#333" />
                            </View>
                        </View>
                    </TouchableOpacity>

{/*
                    <View style={styles.separator} ></View>
                    <TouchableOpacity onPress={ () => { this.props.navigation.navigate('SetPassword') }} >
                        <View style={styles.navItem}>
                            <EntypoIcon name="lock" size={16} color="#333" />
                            <Text style={styles.navItemName}>Change password</Text>
                            <View style={styles.rightWidget}>
                                <EntypoIcon name="chevron-small-right" size={20} color="#333" />
                            </View>
                        </View>
                    </TouchableOpacity> */}

                    <View style={styles.separator} ></View>

                    <TouchableOpacity onPress={ () => { this.props.navigation.navigate('MembersList') }} >
                        <View style={styles.navItem}>
                            <EntypoIcon name="users" size={16} color="#333" />
                            <Text style={styles.navItemName}>Member List</Text>
                            <View style={styles.rightWidget}>
                                <EntypoIcon name="chevron-small-right" size={20} color="#333" />
                            </View>
                        </View>
                    </TouchableOpacity>

                    <View style={styles.separator} ></View>

                    <TouchableOpacity onPress={ () => { this.props.navigation.navigate('BlockList') }} >
                        <View style={styles.navItem}>
                            <EntypoIcon name="remove-user" size={16} color="#333" />
                            <Text style={styles.navItemName}>Block List</Text>
                            <View style={styles.rightWidget}>
                                <EntypoIcon name="chevron-small-right" size={20} color="#333" />
                            </View>
                        </View>
                    </TouchableOpacity>

                    <View style={[styles.separator, styles.highShort]} ></View>

                    <TouchableOpacity onPress={ () => { Linking.openURL('https://' + this.state.accessedForum.domain)}} >
                        <View style={styles.navItem}>
                            <EntypoIcon name="home" size={16} color="#333" />
                            <Text style={styles.navItemName}>Homepage</Text>
                        </View>
                    </TouchableOpacity>

                    <View style={styles.separator} ></View>

                    <TouchableOpacity onPress={this._onPressLogout} >
                        <View style={styles.navItem}>
                            <EntypoIcon name="log-out" size={16} color="#cc0066" />
                            <Text style={styles.navItemName}>Logout</Text>
                        </View>
                    </TouchableOpacity>

                    <View style={[styles.separator, styles.highShort]} ></View>

                    <View style={styles.footer}>
                        <View>
                            <Text style={styles.footerText}>{APP_NAME} by {this.state.accessedForum.name}</Text>
                        </View>
                        <View style={{ paddingTop: 10, }}>
                            <TouchableOpacity onPress={ () => { Linking.openURL(BUG_REPORT_URL)}} >
                                <Text style={styles.navFooterText}>Bug report</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    sectionHeader: {
        paddingVertical: 20,
        alignItems: 'center',
        // backgroundColor: '#cccccc',
    },

    sectionName: {
        paddingTop: 10,
        fontWeight: 'bold',
        color: 'white',
        fontSize: 16,
    },
    editButton: {
        position: 'absolute',
        right: 10,
        bottom: 15,
        width: 30,
        height: 30,
        borderRadius: 5,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    separator: {
        height: 1.5,
        width: '100%',
        backgroundColor: '#e9e9e9',
    },

    highShort: {
        height: 10,
    },

    navItem: {
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 15,
        paddingHorizontal: 15,
        backgroundColor: 'transparent',
    },

    navItemName: {
        paddingLeft: 12,
        fontSize: 16,
    },
    itemGray: {
        color: 'gray',
    },

    rightWidget: {
        position: 'absolute',
        top: 0,
        right: 0,
        paddingRight: 10,
        paddingVertical: 15,
        zIndex: 99,
        flex: 1,
        flexDirection: 'row',
    },

    footer: {
        padding: 20,
    },
    footerText: {
        fontSize: 12,
        color: '#909090',
    },
    navFooterText: {
        fontSize: 12,
        color: '#666',
    },
});
