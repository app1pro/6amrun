import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet, Dimensions } from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import UserAvatar from 'react-native-user-avatar';
import OnlineUserDot from '../../components/OnlineUserDot';
import { getSmallThumb } from "../../utils/global";

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default function MemberItem (props) {
    return (
        <View style={styles.container}>
            <View style={styles.memberItem}>
                <TouchableOpacity onPress={() => props.onPressUser() } >
                    <View style={styles.cardHeader}>
                        <View style={ styles.avtWrapper }>
                            <UserAvatar size={100} name={props.name} src={getSmallThumb(props.avatar)} />
                            <OnlineUserDot shopify_cid={props.shopify_cid} offset={'79%'} />
                        </View>
                        <Text style={styles.memberName}>{props.name}</Text>
                        {props.country && (
                            <View style={styles.location}>
                                <FontAwesome5 name={'map-marker-alt'} solid size={14} color={'green'} />
                                <Text style={{color: 'green'}}>
                                    &nbsp;{props.country}
                                </Text>
                            </View>
                        )}
                    </View>
                </TouchableOpacity>
                <View style={styles.separator}></View>
                <View style={styles.cardBottom}>
                    <View style={styles.cardBottomLeft}>
                        <FontAwesome5 name={'pencil-alt'} solid size={18} color={'#6c757d'} />
                        <Text style={styles.textMuted}>&nbsp;{props.topics_count}</Text>
                    </View>
                    <View style={styles.cardBottomRight}>
                        <View style={styles.likeBubble}>
                            <FontAwesome5 name="thumbs-up" solid size={10} color="#fff" />
                        </View>
                        <Text style={styles.textMuted}>&nbsp;{props.like_count}</Text>
                    </View>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: windowWidth > 600 ? '33.3333%' : '50%',
        // flex: 1,
        // flexDirection: 'column',
    },
    memberItem: {
        marginTop: 10,
        marginHorizontal: 5,
        borderColor: '#e9e9e9',
        borderWidth: 1,
        borderRadius: 8,
        backgroundColor: '#fff',
    },
    cardHeader: {
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: 200,
        flexDirection: 'column'
    },
    avtWrapper: {
        height: 100,
        width: 100,
        marginBottom: 10,
    },
    memberName: {
        fontSize: 16,
    },
    location: {
        alignItems: 'center',
        'flexDirection': 'row'
    },
    cardBottom: {
        flex: 1,
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    cardBottomLeft: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "flex-start",
    },
    cardBottomRight: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: "flex-end",
    },
    likeBubble: {
        backgroundColor: "#007bff",
        paddingVertical: 4,
        paddingHorizontal: 5,
        borderRadius: 50,
        height: 20,
    },
    separator: {
        height: 1,
        width: '100%',
        backgroundColor: '#e9e9e9',
    },
    textMuted: {
        color: '#6c757d',
    }
});