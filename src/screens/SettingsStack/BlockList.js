import React, { Component } from "react";
import { View, Text, FlatList, StyleSheet, Alert, Dimensions } from "react-native";
import BlockItem from './BlockItem';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationActions } from '@react-navigation/compat';
import {name as APP_NAME, STORAGE_KEY, USER_SAVE_KEY, BASE_URL_API} from '../../../app.json';
import axios from 'axios';
import Loader from '../../components/Loader';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default class BlockList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            refreshing: false,
            isLoadingMore: false,
            canLoadMoreUser: true,
            membersList: [],
            blockedUsers: [],
        }
        this.page = 1;
    }

    componentDidMount() {
        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData && this.userData.customer) {
                    this.setState({
                        accessedForum: this.userData.customer,
                        loggedUser: this.userData.loggedUser,
                        blockedUsers: this.userData.blockedUsers,
                    });
                    this._onRefresh();

                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        const query = 'page=' + this.page + '&user_id=' + this.state.loggedUser.id;
        axios.get(BASE_URL_API + '/users/blocks.json?' + query, configHeaders)
        .then(res => {
            let data = res.data;
            this.setState({refreshing: false, isLoading: false, isLoadingMore: false})

            if (data && data.page_count && this.page < data.page_count) {
                if (!this.state.canLoadMoreUser) {
                    this.setState({canLoadMoreUser: true});
                }
            } else {
                this.setState({canLoadMoreUser: false});
            }

            if (data && data.users !== undefined && data.users.length) {
                if (this.page == 1) {
                    this.setState({membersList: data.users});
                } else {
                    this.setState(previousState => ({
                        membersList: [...previousState.membersList, ...data.users],
                    }));
                }
                this.page += 1;
            } else {
                if (this.state.membersList.length) {
                    this.setState({membersList: []});
                }
            }
        })
        .catch(error => {
            console.log(error.response);
            Alert.alert('ERROR!', error.response.data.message);
        });
    }

    doUnblock = (item) => {
        let postData = {
            user_id: this.state.loggedUser.id,
            entity_id: item.id,
        };

        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.post(BASE_URL_API + '/users/unblock.json', postData, configHeaders)
        .then(res => {
            // console.log('Sending data', res.data);
            let data = res.data;
            
            if (data && data.success === true) {
                this.setState(previousState => ({
                    membersList: previousState.membersList.filter(data => data.entity_id !== item.id),
                }));

                // Save blocked Users to storage.
                AsyncStorage.getItem(STORAGE_KEY)
                .then((user_data_json) => {
                    this.userData = JSON.parse(user_data_json);
                    this.userData.blockedUsers = this.userData.blockedUsers.filter(data => !(data.user_id === this.state.loggedUser.id && data.entity_id === item.id) )
                    AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(this.userData));
                });

            } else {
                if (data.error !== undefined) {
                    Alert.alert('Sending data fail', data.error.toString());
                } else {
                    Alert.alert('Sending data fail!', `Something went wrong!`);
                }
            }
        })
        .catch(error => {
            console.log('doUnblock', error.response);
        })
    }

    _unBlock = (user) => {
        Alert.alert(
          `Do you want to unblock ${user.name}?`,
          null,
          [
            {
              text: 'Cancel',
              style: 'cancel',
            },
            {
                text: 'OK',
                onPress: () => this.doUnblock(user)
            },
          ],
          {cancelable: true},
        );
    }

    _renderItem = ({item}) => (
        <BlockItem
            {...item.user}
            unBlock={() => {this._unBlock(item.user)}}
        />
    );

    _loadMoreResults = () => {
        if ( this.state.isLoading || this.state.refreshing || this.state.isLoadingMore || !this.state.canLoadMoreUser) {
            return;
        }
        this.setState({isLoadingMore: true});
        this._onRefresh();
    }

    render() {
        return (
            <View style={styles.container}>
                <Loader loading={this.state.isLoading || this.state.refreshing} />
                <FlatList
                    data={this.state.membersList}
                    keyExtractor={(item, index) => 'item_' + item.id}
                    renderItem={this._renderItem}
                    numColumns={1}
                    onEndReachedThreshold={0.25}
                    onEndReached={({ distanceFromEnd }) => {
                        // console.log('loading more...', distanceFromEnd);
                        if (distanceFromEnd > 0) {
                            this._loadMoreResults();
                        }
                    }}
                    ListFooterComponent={
                        <View style={{flex: 1, height: 20}}/>
                    }
                    ListEmptyComponent={!this.state.isLoading && !this.state.refreshing && (
                        <View style={styles.notFound}>
                            <Text style={styles.textNotFound}>Empty list!</Text>
                        </View>
                    )}
                />
            </View>
        );
    };
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        paddingHorizontal: 5,
    },
    header: {
        flexDirection: 'row',
        paddingHorizontal: 5,
        paddingVertical: 5,
        marginHorizontal: 5,
        marginVertical: 10,
        borderRadius: 50,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    panelLeft: {
        flex: 1,
        paddingLeft: 10,
    },
    panelRight: {
        width: 22,
        paddingTop: 0,
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    notFound: {
        alignItems: 'center',
        paddingVertical: 15,
    },
    textNotFound: {
        fontSize: 15,
        fontWeight: '500',
        color: 'gray'
    }
});