import React, { Component } from "react";
import { View, Text, FlatList, TouchableOpacity, TextInput, StyleSheet, Alert, Dimensions } from "react-native";
import MemberItem from './MemberItem';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationActions } from '@react-navigation/compat';
import {name as APP_NAME, STORAGE_KEY, USER_SAVE_KEY, BASE_URL_API} from '../../../app.json';
import axios from 'axios';
import Loader from '../../components/Loader';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default class MembersList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            refreshing: false,
            isLoadingMore: false,
            canLoadMoreUser: true,
            searchString: '',
            membersList: [],
        }
        this.page = 1;
    }

    componentDidMount() {
        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData && this.userData.customer) {
                    this.setState({
                        isLoading: false,
                        accessedForum: this.userData.customer,
                        loggedUser: this.userData.loggedUser,
                    });
                    this._onRefresh();

                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        const query = 'page=' + this.page + '&q=' + this.state.searchString;
        axios.get(BASE_URL_API + '/users/all.json?' + query, configHeaders)
        .then(res => {
            let data = res.data;
            this.setState({refreshing: false, isLoadingMore: false})

            if (data && data.page_count && this.page < data.page_count) {
                if (!this.state.canLoadMoreUser) {
                    this.setState({canLoadMoreUser: true});
                }
            } else {
                this.setState({canLoadMoreUser: false});
            }

            if (data && data.users !== undefined && data.users.length) {
                if (this.page == 1) {
                    this.setState({membersList: data.users});
                } else {
                    this.setState(previousState => ({
                        membersList: [...previousState.membersList, ...data.users],
                    }));
                }
                this.page += 1;
            } else {
                if (this.state.membersList.length) {
                    this.setState({membersList: []});
                }
            }
        })
        .catch(error => {
            console.log(error.response);
            Alert.alert('ERROR!', error.response.data.message);
        });
    }

    _onPressUser = (_userId) => {
        // return this.props.navigation.navigate('Stories', {
        //     screen: 'UserProfiles',
        //     params: {userId: _userId},
        //     key: 'Member_' + _userId
        // });

        const navigateAction = NavigationActions.navigate({
            routeName: 'UserProfiles',
            params: {userId: _userId},
            key: 'Member_' + _userId
        });

        return this.props.navigation.dispatch(navigateAction);
    }

    _onPressSearch = () => {
        if (this.state.isLoading || this.state.refreshing || this.state.isLoadingMore) {
            return;
        }

        if (this.state.searchString.length) {
            this.isSearching = true;
            this.page = 1;
        } else {
            if (this.isSearching) {
                this.isSearching = false;
                this.page = 1;
            } else {
                Alert.alert('Please enter a member name');
                return;
            }
        }
        this._onRefresh()
    }

    _renderItem = ({item}) => (
        <MemberItem
            {...item}
            onPressUser={() => {this._onPressUser(item.id)}}
        />
    );

    _loadMoreResults = () => {
        if ( this.state.isLoading || this.state.refreshing || this.state.isLoadingMore || !this.state.canLoadMoreUser) {
            return;
        }
        this.setState({isLoadingMore: true});
        this._onRefresh();
    }

    render() {
        return (
            <View style={styles.container}>
                <Loader loading={this.state.isLoading} />
                <View style={styles.header}>
                    <View style={styles.panelLeft}>
                        <TextInput style={{ height: 35, padding: 3 }} placeholder="Search members" onChangeText={(text) => {this.setState({searchString: String(text)})}} maxLength={40} />
                    </View>
                    <TouchableOpacity onPress={() => this._onPressSearch()} style={styles.panelRight}>
                        <FontAwesome5 name="search" size={20} color="#333" />
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={this.state.membersList}
                    keyExtractor={(item, index) => 'item_' + item.id}
                    renderItem={this._renderItem}
                    numColumns={windowWidth > 600 ? 3 : 2}
                    onEndReachedThreshold={0.25}
                    onEndReached={({ distanceFromEnd }) => {
                        // console.log('loading more...', distanceFromEnd);
                        if (distanceFromEnd > 0) {
                            this._loadMoreResults();
                        }
                    }}
                    ListFooterComponent={
                        <View style={{flex: 1, height: 20}}/>
                    }
                    ListEmptyComponent={!this.state.isLoading && !this.state.refreshing && (
                        <View style={styles.notFound}>
                            <Text style={styles.textNotFound}>Member not found!</Text>
                        </View>
                    )}
                />
            </View>
        );
    };
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        paddingHorizontal: 5,
    },
    header: {
        flexDirection: 'row',
        paddingHorizontal: 5,
        paddingVertical: 5,
        marginHorizontal: 5,
        marginVertical: 10,
        borderRadius: 50,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    panelLeft: {
        flex: 1,
        paddingLeft: 10,
    },
    panelRight: {
        width: 22,
        paddingTop: 0,
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    notFound: {
        alignItems: 'center',
        paddingVertical: 15,
    },
    textNotFound: {
        fontSize: 15,
        fontWeight: '500',
        color: 'gray'
    }
});