
import React from "react";
import {View, TextInput, Text, Alert, TouchableOpacity, StyleSheet, ScrollView } from "react-native";
import {name as APP_NAME, USER_SAVE_KEY, STORAGE_KEY, BASE_URL_API} from '../../../app.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export default class SetPassword extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Add/Change password'
        }
    };

    constructor(props){
        super(props);
        this.state ={
            isLoading: true,
            accessToken: null,
            loggedUser: {},
            oldpassword: '',
            password: '',
            password2: '',
        }
    }

    componentDidMount() {
        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData !== undefined && this.userData.customer !== undefined) {
                    this.setState({
                        isLoading: false,
                        accessedForum: this.userData.customer,
                        loggedUser: this.userData.loggedUser,
                    });
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _onPressSubmit (event) {
        let { oldpassword, password, password2 } = this.state;

        if (!oldpassword) {
            Alert.alert('Please enter your old password!');
            return;
        }

        if (!password || !password2) {
            Alert.alert('Please fill all of fields!');
            return;
        }

        if (password.length < 6 || password.length > 20) {
            Alert.alert('Password must be from 6 to 20 characters!');
            return;
        }

        if (password != password2) {
            Alert.alert('Two passwords are not equal!');
            return;
        }

        this.setState({
            loading: true,
        });

        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.post(BASE_URL_API + '/users/' + this.state.loggedUser.id + '/change-password.json', {
            oldpassword: oldpassword,
            password: password,
            password2: password2,
        }, configHeaders)
        .then(res => {
            let data = res.data;
            this.setState({ loading: false });

            if (data && (!!data.success || data.success == 'true')) {
                Alert.alert('Your password was updated successfully!');
                AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(data))
                .then(() => {
                    return this.props.navigation.navigate('Settings');
                });
            } else {
                Alert.alert(data.error);
            }
        });
    };

    render () {
        return (
            <ScrollView keyboardDismissMode="on-drag">
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.inputWrapper}>
                            <View style={styles.label}>
                                <Text style={styles.labelText}>Please fill all of the fields</Text>
                            </View>

                            <TextInput
                                style={styles.input}
                                placeholder="Current password"
                                value={this.state.oldpassword}
                                onChangeText={(oldpassword) => this.setState({ oldpassword })}
                                underlineColorAndroid="transparent"
                                secureTextEntry={true}
                            />

                            <TextInput
                                style={styles.input}
                                placeholder="New password"
                                value={this.state.password}
                                onChangeText={(password) => this.setState({ password })}
                                underlineColorAndroid="transparent"
                                secureTextEntry={true}
                            />
                            <TextInput
                                style={styles.input}
                                placeholder="New password again"
                                value={this.state.password2}
                                onChangeText={(password2) => this.setState({ password2 })}
                                underlineColorAndroid="transparent"
                                secureTextEntry={true}
                            />

                            <View style={styles.searchButton}>
                                <TouchableOpacity activeOpacity={.5} onPress={this._onPressSubmit.bind(this)} keyboardShouldPersistTaps={true}>
                                    <View style={styles.button}>
                                        <Text style={styles.buttonText}>Submit</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        paddingVertical: 10,
        backgroundColor: '#efefef',
    },
    inputWrapper: {
        padding: 10,
        backgroundColor: 'white',
    },
    labelText: {
        fontSize: 16,
    },
    input:{
        width: '100%',
        padding: 10,
        marginVertical: 10,
        borderWidth: .5,
        borderColor: '#999',
        fontSize: 16,
        borderRadius: 4,
        backgroundColor: '#f0f5f5',
    },
    searchButton: {
        alignItems: "center",
    },
    button: {
        backgroundColor:"#0066ff",
        paddingVertical: 8,
        paddingHorizontal: 20,
        marginVertical: 8,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 18,
    },
    buttonText: {
        fontSize: 16,
        color:'#FFFFFF',
        textAlign: 'center',
    },
    separator: {
        height: 1,
        backgroundColor: '#e9e9e9',
    },

    navItem: {
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 15,
        backgroundColor: 'transparent',
    },

    navLink: {
        paddingTop: 2,
        paddingLeft: 10,
        color: '#0066ff',
    },
});
