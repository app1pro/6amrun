import React, { Component } from "react";
import { Text, View, ScrollView, TextInput, TouchableOpacity, StyleSheet, Alert, ActivityIndicator } from "react-native";
import LinearGradient from 'react-native-linear-gradient';
import CheckBox from '@react-native-community/checkbox';
import RNPickerSelect from 'react-native-picker-select';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import UserAvatar from 'react-native-user-avatar';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {name as APP_NAME, STORAGE_KEY, USER_SAVE_KEY, BASE_URL_API} from '../../../app.json';
import axios from 'axios';
import ImagePicker from 'react-native-image-crop-picker';

export default class EditProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isSending: false,
            collapseButton: false,
            loggedUser: {},
            userData: {},
            localImage: null,
        }
    }

    componentDidMount() {
        const {navigation} = this.props;

        AsyncStorage.getItem(USER_SAVE_KEY)
        .then((accessToken) => {
            if (accessToken !== undefined) {
                this.setState({ accessToken });
            }
        });

        try {
            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData !== undefined && this.userData.loggedUser !== undefined) {
                    this.setState({
                        isLoading: false,
                        accessedForum: this.userData.customer,
                        loggedUser: this.userData.loggedUser,
                        userData: this.userData
                    });
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _onPressSave = () => {
        const form = new FormData();
        const { id, first_name, last_name, user_title, signature, city, province, country, location_display, notification_email, personal_page_restricted } = this.state.loggedUser;
        const { localImage } = this.state;

        if (!first_name) {
            Alert.alert('first_name must not be empty!');
            return false;
        }

        if (!last_name) {
            Alert.alert('last_name must not be empty!');
            return false;
        }

        this.setState({
            isSending: true,
        });

        form.append('first_name', first_name);
        form.append('last_name', last_name);
        form.append('user_title', user_title);
        form.append('signature', signature);
        form.append('city', city);
        form.append('province', province);
        form.append('country', country);
        form.append('location_display', location_display);
        form.append('notification_email', notification_email);
        form.append('personal_page_restricted', personal_page_restricted);

        if (localImage) {
            let filename = localImage.path.replace(/^.*[\\\/]/, ''); // works for both / and \

            form.append('avatar', {
                uri: localImage.path,
                type: localImage.mime,
                name: filename,
                size: localImage.size,
            });
        }

        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.post(BASE_URL_API + '/users/' + id + '/edit.json', form, configHeaders)
        .then(res => {
            let data = res.data;
            this.setState({ isSending: false });

            if (data && data.user !== undefined) {
                this.state.userData.loggedUser = data.user;
                Alert.alert('Your profile was updated successfully!');
                AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(this.state.userData))
                .then(() => {
                    return this.props.navigation.navigate('Settings');
                })
            } else {
                Alert.alert(data.error);
            }
        });
    };

    _onPressPhoto (event) {
        ImagePicker.openPicker({
            width: 600,
            height: 600,
            cropping: true,
            forceJpg: true,
        }).then(image => {
            this.setState({
                localImage: image,
            })
        })
        .catch(error => {
            // E_PERMISSION_MISSING
            // console.log('imagePicker error', error.code, error.message)
            if (error.code == 'E_PERMISSION_MISSING') {
                Alert.alert('Cannot update your avatar.');
            }
        })
    }

    _onPressCollapseButton = () => {
        this.setState(previousState => ({
            collapseButton: !previousState.collapseButton,
        }));
    }

    render() {
        return (
            <ScrollView
                ref={ref => this.scrollView = ref}
                keyboardDismissMode="on-drag"
            >
                <LinearGradient colors={['#499bea', '#0066ff']} style={styles.sectionHeader}>
                    <TouchableOpacity onPress={this._onPressPhoto.bind(this)} keyboardShouldPersistTaps={true}>
                        {this.state.loggedUser.name && (
                            <View style={{position: 'relative'}}>
                                <UserAvatar size={80} name={this.state.loggedUser.name} src={this.state.localImage ? this.state.localImage.path: this.state.loggedUser.avatar} />
                                <View style={{position: 'absolute', right: 0, bottom: 0, padding: 5, backgroundColor: '#f0f0f0', borderRadius: 50, borderWidth: .5, borderColor: '#6c757d' }}>
                                    <FontAwesome5 name="camera" solid size={16} color="gray" />
                                </View>
                            </View>
                        )}
                    </TouchableOpacity>
                    <Text style={styles.sectionName}>
                        {this.state.loggedUser.name}
                    </Text>
                </LinearGradient>
                <View style={styles.container}>
                    <View style={styles.sectionTop}>
                        <View>
                            <Text style={styles.label1}>First name</Text>
                            <TextInput
                                style={styles.input}
                                value={this.state.loggedUser.first_name}
                                onChangeText={(value) => {
                                    this.state.loggedUser.first_name = value;
                                    this.setState({
                                        loggedUser: this.state.loggedUser,
                                    })
                                    console.log(this.state.loggedUser.first_name);
                                }}
                                placeholder="First name"
                                underlineColorAndroid="transparent"
                            />
                        </View>

                        <View>
                            <Text style={styles.label1}>Last name</Text>
                            <TextInput
                                style={styles.input}
                                value={this.state.loggedUser.last_name}
                                onChangeText={(value) => {
                                    this.state.loggedUser.last_name = value;
                                    this.setState({
                                        loggedUser: this.state.loggedUser,
                                    })
                                }}
                                placeholder="Last name"
                                underlineColorAndroid="transparent"
                            />
                        </View>

                        <View>
                            <Text style={styles.label1}>Title</Text>
                            <TextInput
                                style={styles.input}
                                value={this.state.loggedUser.user_title}
                                onChangeText={(value) => {
                                    this.state.loggedUser.user_title = value;
                                    this.setState({
                                        loggedUser: this.state.loggedUser,
                                    })
                                }}
                                placeholder="Ex: Designer at..."
                                underlineColorAndroid="transparent"
                            />
                        </View>

                        <View>
                            <Text style={styles.label1}>About me</Text>
                            <TextInput
                                style={styles.input}
                                value={this.state.loggedUser.signature}
                                onChangeText={(value) => {
                                    this.state.loggedUser.signature = value;
                                    this.setState({
                                        loggedUser: this.state.loggedUser,
                                    })
                                }}
                                textAlignVertical={'top'}
                                multiline={true}
                                numberOfLines={3}
                                underlineColorAndroid="transparent"
                            />
                        </View>
                    </View>

                    <View style={styles.location}>
                        <TouchableOpacity style={styles.collapseWrapper} onPress={this._onPressCollapseButton}>
                            <View style={styles.collapseButton}>
                                <Text style={[styles.label1, {color: '#007bff', fontWeight: '700'}]}>Location&nbsp;</Text>
                                <FontAwesome5 name="caret-down" solid size={20} color="#007bff" />
                            </View>
                        </TouchableOpacity>
                        {this.state.collapseButton && (
                            <View style={styles.sectionBody}>
                            <View>
                                <Text style={styles.label1}>City</Text>
                                <TextInput
                                    style={styles.input}
                                    value={this.state.loggedUser.city}
                                    onChangeText={(value) => {
                                        this.state.loggedUser.city = value;
                                        this.setState({
                                            loggedUser: this.state.loggedUser,
                                        })
                                    }}
                                    underlineColorAndroid="transparent"
                                />
                            </View>

                            <View>
                                <Text style={styles.label1}>Province</Text>
                                <TextInput
                                    style={styles.input}
                                    value={this.state.loggedUser.province}
                                    onChangeText={(value) => {
                                        this.state.loggedUser.province = value;
                                        this.setState({
                                            loggedUser: this.state.loggedUser,
                                        })
                                    }}
                                    underlineColorAndroid="transparent"
                                />
                            </View>
                            <View>
                                <Text style={styles.label1}>Country</Text>
                                <TextInput
                                    style={styles.input}
                                    value={this.state.loggedUser.country}
                                    onChangeText={(value) => {
                                        this.state.loggedUser.country = value;
                                        this.setState({
                                            loggedUser: this.state.loggedUser,
                                        })
                                    }}
                                    underlineColorAndroid="transparent"
                                />
                            </View>

                            <View>
                                <Text style={styles.label1}>Show location</Text>
                                <View style={styles.optionsPickerWrapper}>
                                    <RNPickerSelect
                                        style={ pickerSelectStyles }
                                        value={this.state.loggedUser.location_display}
                                        onValueChange={(value) => {
                                            this.state.loggedUser.location_display = value;
                                            this.setState({
                                                loggedUser: this.state.loggedUser
                                            })
                                        }}
                                        items={[
                                            { label: 'None', value: 'none' },
                                            { label: 'Display full (city, province, country)', value: 'full' },
                                            { label: 'Country only', value: 'country-only' },
                                            { label: 'City only', value: 'city-only' },
                                            { label: 'City, Country', value: 'city-country' },
                                            { label: 'Province, Country', value: 'province-country' },
                                        ]}
                                    />
                                </View>
                            </View>
                        </View>
                        )}
                    </View>
                    <View style={styles.separator} />

                    <View style={styles.checkBoxWrapper}>
                        <Text style={[styles.label1, {fontWeight: '700'}]}>Notification</Text>
                        <View style={styles.checkBoxRow}>
                            <CheckBox
                                value={this.state.loggedUser.notification_email}
                                style={{ width: 20, height: 20 }}
                                boxType={'square'}
                                onValueChange={(value) => {
                                    this.state.loggedUser.notification_email = value;
                                    this.setState({
                                        loggedUser: this.state.loggedUser
                                    });
                                }}
                            />
                            <Text style={styles.label2}>Emails notifications</Text>
                        </View>
                        <Text style={styles.textMuted}>Get notified by email when someone replies to your post or mentions you in a post.</Text>
                    </View>
                    <View style={styles.separator} />

                    <View style={styles.checkBoxWrapper}>
                        <Text style={[styles.label1, {fontWeight: '700'}]}>Private</Text>
                        <View style={styles.checkBoxRow}>
                            <CheckBox
                                value={this.state.loggedUser.personal_page_restricted}
                                style={{ width: 20, height: 20 }}
                                boxType={'square'}
                                onValueChange={(value) => {
                                    this.state.loggedUser.personal_page_restricted = value;
                                    this.setState({
                                        loggedUser: this.state.loggedUser
                                    });
                                }}
                            />
                            <Text style={styles.label2}>Personal page restricted</Text>
                        </View>
                        <Text style={styles.textMuted}>Prevent others from viewing your personal information page. Note: this also prevents you from viewing other user's pages. </Text>
                    </View>
                    <View style={styles.separator} />

                    <View style={styles.sectionBottom}>
                        <View style={{flexDirection: 'row' }}>
                            <ActivityIndicator animating={this.state.isSending} color="#00ed6b" />
                            <TouchableOpacity disabled={this.state.isSending} onPress={() => this._onPressSave()} style={styles.button} >
                                <Text style={styles.buttonText}>Save</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            </ScrollView>
        );
    }
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 10,
        paddingHorizontal: 10,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
        width: '100%',
    },
    inputAndroid: {
        fontSize: 8,
        paddingVertical: 10,
        paddingHorizontal: 10,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
        width: '100%',
    },
  });

const styles = StyleSheet.create({
    sectionHeader: {
        paddingVertical: 20,
        alignItems: 'center',
    },
    sectionName: {
        paddingTop: 10,
        fontWeight: 'bold',
        color: 'white',
        fontSize: 16,
    },
    location: {
        paddingBottom: 15,
    },
    sectionBody: {
        paddingTop: 10,
    },
    collapseButton: {
        flexDirection: 'row',
    },
    checkBoxWrapper: {
        paddingVertical: 15,
    },
    checkBoxRow: {
        flexDirection: 'row',
        paddingVertical: 10,
    },
    label1: {
        fontSize: 16,
    },
    label2: {
        flex: 1,
        paddingLeft: 10,
        alignSelf: 'center',
        fontSize: 14
    },
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: 'white',
    },
    labelText: {
        fontSize: 16,
    },
    input:{
        width: '100%',
        padding: 10,
        marginVertical: 10,
        borderWidth: .5,
        borderColor: '#999',
        fontSize: 16,
        borderRadius: 4,
        backgroundColor: '#f0f5f5',
    },
    optionsPickerWrapper: {
        borderWidth: .5,
        borderColor: '#999',
        borderRadius: 4,
        marginVertical: 10,
        backgroundColor: '#f0f5f5',
    },
    sectionBottom: {
        alignItems: "center",
        marginVertical: 20,
    },
    button: {
        backgroundColor:"#0066ff",
        paddingVertical: 8,
        paddingHorizontal: 20,
        marginVertical: 8,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 18,
        minWidth: 110,
    },
    buttonText: {
        fontSize: 15,
        color:'#FFFFFF',
        textAlign: 'center',
    },
    separator: {
        height: 1,
        backgroundColor: '#e9e9e9',
    },

    navItem: {
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 15,
        backgroundColor: 'transparent',
    },

    navLink: {
        paddingTop: 2,
        paddingLeft: 10,
        color: '#0066ff',
    },
    separator: {
        height: 1.5,
        width: '100%',
        backgroundColor: '#e9e9e9',
    },
    textMuted: {
        color: '#6c757d',
    }
});
