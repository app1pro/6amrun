import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet, Dimensions } from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import UserAvatar from 'react-native-user-avatar';
import { getSmallThumb } from "../../utils/global";

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default function BlockItem (props) {
    return (
        <View style={styles.container}>
            <View style={styles.memberItem}>
                <View style={styles.cardBottom}>
                    <View style={styles.cardBottomLeft}>
                        <View style={ styles.avtWrapper }>
                            <UserAvatar size={25} name={props.name} src={getSmallThumb(props.avatar)} />
                        </View>
                        <Text style={styles.memberName}>{props.name}</Text>
                    </View>
                    <View style={styles.cardBottomRight}>
                        <TouchableOpacity onPress={() => props.unBlock()} >
                            <View style={styles.buttonBlock}>
                                <Text style={styles.textMuted}>Unblock</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            <View style={styles.separator}></View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flex: 1,
        flexDirection: 'column',
    },
    memberItem: {
        backgroundColor: '#fff',
    },
    avtWrapper: {
        height: 25,
        width: 25,
        marginRight: 10,
    },
    memberName: {
        fontSize: 16,
    },
    cardBottom: {
        flex: 1,
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingVertical: 15,
    },
    cardBottomLeft: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "flex-start",
    },
    cardBottomRight: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: "flex-end",
    },
    separator: {
        height: 1,
        width: '100%',
        backgroundColor: '#e9e9e9',
    },
    textMuted: {
        color: '#ff8000',
    },
    buttonBlock: {
        padding: 5,
        backgroundColor: '#fff5e6',
        borderRadius: 8,
    }
});