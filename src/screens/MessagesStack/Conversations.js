
import React, { Component } from 'react';
import { Platform, View, Text, FlatList, Alert, AppState, RefreshControl, StyleSheet} from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import {name as APP_NAME, STORAGE_KEY, USER_SAVE_KEY, BASE_URL_API} from '../../../app.json';
import ConversationItem from './ConversationItem';
import Loader from '../../components/Loader';
import LoadingIcon from '../../components/LoadingIcon';
import axios from 'axios';

export default class Conversations extends Component {
    constructor(props){
        super(props);
        this.state = {
            appState: AppState.currentState,
            refreshing: false,
            isLoading: true,
            isLoadingMore: false,
            accessedForum: {},
            loggedUser: {},
            threadsList: [],
            page: 1,
        }
    }

    componentDidMount() {
        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData && this.userData.customer) {
                    this.setState({
                        isLoading: false,
                        accessedForum: this.userData.customer,
                        loggedUser: this.userData.loggedUser,
                    });
                    this._onRefresh();
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _onRefresh = () => {
        this.setState({refreshing: true});

        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        const query = 'user_id=' + this.state.loggedUser.id + '&page=' + this.state.page;
        axios.get(BASE_URL_API + '/messages.json?' + query, configHeaders)
        .then(res => {
            let data = res.data;
            this.setState({refreshing: false, isLoadingMore: false})

            if (data && data.threads !== undefined && data.threads.length) {
                let { threadsList } = this.state;

                threadsList.forEach(thread => {
                    data.threads = data.threads.filter(_thread => _thread.id != thread.id);
                })

                this.setState(previousState => ({
                    threadsList: [...previousState.threadsList, ...data.threads],
                    page: previousState.page + 1
                }))
            }
        })
        .catch(error => {
            console.log(error.response);
            Alert.alert('ERROR!', error.response.data.message);
        });
    }

    _onRefreshNew = () => {
        if ( this.state.isLoading || this.state.refreshing || this.state.isLoadingMore) {
            return;
        }

        this.setState({
            threadsList: [],
            page: 1,
        })

        setTimeout(() => {
            this._onRefresh();
        }, 200)
    }

    _loadMoreResults = () => {
        if ( this.state.isLoading || this.state.refreshing || this.state.isLoadingMore) {
            return;
        }

        this.setState({isLoadingMore: true});
        this._onRefresh();
    }

    _onPressItem = (_thread: Object) => {
        if (!_thread.readn) {
            let { threadsList } = this.state;

            threadsList.map(thread => {
                if (thread.thread_id == _thread.thread_id) {
                    thread.readn = 1;
                }
                return thread;
            })

            this.setState({ threadsList });
        }

        return this.props.navigation.navigate('Messages', {threadId: _thread.thread_id, userId: _thread.user_id, title: _thread.title});
    };

    _onPressDelete = (_id: String) => {
        _key = this.state.threadsList.findIndex(_user => _user.id == _id);
        if (_key !== -1) {
            this.state.threadsList.splice(_key, 1);

            this.setState({
                threadsList: this.state.threadsList,
            });

            const options = {
                headers: { Authorization: this.state.loggedUser.api_token }
            };

            axios.delete(BASE_URL_API + '/thread-delete/'+_id+'.json', options);
        }
    };

    _renderItem = ({item}) => {
        return (
            <ConversationItem
                {...item}
                loggedUser={this.state.loggedUser}
                onPressthread={(_item) => this._onPressItem(_item)}
                onPressDelete={(_id) => this._onPressDelete(_id)}
            />
        );
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Loader loading={this.state.isLoading} />
                <FlatList
                    data={this.state.threadsList}
                    extraData={this.state}
                    keyExtractor={item => 'item_' + item.id}
                    renderItem={this._renderItem}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefreshNew}
                        />
                    }
                    onEndReachedThreshold={0.25}
                    onEndReached={({ distanceFromEnd }) => {
                        if (distanceFromEnd > 0) {
                            this._loadMoreResults();
                        }
                    }}
                    ListEmptyComponent={
                        !this.state.isLoading && !this.state.refreshing ? (
                                <View style={{ padding: 15, alignContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: '#999' }}>No messages here</Text>
                                </View>
                        ) : null
                    }
                />

                {this.state.isLoadingMore && (
                    <LoadingIcon />
                )}
            </View>
        );
    }
}