
import React from "react";
import { View, Text, Alert, StyleSheet, TouchableOpacity } from "react-native";
import UserAvatar from "react-native-user-avatar";
import Moment from 'react-moment';
import runes from "runes";
import OnlineUserDot from '../../components/OnlineUserDot';
import { getSmallThumb } from "../../utils/global";

export default class ConversationItem extends React.PureComponent {
    constructor(props){
        super(props);
    }

    _onPress = () => {
        this.props.onPressthread(this.props);
    };

    _deleteAlert = () => {
        Alert.alert(
            `Do you want to delete the messages with ${this.props.name}?`,
            '',
            [
              {
                text: 'Cancel',
                style: 'cancel',
              },
              {
                  text: 'OK',
                  onPress: () => this.props.onPressDelete(this.props.thread_id)
              },
            ],
            {cancelable: true},
        );
    }

    truncateString(_string, max_length = 20) {
        _string = _string.replace(/<\/?("[^"]*"|'[^']*'|[^>])*(>|$)/g, "").replace(/&nbsp;/g, " ");
        
        if (_string.length > max_length) {
            // Runes
            return runes.substr(_string, 0, max_length)+'...'
        }
        return _string
    }

    render() {
        let partner = this.props.loggedUser.id == this.props.user_id ? this.props.author : this.props.user;

        return (
            <TouchableOpacity onPress={this._onPress}>
                <View style={[ styles.container, (this.props.loggedUser.id == this.props.user_id && !this.props.readn ? styles.containerActive : null) ]}>
                    <View style={styles.panelLeft}>
                        <UserAvatar size={40} name={partner.name} src={getSmallThumb(partner.avatar)} />
                            <OnlineUserDot shopify_cid={partner.shopify_cid} />
                    </View>
                    <View style={styles.panelRight}>
                        <View style={styles.itemDetails}>
                            <Text style={[ styles.itemName, !this.props.readn ? styles.textStrong: null ]}>{partner.name}</Text>
                            <View style={styles.row}>
                                {this.props._total > 1 ? (
                                    <Text style={[ styles.totalMessage, !this.props.readn ? styles.textStrong: null ]}>{"(" + this.props._total + ")"} </Text>
                                ) : null}
                                {this.props.title.length ? (
                                    <Text style={[ styles.itemName, !this.props.readn ? styles.textStrong: null ]}>{this.props.title}</Text>
                                ) : null}
                            </View>
                            {this.props.content.length ? (
                                <Text style={styles.itemDescriptions}>
                                {this.props.author.id == this.props.loggedUser.id ? (
                                    <Text style={styles.mentionLabel}>You: </Text>
                                ) : null}
                                    {this.truncateString(this.props.content, 60)}
                                </Text>
                            ) : null}
                        </View>

                        <View style={styles.rightWidget}>
                            <View>
                                {this.props.created && (
                                    <Moment fromNow ago element={Text} style={[styles.itemDescriptions]}>{this.props.created}</Moment>
                                )}
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 8,
        paddingHorizontal: 10,
        backgroundColor: 'white',
    },
    row: {
        flex: 1,
        flexDirection: 'row'
    },
    containerActive: {
        backgroundColor: 'lightyellow',
    },
    panelLeft: {
        height: 50,
        width: 50,
        paddingRight: 10,
    },
    panelRight: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#e9e9e9',
    },
    rightWidget: {
        position: 'absolute',
        top: 0,
        right: 0,
        zIndex: 99,
        alignItems: 'flex-end',
    },
    itemDetails: {
        flex: 1,
        flexDirection: 'column',
        paddingBottom: 10,
    },
    textStrong: {
        fontWeight: '600',
    },
    itemName: {
        fontSize: 15,
        paddingBottom: 3,
    },
    totalMessage: {
        fontSize: 15,
        paddingBottom: 3,
    },
    mentionLabel: {
        fontSize: 15,
        color: 'gray',
    },
    itemDescriptions: {
        flex: 1,
        flexWrap: 'wrap',
        fontSize: 15,
        color: '#666',
    },
});

