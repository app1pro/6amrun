
import React, {Component} from 'react';
import { StyleSheet, Text, View, Alert, TouchableOpacity, ScrollView, KeyboardAvoidingView, SafeAreaView, Image } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {USER_SAVE_KEY, STORAGE_KEY, BASE_URL_API, MAX_FILE_SIZE} from '../../../app.json';
import axios from 'axios';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ImagePicker from 'react-native-image-crop-picker';
import MessageItem from "./MessageItem";
import { RichEditor } from 'react-native-new-rich-editor';

export default class Messages extends Component {
    constructor(props) {
        super(props);

        this.state = {
            threadId: props.route.params.threadId,
            userId: props.route.params.userId,
            title: props.route.params.title,
            messages: [],
            localImages: [],
            refreshing: false,
            loggedUser: {},
            blockedUsers: [],
            accessToken: null,
            accessedForum: {},
            sending: false,
            content: null,
            isBlocked: false,
        };
    }

    componentDidMount() {
        this.props.navigation.setOptions({
            title: this.props.route.params.title,
        });

        try {
            AsyncStorage.getItem(USER_SAVE_KEY)
            .then((accessToken) => {
                if (accessToken !== undefined) {
                    this.setState({ accessToken });
                }
            });

            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData !== undefined && this.userData.customer !== undefined) {
                    let isBlocked = this.userData.blockedUsers
                    && this.userData.blockedUsers.length
                    && this.userData.blockedUsers.filter(data => 
                        (data.user_id === this.userData.loggedUser && data.entity_id === this.state.userId)
                        || (data.user_id === this.state.userId && data.entity_id === this.userData.loggedUser)
                    )

                    this.setState({
                        isLoading: false,
                        accessedForum: this.userData.customer,
                        loggedUser: this.userData.loggedUser,
                        blockedUsers: this.userData.blockedUsers,
                        isBlocked: isBlocked,
                    });

                    this._onRefresh();
                }
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _onRefresh = () => {
        this.setState({refreshing: true});

        const configHeaders = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        const query = 'user_id=' + this.state.userId + '&thread_id=' + this.state.threadId;
        axios.get(BASE_URL_API + '/messages/details.json?' + query, configHeaders)
        .then(res => {
            let data = res.data;
            this.setState({refreshing: false});

            if (data && data.messages !== undefined) {
                this.setState({
                    messages: data.messages,
                });

                // Wait for JSX render
                setTimeout( () => this.scrollView.scrollToEnd({ animated: true }), 100)
            };
        })
        .catch(error => {
            // console.log(error.response);
            Alert.alert('ERROR!', error.response.data.message);
        });
    }

    _sendPost = () => {
        const form = new FormData();
        let { content, threadId, userId, title, localImages } = this.state;

        if (content !== null && content !== '') {
            content = content.trim();
        }

        form.append('thread_id', threadId);
        form.append('user_id', userId);
        form.append('from_id', this.state.loggedUser.id);
        form.append('content', content);
        form.append('title', title);

        localImages.forEach(item => {
            let filename = item.path.replace(/^.*[\\\/]/, '');
            form.append('images[]', {
                uri: item.path,
                type: item.mime,
                name: filename,
                size: item.size,
            });
        });

        this.setState({
            sending: true,
        })

        const configHeaders = { headers: {
            Shop: this.state.accessedForum.shop,
            token: this.state.accessToken,
            'Content-Type': 'multipart/form-data',
        } };
        axios.post(BASE_URL_API + '/messages/send-message.json', form, configHeaders)
        .then(res => {
            let {data} = res;

            if (data.success) {
                let {messages} = this.state;
                messages.push(data.message);

                this.setState(previousState => ({
                    localImages: [],
                    content: null,
                    messages: messages
                }));
                this.richtext.setContentHTML('');

                // Wait for JSX render
                setTimeout( () => this.scrollView.scrollToEnd({ animated: true }), 100)
            }

            this.setState({
                sending: false,
            })
        })
        .catch(error => {
            this.setState({
                sending: false,
            })
            console.log('add message error', error.response)
        })
    }

    uniqueImage = (_images) => {
        return _images.filter((value, pos, arr) =>  arr.findIndex(item => item.path.replace(/^.*[\\\/]/, '') == value.path.replace(/^.*[\\\/]/, '')) === pos )
    }

    _onPressPhoto = (event) => {
        ImagePicker.openPicker({
            multiple: true,
            maxFiles: 10,
        }).then(images => {
            this.setState(previousState => ({
                localImages: this.uniqueImage(previousState.localImages.concat(images)),
            }));
        })
        .catch(error => {
            if (error.code == 'E_PERMISSION_MISSING') {
                Alert.alert('Cannot access your gallery', 'You have to go to Settings to enable it.');
            }
        })
    }

    removeImage = (_localIdentifier) => {
        this.setState(previousState => ({
            localImages: previousState.localImages.filter(_item => _item.localIdentifier !== _localIdentifier)
        }))
    }

    render() {
        let renderSendBtn;
        if ( !!this.state.content && this.state.content.trim() && !this.state.sending ) {
            renderSendBtn = (
                <TouchableOpacity onPress={this._sendPost}>
                    <View style={styles.sendInside}>
                        <Ionicons name="md-send" size={25} color="#0066ff" />
                    </View>
                </TouchableOpacity>
            )
        } else {
            renderSendBtn = (
                <View style={styles.sendInside}>
                    <Ionicons name="md-send" size={25} color="gray" />
                </View>
            )
        }

        return (
            <KeyboardAvoidingView style={styles.container} behavior={Platform.OS === "ios" ? "padding" : "height"} keyboardVerticalOffset={90}>
                <SafeAreaView style={{ flex: 1 }}>
                    <ScrollView
                        ref={ref => this.scrollView = ref}
                        showsVerticalScrollIndicator={false}
                        style={{ flex: 1 }}
                    >
                        <View style={{ flex: 1, paddingBottom: 30 }}>
                            {this.state.messages.map(item => (
                                <MessageItem
                                    key={'message_' + item.id}
                                    user={item.user}
                                    author={item.author}
                                    content={item.content}
                                    title={item.title}
                                    images={item.images || []}
                                    created={item.created}
                                    readn={item.readn}
                                    loggedUser={this.state.loggedUser}
                                />
                            ))}
                        </View>
                        <View style={{ height: 50 }}></View>
                    </ScrollView>

                    <View>
                        <View style={styles.rowNewComment} >
                            {this.state.localImages.length > 0 ? (
                                <View style={styles.mediaBox}>
                                    <ScrollView horizontal={true}>
                                        <View style={styles.scrollImagesBox}>
                                        {this.state.localImages.map((_item, _index) => (
                                            <View style={styles.imageBox} key={'image_' + _index}>
                                                <Image source={{ uri: _item.path}} style={{width: 60, height: 60}} />
                                                <View style={styles.imgClose}>
                                                    <TouchableOpacity onPress={() => this.removeImage(_item.localIdentifier) }>
                                                        <View>
                                                            <Ionicons name="md-close" size={18} color="white" />
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        ))}
                                        </View>
                                    </ScrollView>
                                </View>
                            ) : null}

                            {this.state.isBlocked ? (
                                <View style={styles.rowInputInner}>
                                    <View style={{ alignItems: 'center', flex: 1 }}>
                                        <Text style={{ color: '#999' }}>This action is not available.</Text>
                                    </View>
                                </View>
                            ): (
                                <View style={styles.rowInputInner}>
                                <TouchableOpacity onPress={() => this._onPressPhoto()}>
                                    <View style={ styles.btnImage }>
                                        <FontAwesome5 name="image" solid size={25} color="gray" />
                                    </View>
                                </TouchableOpacity>
                                <ScrollView style={ styles.editorWrapper }>
                                    <RichEditor
                                        ref={(r) => this.richtext = r}
                                        initialHeight={25}
                                        containerStyle={{ borderWidth: 1, borderColor: 'transparent' }}
                                        editorStyle={{ backgroundColor: '#f0f0f0' }}
                                        initialContentHTML={this.state.content}
                                        onChange={content => {this.setState({ content })}}
                                    />
                                </ScrollView>
                                <View style={styles.rowSend}>
                                    {renderSendBtn}
                                </View>
                            </View>
                            )}
                        </View>
                    </View>
                </SafeAreaView>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    rowNewComment: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'white',
    },
    rowInputInner: {
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 5,
        paddingHorizontal: 5,
        borderTopWidth: 1,
        borderTopColor: '#e9e9e9',
    },
    editorWrapper: {
        flex: 1,
        backgroundColor: '#f0f0f0',
        borderWidth: 0.5,
        borderColor: '#e0e0e0',
        maxHeight: 150
    },
    scrollImagesBox: {
        flexDirection: 'row',
    },
    mediaBox: {
        padding: 10,
        backgroundColor:"#f9f9f9",
    },
    imageBox: {
        marginHorizontal: 5,
    },
    btnImage: {
        width: 35,
        padding: 3,
        paddingTop: 8,
    },
    imgClose: {
        position: 'absolute',
        right: 3,
        top: 3,
        width: 20,
        height: 20,
        alignContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'white',
        backgroundColor: 'rgba(0, 0, 0, .5)',
    },
    rowSend: {
        width: 35,
    },
    sendInside: {
        paddingVertical: 6,
        paddingHorizontal: 5,
    },
    input: {
        paddingVertical: 5,
        paddingHorizontal: 12,
        borderWidth: 0,
        borderColor: '#99c2ff',
        borderRadius: 4,
        fontSize: 15,
        backgroundColor: '#f0f5f5',
    },
});