import React, { useState } from "react";
import { Text, View, Image, TouchableOpacity, StyleSheet } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import UserAvatar from "react-native-user-avatar";
import Moment from 'react-moment';
import HTML from 'react-native-render-html';
import ImageView from 'react-native-image-viewing';
import {USER_SAVE_KEY, STORAGE_KEY, BASE_URL_API, MAX_FILE_SIZE} from '../../../app.json';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { getMediumThumb, getSmallThumb } from "../../utils/global";

const styles = StyleSheet.create({
    row: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        paddingTop: 8,
        paddingHorizontal: 10,
        marginBottom: 10,
    },
    panelLeft: {
        height: 50,
        width: 50,
        paddingRight: 10,
    },
    panelRight: {
        flex: 1,
        flexDirection: 'column',
    },
    rightWidget: {
        position: 'absolute',
        top: 0,
        right: 10,
        paddingTop: 10,
        zIndex: 99,
        alignItems: 'flex-end',
    },
    messageTitle: {
        fontSize: 15,
        fontWeight: '700',
        color: '#007bff'
    },
    itemDetails: {
        // flex: 1,
        // flexDirection: 'column',
        paddingVertical: 15,
        paddingHorizontal: 8,
        backgroundColor: 'rgba(0,0,0,.07)',
        borderRadius: 8,
    },
    itemName: {
        color: '#007bff',
        fontSize: 12,
        fontWeight: '500',
    },
    itemTime: {
        fontSize: 12,
        color: '#999',
    },
    iconTrash: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    likeButton: {
        // color: '#007bff',
        fontSize: 12
    },
    replyButton: {
        color: '#007bff',
        fontSize: 12
    },
    bullet: {
        fontSize: 12,
        color: '#939393',
        paddingHorizontal: 5
    },
    loadMore: {
        padding: 10,
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
    },
    itemFooter: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 5,
        paddingHorizontal: 5
    },
    likeWrapper: {
        flex: 1,
        alignItems: 'flex-end',
        textAlign: 'right',
        marginTop: -10,
    },
    likeIcon: {
        flexDirection: 'row',
        paddingVertical: 3,
        paddingHorizontal: 5,
        borderWidth: 1,
        borderColor: '#e0e0e0',
        borderRadius: 50,
        backgroundColor: "#fff",
    },
    likeBubble: {
        backgroundColor: "#007bff",
        paddingVertical: 4,
        paddingHorizontal: 5,
        marginRight: 5,
        borderRadius: 50,
        height: 20,
    },
    messageImage : {
        width: 180,
        height: 101,
    }
});

export default function MessageItem ( props ) {
    const [imageCurrentIndex, setImageIndex] = useState(0);
    const [isImageViewVisible, setModalViewer] = useState(false);

    const showImage = (image) => {
        setImageIndex( props.images.findIndex(_item => _item.id == image.id) );
        setModalViewer( true );
    }

    let imagesViewer;

    if (props.images !== undefined && props.images.length > 0) {
        imagesViewer = props.images.map(_image => ({
            id: _image.id,
            uri: getMediumThumb(_image.url),
        }))
    }

    return (
        <View>
            {props.author.id == props.loggedUser.id ? (
                <View style={[styles.row, {justifyContent: "flex-end"}]}>
                    <View style={[styles.panelRight, { maxWidth: '80%'}]}>
                        <View style={{ paddingBottom: 5 }}>
                            <Text style={styles.messageTitle}>{props.title}</Text>
                        </View>
                        {props.images.length ? (
                            <View style={{ paddingBottom: 5 }}>
                                {
                                    props.images.map((image, index) => (
                                        <TouchableOpacity onPress={() => showImage(image)} key={'image_' + image.id}>
                                            <Image source={{ uri: image.url }} style={styles.messageImage} />
                                        </TouchableOpacity>
                                    ))
                                }
                                <ImageView
                                    images={imagesViewer}
                                    imageIndex={imageCurrentIndex}
                                    animationType={'slide'}
                                    visible={isImageViewVisible}
                                    onRequestClose={() => setModalViewer(false)}
                                />
                            </View>
                        ) : null}
                        <View style={[styles.itemDetails]}>
                            <View>
                                <HTML
                                    source={{ html: props.content }}
                                    baseFontStyle={{ fontSize: 15 }}
                                    tagsStyles={{
                                        p: { marginBottom: 0, }
                                    }}
                                    parentWrapper={View}
                                />
                            </View>
                        </View>
                        <View style={styles.itemFooter}>
                            <Text style={[styles.itemName, {color: '#333'}]}>{props.loggedUser.name}</Text>
                            <Text style={styles.bullet}>•</Text>
                            <Moment fromNow ago element={Text} style={[styles.itemTime]}>{props.created}</Moment>
                            {props.readn && (
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.bullet}>•</Text>
                                    <Text style={{ color: '#28a745', fontSize: 12 }}><FontAwesome5 name="check" size={12} color="#28a745" solid /> Seen</Text>
                                </View>
                            )}
                        </View>
                    </View>
                </View>
                ) : (
                <View style={[styles.row, { maxWidth: '80%'}]}>
                    <View style={styles.panelLeft}>
                        <TouchableOpacity onPress={() => console.log('props.onPressPartner')} >
                            <UserAvatar size={40} name={props.author.name} src={getSmallThumb(props.author.avatar)} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.panelRight}>
                        <View style={{ paddingBottom: 5 }}>
                            <Text style={styles.messageTitle}>{props.title}</Text>
                        </View>
                        {props.images.length ? (
                            <View style={{ paddingBottom: 5 }}>
                                {
                                    props.images.map((image, index) => (
                                        <TouchableOpacity onPress={() => showImage(image)} key={'image_' + image.id}>
                                            <Image source={{ uri: image.url }} style={styles.messageImage} />
                                        </TouchableOpacity>
                                    ))
                                }
                                <ImageView
                                    images={imagesViewer}
                                    imageIndex={imageCurrentIndex}
                                    animationType={'slide'}
                                    visible={isImageViewVisible}
                                    onRequestClose={() => setModalViewer(false)}
                                />
                            </View>
                        ) : null}
                        <View style={[styles.itemDetails, {alignSelf: 'flex-start'}]}>
                            <HTML
                                source={{ html: props.content }}
                                baseFontStyle={{ fontSize: 15 }}
                                tagsStyles={{
                                    p: { marginBottom: 0, }
                                }}
                                parentWrapper={View}
                            />
                        </View>
                        <View style={styles.itemFooter}>
                            <TouchableOpacity onPress={() => console.log('props.onPressPartner')} >
                                <Text style={[styles.itemName]}>{props.author.name}</Text>
                            </TouchableOpacity>
                            <Text style={styles.bullet}>•</Text>
                            <Moment fromNow ago element={Text} style={[styles.itemTime]}>{props.created}</Moment>
                        </View>
                    </View>
                </View>
            )}
        </View>
    );
}