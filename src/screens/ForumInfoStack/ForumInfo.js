import React, { useRef, createRef } from "react";
import {View, Text, Image, ScrollView, FlatList, RefreshControl, TouchableOpacity, StyleSheet } from "react-native";
import {name as APP_NAME, USER_SAVE_KEY, STORAGE_KEY, BASE_URL_API} from '../../../app.json';
import AsyncStorage from '@react-native-async-storage/async-storage';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import HTML from 'react-native-render-html';
import Loader from '../../components/Loader';
import axios from 'axios';
import Moment from "react-moment";
import clip from "text-clipper";
import OnlineUserDot from '../../components/OnlineUserDot';
import UserAvatar from "react-native-user-avatar";
import runes from "runes";
import { getSmallThumb } from "../../utils/global";

export default class ForumInfo extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            isLoading: true,
            accessedForum: {},
            accessToken: null,
            loggedUser: {},
            settingsForum: null,
            lastestTopics: [],
            newestUsers: [],
            mostLikedAuthors: [],
            mostLikedAuthorsMore: [],
            viewMoreAuthors: false,
            viewMoreDescription: false,
        }
    }

    componentDidMount() {
        AsyncStorage.getItem(USER_SAVE_KEY)
        .then((accessToken) => {
            if (accessToken !== undefined) {
                this.setState({ accessToken });
            }
        });

        try {
            AsyncStorage.getItem(STORAGE_KEY)
            .then((user_data_json) => {
                this.userData = JSON.parse(user_data_json);
                if (this.userData !== undefined && this.userData.loggedUser !== undefined) {
                    this.setState({
                        // isLoading: false,
                        accessedForum: this.userData.customer,
                        // loggedUser: this.userData.loggedUser,
                    });
                }
                this._refreshData();
            });
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    }

    _refreshData() {
        const options = { headers: { Shop: this.state.accessedForum.shop, token: this.state.accessToken } };
        axios.get(BASE_URL_API + '/informations.json', options)
        .then(res => {
            let { data } = res;
            if (data) {
                let { settings, badges, latest_topics, newest_users, most_liked_users, most_liked_users_more } = data;

                this.setState({
                    settingsForum: settings,
                })

                if (latest_topics) {
                    this.setState({
                        lastestTopics: latest_topics,
                    })
                }
                if (newest_users) {
                    this.setState({
                        newestUsers: newest_users,
                    })
                }
                if (most_liked_users) {
                    this.setState({
                        mostLikedAuthors: most_liked_users,
                    })
                }
                if (most_liked_users_more) {
                    this.setState({
                        mostLikedAuthorsMore: most_liked_users_more,
                    });
                }

                // Update Forum's badges and settings
                try {
                    AsyncStorage.getItem(STORAGE_KEY)
                    .then((user_data_json) => {
                        let _userData = JSON.parse(user_data_json);
                        if (_userData !== undefined) {
                            _userData.settings = settings;
                            _userData.badges = badges;
                            AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(_userData))
                        }
                    });
                } catch (error) {
                    console.log('AsyncStorage error: ' + error.message);
                }
            }
            this.setState({
                isLoading: false,
            })
        })
        .catch(error => {
            this.setState({
                isLoading: false,
            })
            console.log('error connect:', error.response)
        })
    }

    _onRefreshNew = () => {
        if ( this.state.isLoading ) {
            return;
        }

        this.setState({
            isLoading: true,
        })

        this.setState({
            settingsForum: null,
            lastestTopics: [],
            newestUsers: [],
            mostLikedAuthors: [],
            mostLikedAuthorsMore: [],
        });

        setTimeout(() => {
            this._refreshData();
        }, 200)
    }

    _onPressContent = (_post_id) => {
        return this.props.navigation.navigate('StoryDetail', {postId: _post_id});
    }

    _onPressAuthor = (_author_id) => {
        return this.props.navigation.navigate('UserProfiles', {userId: _author_id});
    }

    truncateString2(str, max) {
        if (str.length > max) {
            const suffix = "...";
            return `${str.substr(0, str.substr(0, max - suffix.length).lastIndexOf(' '))}${suffix}`;
        }
        return str;
    }

    truncateString(_string, max_length = 20) {
        _string = _string.replace(/<\/?("[^"]*"|'[^']*'|[^>])*(>|$)/g, "").replace(/&nbsp;/g, " ");

        if (_string.length > max_length) {
            // Runes
            return runes.substr(_string, 0, max_length)+'...'
        }
        return _string
    }

    _onPressCollapseButton = () => {
        this.setState((previousState) => ({
            viewMoreAuthors: !previousState.viewMoreAuthors
        }));
    }

    _onPressViewMoreDescription = () => {
        this.setState((previousState) => ({
            viewMoreDescription: !previousState.viewMoreDescription
        }))
    }

    _renderForumDescription = (forumDescription) => {
        const maxCharacters = 530;
        let clippedHtml =  clip( forumDescription, maxCharacters, { html: true, maxLines: 7, indicator: '... (Read more)'} )

        if (clippedHtml === forumDescription || forumDescription.length < maxCharacters) {
            return (
                <HTML
                    source={{ html: clippedHtml }}
                    emSize={15}
                    baseFontStyle={{ fontSize: 15 }}
                    tagsStyles={{
                        p: { marginBottom: 0 }
                    }}
                    parentWrapper={View}
                />
            )
        }

        if (this.state.viewMoreDescription) {
            return (
                <TouchableOpacity
                    onPress={() => this._onPressViewMoreDescription()}
                    keyboardShouldPersistTaps={true}
                >
                    <HTML
                        source={{ html: forumDescription }}
                        emSize={15}
                        baseFontStyle={{ fontSize: 15 }}
                        tagsStyles={{
                            p: { marginBottom: 0 }
                        }}
                        parentWrapper={View}
                    />
                </TouchableOpacity>
            )
        }

        return (
            <TouchableOpacity
                onPress={() => this._onPressViewMoreDescription()}
                keyboardShouldPersistTaps={true}
            >
                <HTML
                    source={{ html: clippedHtml }}
                    emSize={15}
                    baseFontStyle={{ fontSize: 15 }}
                    tagsStyles={{
                        p: { marginBottom: 0 }
                    }}
                    parentWrapper={View}
                />
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Loader loading={this.state.isLoading} />
                <ScrollView
                    refreshControl={
                        <RefreshControl
                        refreshing={this.state.isLoading}
                        onRefresh={this._onRefreshNew}
                        />
                    }
                >
                    <View style={styles.body}>
                        <View style={styles.container}>
                            <View style={styles.labelWrapper}>
                                <Text style={styles.headerText}>About Us</Text>
                            </View>
                            <View style={styles.separator}/>
                            <View style={styles.descriptionWrapper}>
                                {this.state.settingsForum && this.state.settingsForum.description ?
                                    this._renderForumDescription(this.state.settingsForum.description) : null}
                            </View>
                        </View>
                        <View style={styles.container}>
                            <View style={styles.labelWrapper}>
                                <Text style={styles.headerText}>Lastest Posts</Text>
                            </View>
                            <View style={styles.separator}/>
                            <View style={styles.posts}>
                                {this.state.lastestTopics.length ?
                                    this.state.lastestTopics.map((item, index) => (
                                        <View key={'topic_' + item.id} style={styles.post}>
                                            <View style={styles.postContentWrapper}>
                                                {/* {this._renderTopicContent(item)} */}
                                                {item.content.length ? (
                                                    <TouchableOpacity onPress={() => this._onPressContent(item.id)} >
                                                        <Text style={ styles.topicTitle }>{this.truncateString(item.content, 50)}</Text>
                                                    </TouchableOpacity>
                                                ) : null}
                                            </View>
                                            <View style={styles.postInfo}>
                                                <TouchableOpacity onPress={() => this._onPressAuthor(item.author.id)} >
                                                    <Text style={styles.postAuthorName}>{item.author.name}</Text>
                                                </TouchableOpacity>
                                                <Text style={styles.bullet}>•</Text>
                                                <Moment fromNow ago element={Text} style={styles.createdtime}>{item.created}</Moment>
                                            </View>
                                        </View>
                                    ))
                                : null}
                            </View>
                        </View>
                        <View style={styles.container}>
                            <View style={styles.labelWrapper}>
                                <Text style={styles.headerText}>New Members</Text>
                            </View>
                            <View style={styles.separator}/>
                            <View style={styles.members}>
                                {this.state.newestUsers.length ?
                                    this.state.newestUsers.map((item, index) => (
                                        <TouchableOpacity onPress={() => this._onPressAuthor(item.id)} key={'member_' + item.id}>
                                            <View style={ styles.member }>
                                                <View style={styles.panelLeft}>
                                                    <UserAvatar size={40} name={item.name} src={getSmallThumb(item.avatar)} />
                                                    <OnlineUserDot shopify_cid={item.shopify_cid} />
                                                </View>
                                                <View style={styles.panelRight}>
                                                    <View style={styles.memberNameWrapper}>
                                                        <Text style={styles.memberName}>{item.name}</Text>
                                                    </View>
                                                    <View style={styles.memberInfo}>
                                                        <Moment fromNow ago element={Text} style={styles.createdtime}>{item.created}</Moment>
                                                        <Text style={styles.bullet}>•</Text>
                                                        <View style={styles.location}>
                                                            <FontAwesome5 name={'map-marker-alt'} solid size={14} color={'green'} />
                                                            <Text style={{color: 'green', fontSize: 14}}>
                                                                &nbsp;{item.country}
                                                            </Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    ))
                                : null}
                            </View>
                        </View>
                        <View style={styles.container}>
                            <View style={styles.labelWrapper}>
                                <Text style={styles.headerText}>Most Liked Authors</Text>
                            </View>
                            <View style={styles.separator}/>
                            <View style={styles.members}>
                                {this.state.mostLikedAuthors.length ?
                                    this.state.mostLikedAuthors.map((item, index) => (
                                        <TouchableOpacity onPress={() => this._onPressAuthor(item.id)} key={'author_' + item.id}>
                                            <View style={ styles.member }>
                                                <View style={styles.panelLeft}>
                                                    <UserAvatar size={40} name={item.name} src={getSmallThumb(item.avatar)} />
                                                    <OnlineUserDot shopify_cid={item.shopify_cid} />
                                                </View>
                                                <View style={styles.panelRight}>
                                                    <View style={styles.memberNameWrapper}>
                                                        <Text style={styles.memberName}>{item.name}</Text>
                                                    </View>
                                                    <View style={styles.memberInfo}>
                                                        <View style={styles.likeIcon}>
                                                            <View style={styles.likeBubble}>
                                                                <FontAwesome5 name="thumbs-up" solid size={10} color="#fff" />
                                                            </View>
                                                            <Text style={{ fontSize: 16, color: '#6c757d'}}>{item.like_count}</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                                <View>
                                                    <Text style={styles.order}>{index + 1}</Text>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    ))
                                : null}
                                {this.state.viewMoreAuthors && this.state.mostLikedAuthorsMore.length ?
                                    this.state.mostLikedAuthorsMore.map((item, index) => (
                                        <TouchableOpacity onPress={() => this._onPressAuthor(item.id)} key={'author_' + item.id}>
                                            <View style={ styles.member }>
                                                <View style={styles.panelLeft}>
                                                    <UserAvatar size={40} name={item.name} src={getSmallThumb(item.avatar)} />
                                                    <OnlineUserDot shopify_cid={item.shopify_cid} />
                                                </View>
                                                <View style={styles.panelRight}>
                                                    <View style={styles.memberNameWrapper}>
                                                        <Text style={styles.memberName}>{item.name}</Text>
                                                    </View>
                                                    <View style={styles.memberInfo}>
                                                        <View style={styles.likeIcon}>
                                                            <View style={styles.likeBubble}>
                                                                <FontAwesome5 name="thumbs-up" solid size={10} color="#fff" />
                                                            </View>
                                                            <Text style={{ fontSize: 16, color: '#6c757d'}}>{item.like_count}</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                                <View>
                                                    <Text style={styles.order}>{this.state.mostLikedAuthors.length + index + 1}</Text>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    ))
                                : null}
                                {this.state.mostLikedAuthorsMore.length ? (
                                    <TouchableOpacity style={styles.collapseWrapper} onPress={() => this._onPressCollapseButton()}>
                                        <View style={styles.collapseButton}>
                                            <Text style={[styles.label1, {color: '#6c757d'}]}>{!this.state.viewMoreAuthors ? "View more" : "View less"}</Text>
                                            <FontAwesome5 name={!this.state.viewMoreAuthors ? "caret-down" : "caret-up"} solid size={20} color="#6c757d" />
                                        </View>
                                    </TouchableOpacity>
                                ) : null}
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    body: {
        padding: 10,
    },
    container: {
        borderWidth: .5,
        borderColor: '#d0d0d0',
        borderRadius: 4,
        // marginHorizontal: 10,
        marginBottom: 10,
        backgroundColor: '#fff'
    },
    labelWrapper: {
        padding: 10,
    },
    headerText: {
        fontSize: 17,
        fontWeight: '600',
    },
    descriptionWrapper: {
        padding: 10,
    },
    posts: {
        padding: 10,
    },
    post: {
        flexDirection: 'column',
        paddingBottom: 5,
    },
    postContentWrapper: {
        paddingBottom: 5,
    },
    topicTitle: {
        fontSize: 16,
        fontWeight: '500',
    },
    postInfo: {
        flexDirection: 'row',
    },
    postAuthorName: {
        color: '#007bff',
        fontSize: 15,
        fontWeight: '600',
    },
    bullet: {
        fontSize: 14,
        color: '#939393',
        paddingHorizontal: 5
    },
    createdtime: {
        fontSize: 15,
        color: '#999',
    },
    separator: {
        height: 1.5,
        width: '100%',
        backgroundColor: '#e9e9e9',
    },
    members: {
        padding: 10,
    },
    member: {
        flexDirection: 'row',
        paddingBottom: 5,
    },
    panelLeft: {
        height: 50,
        width: 50,
        paddingRight: 10,
    },
    panelRight: {
        flex: 1,
        flexDirection: 'column',
    },
    memberNameWrapper: {

    },
    memberName: {
        fontSize: 16,
        fontWeight: '500'
    },
    memberInfo: {
        flexDirection: 'row',
    },
    location: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    collapseButton: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    label1: {
        fontSize: 16,
        paddingRight: 5,
    },
    likeIcon: {
        flexDirection: 'row',
        paddingRight: 5,
    },
    likeBubble: {
        backgroundColor: "#007bff",
        paddingVertical: 4,
        paddingHorizontal: 5,
        marginRight: 5,
        borderRadius: 50,
        height: 20,
    },
    order: {
        fontSize: 22,
        fontWeight: '600',
        color: '#dfe4e6',
        paddingTop: 5,
        paddingRight: 5,
    }
})
