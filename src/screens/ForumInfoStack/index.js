import * as React from 'react';
import ForumInfo from './ForumInfo';
import { createStackNavigator } from '@react-navigation/stack';

const ContactStack = createStackNavigator();

function ForumInfoStack() {
    return (
      <ContactStack.Navigator>
        <ContactStack.Screen name="Forum Information" component={ForumInfo} />
      </ContactStack.Navigator>
    );
}

export default ForumInfoStack;
