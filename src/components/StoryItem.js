import React from "react";
import { View, Text, Share, Alert, StyleSheet, TouchableOpacity } from "react-native";
import {STORAGE_KEY, BASE_URL_API} from '../../app.json';
import UserAvatar from "react-native-user-avatar";
// import EntypoIcon from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import StoryItemData from './StoryItemData';
import Moment from 'react-moment';
import OptionsMenu from 'react-native-option-menu';
import OnlineUserDot from './OnlineUserDot';
import UserBadges from "../components/UserBadges";
import CategoryNotch from "./CategoryNotch";
import Comments from "./HomePage/Comments";
import { getSmallThumb } from "../utils/global";

export default class StoryItem extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            imageCurrentIndex: 0,
            isImageViewVisible: false,
            // authenUser: null,
        }
    }

    _onPressRow = () => {
        this.props.onPressItem(this.props);
    }

    _onPressShare = () => {
        Share.share({
            url: this.props.url
        })
        .then(result => console.log(result))
        .catch(errorMsg => console.log(errorMsg));
    }

    _onPressOptions = () => {
        Alert.alert('Click', `More Options`);
    }

    _onPressUser = () => {
        this.props.onPressUser(this.props);
    }

    reportPost = () => {
        return this.props.openSendReport({object_id: this.props.id, type: 'topic'});
    }

    _hideTopic = () => {
        Alert.alert(
            "Do you want to hide this topic?",
            null,
            [
            {
                text: "Cancel",
                style: "cancel"
            },
            { text: "OK", onPress: this.props.hideTopic }
            ],
            { cancelable: false }
        );
    }

    _deleteTopic = () => {
        Alert.alert(
            "Do you want to delete this topic?",
            null,
            [
                { text: "Cancel", style: "cancel" },
                { text: "OK", onPress: this.props.deleteTopic }
            ],
            { cancelable: false }
        );
    }

    render() {
        let renderLikeButton;
        let renderLikesList;
        const myOptionIcon = (
            <View style={styles.myOptionIcon}>
                <FontAwesome5 name="ellipsis-h" size={15} color="gray" solid />
            </View>
        );
        let optionsForMenu = {
            "Copy URL": this.props.copyURL,
            "Report": this.reportPost,
        }

        if (this.props.authenUser && this.props.author && this.props.authenUser.id == this.props.author.id) {
            optionsForMenu['Edit'] = this.props.editTopic;
            optionsForMenu['Hide'] = this._hideTopic;
            optionsForMenu['Delete'] = this._deleteTopic;
        }

        if (this.props.likes && this.props.likes.length > 0) {
            var likesList = this.props.likes.map(item => item.user_name);
            renderLikesList = likesList.join(', ');
            if (this.props.like_count > this.props.likes.length) {
                renderLikesList += ' and ' + (this.props.like_count - this.props.likes.length) + ' others';
            }
        }

        if (this.props.authenUser && this.props.likes.findIndex(_item => _item.user_id == this.props.authenUser.id) > -1) {
            renderLikeButton = (
                <TouchableOpacity onPress={() => this.props.onPressLike(this.props.id)} >
                    <View style={styles.boxButton}>
                        <FontAwesome5 name="thumbs-up" size={15} color="#007bff" />
                        <Text style={[ styles.numbers, {color: '#007bff'} ]}>Like</Text>
                    </View>
                </TouchableOpacity>
            )
        } else {
            renderLikeButton = (
                <TouchableOpacity onPress={() => this.props.onPressLike(this.props.id)} >
                    <View style={styles.boxButton}>
                        <FontAwesome5 name="thumbs-up" size={15} color="gray" />
                        <Text style={styles.numbers}>Like</Text>
                    </View>
                </TouchableOpacity>
            )
        }

        return (
            <View>
                {this.props.showCategory && this.props.category ? (
                    <CategoryNotch {...this.props.category}/>
                ) : null}
                <View style={styles.row}>
                    <View style={styles.rowHeader}>
                        {this.props.author ? (
                        <View style={styles.panelLeft}>
                            <TouchableOpacity onPress={this._onPressUser} >
                                <UserAvatar size={40} name={this.props.author.name} src={getSmallThumb(this.props.author.avatar)} />
                                <OnlineUserDot shopify_cid={this.props.author.shopify_cid} />
                            </TouchableOpacity>
                        </View>
                        ): null}
                        <View style={styles.panelRight}>
                            {this.props.author ? (
                            <View style={styles.itemAuthor}>
                                <UserBadges badgeList={this.props.badges} badgeIds={this.props.author.badge_id} />
                                <View style={{ flex: 1, flexDirection: 'row'}}>
                                    <TouchableOpacity onPress={this._onPressUser} style={{ marginRight: 5 }}>
                                        <Text style={[styles.itemName]}>{this.props.author.name}</Text>
                                    </TouchableOpacity>
                                    {this.props.author.is_staff && (
                                        <View style={styles.wrapStaffBadge}>
                                            <Text style={styles.staffText}>{this.props.settingsForum.staff_text}</Text>
                                        </View>
                                    )}
                                    {this.props.pinned && (
                                        <View style={{ marginLeft: 5, justifyContent: 'center' }}>
                                            <FontAwesome5 name="thumbtack" solid size={13} color="gray" />
                                        </View>
                                    )}
                                </View>
                                <View style={{ flexDirection: 'row' }} >
                                    <TouchableOpacity onPress={this._onPressRow} >
                                        <Moment fromNow element={Text} style={[styles.itemDescText]}>{this.props.created}</Moment>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            ): null}

                            <View style={styles.rightWidget}>
                                <OptionsMenu
                                    customButton={myOptionIcon}
                                    destructiveIndex={Object.keys(optionsForMenu).indexOf('Delete')}
                                    options={[...Object.keys(optionsForMenu), "Cancel"]}
                                    actions={[...Object.values(optionsForMenu)]}/>
                            </View>
                        </View>
                    </View>

                    <StoryItemData
                        content={this.props.content}
                        images={this.props.images}
                        videos={this.props.videos}
                        products={this.props.products}
                        embeds={this.props.embeds}
                        poll={this.props.poll}
                        viewUrlModal={this.props.viewUrlModal}
                        changeVote={this.props.changeVote}
                    />

                    <View style={styles.rowFooter}>
                        <View style={ styles.boxRowHaft }>
                            {this.props.like_count ? (
                                <TouchableOpacity onPress={() => this.props.viewLikesList(this.props.id)} >
                                    <View style={[ styles.flexRowInside, styles.padVerticalMd ]}>
                                        <View style={styles.likeBubble}>
                                            <FontAwesome5 name="thumbs-up" solid size={10} color="#fff" />
                                        </View>
                                        <Text style={styles.numbers}>{renderLikesList}</Text>
                                    </View>
                                </TouchableOpacity>
                            ): null}
                        </View>

                        <View style={styles.boxRowHaft}>
                            {this.props.no_posts ? (
                                <TouchableOpacity onPress={this._onPressRow} >
                                    <View style={[ styles.boxColumnRight, styles.padVerticalMd ]}>
                                        <Text style={styles.numbers}>{this.props.no_posts} Comments</Text>
                                    </View>
                                </TouchableOpacity>
                            ): null}
                        </View>
                    </View>

                    <View style={[styles.rowFooter, styles.rowFooterBtn]}>
                        <View style={styles.boxRowHaft}>
                                {renderLikeButton}
                        </View>

                        <View style={styles.boxRowHaft}>
                            {this.props.locked ? (
                                <View style={styles.boxButton}>
                                    <FontAwesome5 name="lock" solid size={15} color="#6c757d" />
                                    <Text style={[styles.numbers, {color: '#6c757d'}]}>Comment</Text>
                                </View>
                            ) : (
                                <TouchableOpacity onPress={this._onPressRow} >
                                    <View style={styles.boxButton}>
                                        <FontAwesome5 name="comment-alt" size={15} color="gray" />
                                        <Text style={styles.numbers}>Comment</Text>
                                    </View>
                                </TouchableOpacity>
                            )}
                        </View>
                        <View style={styles.boxRowHaft}>
                            <TouchableOpacity onPress={this._onPressShare} >
                                <View style={styles.boxButton}>
                                    <FontAwesome5 name="share-alt" size={15} color="gray" />
                                    <Text style={styles.numbers}>Share</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                {this.props.comments.length ? (
                    <View style={{height: 1, backgroundColor: '#dedede'}} />
                ) : null}

                {this.props.comments.length ? (
                    <View style={ styles.commentsBox }>
                        {this.props.comments.length ? (
                            <View style={{ marginVertical: 7 }}>
                                <Comments
                                    topicId={this.props.id}
                                    navigation={this.props.navigation}
                                    accessedForum={this.props.accessedForum}
                                    accessToken={this.props.accessToken}
                                    loggedUser={this.props.loggedUser}
                                    settingsForum={this.props.settingsForum}
                                    badgeList={this.props.badges}
                                    comments={this.props.comments}
                                    editComment={this.props.editComment}
                                    deleteComment={this.props.deleteComment}
                                    openSendReport={this.props.openSendReport}
                                />
                            </View>
                        ) : null}
                    </View>
                ) : null}

                <View style={[styles.separator, styles.highShort]} ></View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    row: {
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: '#ffffff',
    },
    rowHeader: {
        flex: 1,
        flexDirection: 'row',
    },
    panelLeft: {
        height: 40,
        width: 40,
        paddingTop: 0,
    },
    panelRight: {
        flex: 1,
        flexDirection: 'row',
    },
    wrapStaffBadge: {
        borderRadius: 4,
        backgroundColor: '#dce0e2',
        alignItems: 'center'
    },
    staffText: {
        fontSize: 12,
        color: '#5d707f',
        paddingHorizontal: 7,
        paddingVertical: 2
    },
    padVerticalMd: {
        paddingVertical: 10,
    },
    itemAuthor: {
        paddingLeft: 15,
        paddingVertical: 3,
        flex: 1,
        flexDirection: 'column',
    },
    rightWidget: {
        position: 'absolute',
        top: -3,
        right: -10,
        zIndex: 99,
        position: 'relative',
    },
    myOptionIcon: {
        paddingHorizontal: 10,
        paddingVertical: 8
    },
    itemName: {
        fontSize: 16,
        fontWeight: '500',
        color: '#007bff',
    },
    itemDescriptions: {
        flexDirection: 'row',
    },
    itemDescText: {
        fontSize: 15,
        color: '#666',
    },
    rowBody: {
        paddingTop: 15,
        marginBottom: 10,
    },
    itemBodyText: {
        paddingBottom: 15,
        fontSize: 16,
    },
    rowImages: {
        marginLeft: -10,
    },
    rowEmbeds: {
        marginBottom: 10,
        marginHorizontal: -10,
    },
    rowProducts: {
        marginBottom: 10,
    },
    separator: {
        height: 1,
        width: '100%',
    },
    highShort: {
        height: 10,
    },
    rowFooter: {
        width: '100%',
        flexDirection: 'row',
    },
    rowFooterBtn: {
        borderTopWidth: 1,
        borderTopColor: '#dedede',
        marginBottom: -5,
    },
    boxButton: {
        flexDirection: 'row',
        paddingVertical: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    boxColumnRight: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-end'
    },
    boxRowHaft: {
        flex: 1,
    },
    flexRowInside: {
        flex: 1,
        flexDirection: 'row',
    },
    numbers: {
        paddingTop: 1,
        paddingLeft: 6,
        fontSize: 15,
    },
    likeBubble: {
        backgroundColor: "#007bff",
        paddingVertical: 4,
        paddingHorizontal: 5,
        borderRadius: 50,
        height: 20,
    },
    commentsBox: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        paddingVertical: 0,
    },
});

