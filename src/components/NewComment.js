import React from "react";
import {
    View, Text, Image, Alert,
    StyleSheet,
    ActivityIndicator,
    TouchableOpacity,
    ScrollView
} from "react-native";
import { BASE_URL_API } from '../../app.json';
// import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ImagePicker from 'react-native-image-crop-picker';
// import { restElement } from "@babel/types";
import {actions, RichEditor, RichToolbar} from 'react-native-new-rich-editor';
import axios from 'axios';

const filterHtmlData = (html) => html.trim()
        .replace(/<p>(.*?)<br><\/p>/g, '<p>$1<\/p>')
        .replace(/^(<p>\s*(\&nbsp;)*<\/p>)+/g, "")
        .replace(/(<p>\s*(\&nbsp;)*<\/p>)+$/g, "");

export default class NewComment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            sending: false,
            content: null,
            localImage: null,
        }

        this._createComment = this._createComment.bind(this);
        this._closeCreateComment = this._closeCreateComment.bind(this);
        this._onPressCloseCreateComment = this._onPressCloseCreateComment.bind(this);
    }

    insertMention = (_user) => {
        let _html = `<span class="mention" data-id="${_user.id}" contenteditable="false" >${_user.name}</span> `;
        // this.richtext.focusContentEditor()
        this.richtext.insertHTML(' ')
        setTimeout(() => {
            this.richtext.insertRawHTML(_html)
        }, 100)
    }

    componentDidUpdate = () => {
        if (this.props.initialFocus || this.state.content && this.state.content.length) {
            this.richtext.focusContentEditor();
        } else {
            this.richtext.blurContentEditor();
        }
    }

    _createComment = () => {
        const form = new FormData();
        let { content, localImage } = this.state
        const { comment } = this.props

        if (content !== null && content !== '') {
            content = filterHtmlData(content)
        }

        form.append('content', content);
        form.append('user_id', this.props.loggedUser.id);
        form.append('topic_id', this.props.postId);

        if (comment && comment.parent_id) {
            form.append('parent_id', comment.parent_id);
        }

        if (localImage) {
            let filename = localImage.path.replace(/^.*[\\\/]/, ''); // works for both / and \

            form.append('images[]', {
                uri: localImage.path,
                type: localImage.mime,
                name: filename,
                size: localImage.size,
            });
        }

        this.setState({
            sending: true,
        })

        const configHeaders = {
            headers: {
                'Content-Type': 'multipart/form-data',
                Shop: this.props.accessedForum.shop,
                token: this.props.accessToken
            }
        };

        axios.post(BASE_URL_API + '/posts/create.json', form, configHeaders)
        .then(res => {
            let {data} = res;

            if (data.post) {
                this.props.updateCommentsData(data.post);
            }
            this._closeCreateComment();
            this.setState({
                sending: false,
            })
        })
        .catch(error => {
            this.setState({
                sending: false,
            })
            console.log('add comment error', error.response)
        })
    }

    _onPressPhoto (event) {
        ImagePicker.openPicker({
            multiple: false,
            forceJpg: true,
        }).then(image => {
            this.setState({
                localImage: image,
            });
        })
        .catch(error => {
            // E_PERMISSION_MISSING
            // console.log('imagePicker error', error.code, error.message)
            if (error.code == 'E_PERMISSION_MISSING') {
                Alert.alert('Cannot access your gallery', 'You have to go to Settings to enable it.');
            }
        })
    }

    removeImage = () => {
        this.setState({
            localImage: null,
        });
    }

    _closeCreateComment = () => {
        this.richtext.setContentHTML('');
        this.props.onCloseCreateComment();
        this.setState({
            content: null,
            localImage: null,
        });
    }

    _onPressCloseCreateComment = () => {
        Alert.alert(
            "Do you want to close this post?",
            null,
            [
                { text: "Cancel", style: "cancel" },
                {
                    text: "OK",
                    onPress: () => this._closeCreateComment()
                }
            ],
            { cancelable: true }
        );
    }

    render() {
        let renderSendBtn;

        if (!!this.state.content && this.state.content.trim() && !this.state.sending) {
            renderSendBtn = (
                <TouchableOpacity onPress={() => this._createComment()}>
                    <View style={styles.sendInside}>
                        <Ionicons name="md-send" size={25} color="#0066ff" />
                    </View>
                </TouchableOpacity>
            )
        } else {
            renderSendBtn = (
                <View style={styles.sendInside}>
                    <Ionicons name="md-send" size={25} color="gray" />
                </View>
            )
        }

        return (
            <View style={styles.container}>
                {this.props.initialFocus && this.props.comment && this.props.comment.name && (
                    <View style={styles.rowMention}>
                        <View style={styles.mentionName}>
                            <Text style={{ fontSize: 16, color: '#222'}}>Reply to: <Text style={{fontWeight: '700'}}>{this.props.comment.name}</Text></Text>
                        </View>
                        <TouchableOpacity onPress={() => this._onPressCloseCreateComment()}>
                            <View style={styles.closeMention}>
                                <FontAwesome5 name="times" solid size={18} color="gray" />
                            </View>
                        </TouchableOpacity>
                    </View>
                )}

                    {this.state.localImage && (
                        <View style={styles.mediaBox}>
                            <View style={styles.imageBox} >
                                <Image source={{ uri: this.state.localImage.path}} style={{width: 60, height: 60}} />
                            </View>

                            <View style={styles.imgClose}>
                                <TouchableOpacity onPress={this.removeImage}>
                                    <View>
                                        <Ionicons name="md-close" size={18} color="white" />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )}

                    <View style={styles.rowInputInner}>
                        <TouchableOpacity onPress={this._onPressPhoto.bind(this)}>
                            <View style={ styles.btnImage }>
                                <FontAwesome5 name="image" solid size={25} color="gray" />
                            </View>
                        </TouchableOpacity>
                        <ScrollView style={ styles.editorWrapper }>
                            <RichEditor
                                ref={(r) => this.richtext = r}
                                initialHeight={25}
                                containerStyle={{ borderWidth: 1, borderColor: 'transparent' }}
                                placeholder={"Write a comment..."}
                                editorStyle={{
                                    cssText,
                                    backgroundColor: '#f0f0f0'
                                }}
                                defaultParagraphSeparator="p"
                                pasteAsPlainText={true}
                                initialContentHTML={this.state.content}
                                onChange={content => this.setState({ content }) }
                            />
                        </ScrollView>
                        <View style={styles.btnSend}>
                            {renderSendBtn}
                        </View>
                    </View>

            </View>
        );
    }
}

const cssText = `
p { margin: 0; margin-bottom: 5px; }
span.mention {
    background-color: rgba(96, 160, 206, 0.25);
    border-radius: 6px;
    padding: 1px 3px;
  }
`;

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'white',
    },
    mentionName: {
        paddingLeft: 8,
        paddingVertical: 10,
    },
    closeMention: {
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    rowMention: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderTopWidth: 1,
        borderTopColor: '#e9e9e9',

    },
    rowInputInner: {
        flex: 1,
        flexDirection: 'row',
        padding: 5,
        borderTopWidth: 1,
        borderTopColor: '#e9e9e9',
    },
    editorWrapper: {
        flex: 1,
        backgroundColor: '#f0f0f0',
        borderWidth: 0.5,
        borderColor: '#e0e0e0',
        maxHeight: 150
    },
    mediaBox: {
        padding: 10,
        backgroundColor:"#f9f9f9",
        flexDirection: 'row',
        justifyContent: 'center',
    },
    imageBox: {
        marginHorizontal: 5,
    },
    btnImage: {
        width: 35,
        padding: 3,
        paddingTop: 8,
    },
    btnSend: {
        width: 40,
    },
    sendInside: {
        paddingVertical: 7,
        paddingHorizontal: 5,
    },
    imgClose: {
        position: 'absolute',
        right: 3,
        top: 3,
        width: 20,
        height: 20,
        alignContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'white',
        backgroundColor: 'rgba(0, 0, 0, .5)',
    }
});
