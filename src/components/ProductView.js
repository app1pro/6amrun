import React from "react";
import { Text, View, Image, TouchableOpacity, StyleSheet } from "react-native";

export default function ProductView( props ) {
    const truncateString = (str, max) => {
        if (str.length > max) {
            const suffix = "...";
            return `${str.substr(0, str.substr(0, max - suffix.length).lastIndexOf(' '))}${suffix}`;
        }
        return str;
    }

    return (
        <TouchableOpacity onPress={() => props.viewUrlModal(props.product_url)}>
            <View style={styles.item} key={props.product_id}>
                <Image source={{ uri: props.product_image}} style={styles.image}/>
                <View style={styles.productDetails}>
                    <Text style={styles.title}>{props.product_title}</Text>
                    <Text style={styles.desc}>{truncateString(props.product_desc, 120)}</Text>
                    {!!props.product_price && (
                        <Text style={styles.price}>${props.product_price}</Text>
                    )}
                </View>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    item: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: 'rgba(0, 0, 0, .2)',
    },
    image: {
        width: 65,
        height: 100
    },
    productDetails: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 15,
    },
    title: {
        color: "#007bff",
        fontWeight: "bold",
        marginBottom: 4,
        fontSize: 15,
    },
    desc: {
        color: "#6c757d",
        marginBottom: 5,
        fontSize: 15
    },
    price: {
        color: "#ff0000",
    }
});