import React from 'react';
import { StyleSheet, View, Text, } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default function UserBadges ( props ) {
    const getAuthorBadges = (badge_ids) => {
        if (badge_ids == '') return [];

        let ids = badge_ids.split(',');
        let { badgeList } = props;

        return ids.map(id => {
            return badgeList[badgeList.findIndex(badge => badge.id == id)];
        }).filter(item => item !== undefined);
    }

    const badgeIcon = (badge) => {
        let iconAttribute = badge.icon.split(' ');
        let icon = iconAttribute[1].substring(3);

        switch (iconAttribute[0]) {
            case 'fas':
                return (
                    <FontAwesome5 name={icon} solid size={12} color={'#6c757d'} />
                )
            case 'fab':
                return (
                    <FontAwesome5 name={icon} brand size={12} color={'#6c757d'} />
                )
            default:
                return (
                    <FontAwesome5 name={icon} size={12} color={'#6c757d'} />
                )
        }
    }

    if (props.badgeIds == null || props.badgeIds === '' || !props.badgeList) {
        return null;
    }

    return (
        <View style={styles.wrapBadges}>
            {getAuthorBadges(props.badgeIds).map((badge, index) => (
                <View key={'badge_id_' + index} style={styles.badgeLabel}>
                    {badgeIcon(badge)}
                    <Text style={styles.staffBadge}>&nbsp;{badge.name}</Text>
                </View>
            ))}
        </View>
    );
}

const styles = StyleSheet.create({
    wrapBadges: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        marginBottom: 2,
        marginRight: 25,
    },
    badgeLabel: {
        flexDirection: 'row',
        marginRight: 7,
        alignItems: 'center'
    },
    staffBadge: {
        fontSize: 12,
        fontWeight: '500',
        color: '#6c757d'
    },
})