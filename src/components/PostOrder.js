import React from 'react';
import {
  StyleSheet,
  View,
  ActivityIndicator
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default function PostOrder( props ) {

    return (
        <View style={{ padding: 10, paddingTop: 0 }}>
            <View style={ styles.boxFilter }>
                <View style={ styles.boxFilterInner }>
                    <RNPickerSelect
                        style={ pickerSelectStyles }
                        onValueChange={value => props.onChange(value) }
                        items={[
                            { label: 'Recent Activity', value: 'recent-activity' },
                            { label: 'Recent Post', value: 'recent-post' },
                            { label: 'Most Likes', value: 'most-likes' },
                            { label: 'Most Comments', value: 'most-comments' },
                        ]}
                    />
                </View>
                <View style={{ padding: 5, paddingVertical: 10 }}>
                    <FontAwesome5 name="sort-amount-up" solid size={20} color="#c0c0c0" />
                </View>
            </View>
        </View>
    )
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 15,
        paddingVertical: 5,
        paddingHorizontal: 10,
        color: 'black',
        paddingRight: 25, // to ensure the text is never behind the icon
        width: '100%',
    },
    inputAndroid: {
        fontSize: 8,
        paddingVertical: 5,
        paddingHorizontal: 10,
        color: 'black',
        paddingRight: 25, // to ensure the text is never behind the icon
        width: '100%',
    },
  });

const styles = StyleSheet.create({
    boxFilter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 10,
        paddingVertical: 5,
        backgroundColor: '#fff',
        borderRadius: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    boxFilterInner: {
        flex: 1,
    }
});
