import React,{ Component } from "react";
import { Text, View, TouchableOpacity, StyleSheet } from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default function SubCategoryItem( props ) {
    return (
        <View>
            <View style={[styles.container, props.borderTopRadius ? styles.borderTopRadius : null, props.borderBottomRadius ? styles.borderBottomRadius : null]}>
                <View style={styles.containerInner}>
                    <View style={styles.row}>
                        {!!props.icon && (
                            <View style={styles.panelLeft}>
                                <View style={styles.itemIcon}>
                                    <CatIcon color={ props.color } icon={ props.icon } />
                                </View>
                            </View>
                        )}
                        <View style={styles.panelRight}>
                            <TouchableOpacity onPress={() => props.onPressItem(props)} style={styles.contentWrap}>
                                <View style={styles.itemDetails}>
                                    <Text style={[styles.itemName]}>{props.title}</Text>
                                </View>
                                {(props.topics_count > 0) && (
                                    <View style={styles.badgeCount}>
                                        <Text style={styles.badgeCountText}>{props.topics_count}</Text>
                                    </View>
                                )}
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
}

function CatIcon( props ) {
    if (!props.icon) return null;
    let arrIcon = props.icon.split(' ');
    if (arrIcon.length === 0) return null;
    let icon = arrIcon[1].substring(3);

    switch (arrIcon[0]) {
        case 'fas':
            return (
                <FontAwesome5 name={icon} solid size={props.size || 20} color={props.color} />
            )
        case 'fab':
            return (
                <FontAwesome5 name={icon} brand size={props.size || 20} color={props.color} />
            )
        default:
            return (
                <FontAwesome5 name={icon} size={props.size || 20} color={props.color} />
            )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        paddingLeft: 15,
        borderTopWidth: 1,
        borderBottomWidth: 0,
        borderColor: '#e9e9e9',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    borderTopRadius: {
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
    },
    borderBottomRadius: {
        borderBottomWidth: 1,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
    },
    containerInner: {
        flex: 1,
        flexDirection: 'row',
    },
    row: {
        flex: 1,
        flexDirection: 'row',
    },
    panelLeft: {
        height: 'auto',
        width: 35,
        paddingVertical: 15,
    },
    panelRight: {
        flex: 1,
        flexDirection: 'row',
    },
    contentWrap: {
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 12,
        paddingRight: 12,
        justifyContent: 'space-between'
    },
    itemIcon: {
        paddingVertical: 0,
        alignItems: 'flex-start',
    },
    itemDetails: {
        paddingVertical: 4,
    },
    itemName: {
        fontSize: 16,
    },
    badgeCount: {
        height: 24,
        width: 24,
        paddingHorizontal: 4,
        paddingVertical: 4,
        borderWidth: 1,
        borderColor: 'white',
        backgroundColor: '#007bff',
        borderRadius: 8,
    },
    badgeCountText: {
        fontSize: 12,
        color: 'white',
        textAlign: 'center',

    },
});
