import React, { Component } from "react";
import { Text, View, ScrollView, TextInput, SafeAreaView, TouchableOpacity, StyleSheet, Alert, Image, Modal, StatusBar, ActivityIndicator } from "react-native";
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {name as APP_NAME, BASE_URL_API} from '../../app.json';
import axios from 'axios';
import ImagePicker from 'react-native-image-crop-picker';

export default class ModalAddMessage extends Component {
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            content: null,
            progress: 0,
            title: '',
            localImages: [],
        }
    }

    async _sendPost() {
        this.setState({ isLoading: true, })

        const form = new FormData();
        let { content, title, localImages } = this.state;
        if (content !== null && content !== '') {
            content = content.trim();
        }

        form.append('user_id', this.props.currentUser.id);
        form.append('from_id', this.props.loggedUser.id);
        form.append('title', title);
        form.append('content', content);

        localImages.forEach(item => {
            let filename = item.path.replace(/^.*[\\\/]/, '');
            form.append('images[]', {
                uri: item.path,
                type: item.mime,
                name: filename,
                size: item.size,
            });
        });
        const configHeaders = {
            headers: {
                Shop: this.props.accessedForum.shop,
                token: this.props.accessToken,
                'Content-Type': 'multipart/form-data',
            },
            onUploadProgress: progressEvent => {
                var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                this.setState({ progress: percentCompleted });
            },
        };
        axios.post(BASE_URL_API + '/messages/send-message.json', form, configHeaders)
        .then(res => {
            let {data} = res;

            if (data.success) {
                this._closeModalMessage();
                Alert.alert('Your message has been sent successfully!');
                this.setState({
                    isLoading: false,
                })
            }
        })
        .catch(error => {
            this.setState({
                isLoading: false,
            })
            console.log('Add message error', error.response)
        })
    }

    _closeModalMessage = () => {
        this.setState({
            title: '',
            content: '',
            localImages: [],
        })
        this.props.setModalVisible(false);
    }

    uniqueImage = (_images) => {
        return _images.filter((value, pos, arr) =>  arr.findIndex(item => item.path.replace(/^.*[\\\/]/, '') == value.path.replace(/^.*[\\\/]/, '')) === pos )
    }

    _onPressPhoto = (event) => {
        ImagePicker.openPicker({
            multiple: true,
            maxFiles: 10,
        }).then(images => {
            this.setState(previousState => ({
                localImages: this.uniqueImage(previousState.localImages.concat(images)),
            }));
        })
        .catch(error => {
            if (error.code == 'E_PERMISSION_MISSING') {
                Alert.alert('Cannot access your gallery', 'You have to go to Settings to enable it.');
            }
        })
    }

    _removeImage = (_filePath) => {
        this.setState(previousState => ({
            localImages: previousState.localImages.filter(_item => _item.path.replace(/^.*[\\\/]/, '') !== _filePath.replace(/^.*[\\\/]/, ''))
        }))
    }

    render() {
        let renderSendBtn;

        if ( (!!this.state.content && this.state.content.trim() ) && (!!this.state.title && this.state.title.trim()) && !this.state.isLoading) {
            renderSendBtn = (
                <TouchableOpacity onPress={() => this._sendPost() }>
                    <View>
                        <Ionicons name="md-send" size={25} color="#0066ff" />
                    </View>
                </TouchableOpacity>
            );
        } else {
            renderSendBtn = (
                <View>
                    <Ionicons name="md-send" size={25} color="gray" />
                </View>
            );
        }

        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.modalVisible}
            >
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={styles.header}>
                        <View style={styles.panelLeft}>
                            <TouchableOpacity onPress={() => this._closeModalMessage()}>
                                <View>
                                    <FontAwesome5 name="times" size={25} color="gray" />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View>
                            {this.props.currentUser && (
                                <Text style={styles.titleText}>To: { this.props.currentUser.name}</Text>
                            )}
                        </View>
                        <View style={styles.panelRight}>
                            {this.state.isLoading && (
                                <View style={styles.wrapLoading}>
                                    {/* <View style={styles.loadingPercent}>
                                        <Text>{this.state.progress}%</Text>
                                    </View> */}
                                    <ActivityIndicator size={16} color="gray" animating={this.state.isLoading} />
                                    <View style={{width: 10}}/>
                                </View>
                            )}
                            {renderSendBtn}
                        </View>
                    </View>
                    <View style={styles.container}>
                        <View style={styles.sectionContent}>
                            <View>
                                <Text style={styles.label1}>Title</Text>
                                <TextInput
                                    style={styles.input}
                                    value={this.state.title}
                                    onChangeText={(value) => {
                                        this.setState({
                                            title: value,
                                        })
                                    }}
                                    placeholder="Title..."
                                    underlineColorAndroid="transparent"
                                />
                            </View>

                            <View>
                                <Text style={styles.label1}>Content</Text>
                                <TextInput
                                    style={[ styles.input, styles.textarea ]}
                                    value={this.state.content}
                                    onChangeText={(value) => {
                                        this.setState({
                                            content: value,
                                        })
                                    }}
                                    textAlignVertical={'top'}
                                    multiline={true}
                                    numberOfLines={4}
                                    underlineColorAndroid="transparent"
                                />
                            </View>

                        </View>

                        {this.state.localImages.length > 0 ? (
                            <View style={styles.sectionMedia}>
                                <Text style={styles.label1}>Images</Text>
                                <View style={styles.mediaBox}>
                                    <ScrollView horizontal={true}>
                                        <View style={styles.scrollImagesBox}>
                                        {this.state.localImages.map((_item, _index) => (
                                            <View style={styles.imageBox} key={'image_' + _index}>
                                                <Image source={{ uri: _item.path}} style={{width: 60, height: 60}} />
                                                <View style={styles.imgClose}>
                                                    <TouchableOpacity onPress={() => this._removeImage(_item.path) }>
                                                        <View>
                                                            <Ionicons name="md-close" size={18} color="white" />
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        ))}
                                        </View>
                                    </ScrollView>
                                </View>
                            </View>
                        ) : null}

                        <View style={styles.sectionBottom}>
                            <View style={styles.rowActions}>
                                <TouchableOpacity onPress={() => this._onPressPhoto()} style={styles.btnAction}>
                                    <View style={styles.btnImage}>
                                        <FontAwesome5 name="image" solid size={16} color="#222" />
                                        <Text style={styles.label1}>&nbsp;Add images</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>
                </SafeAreaView>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: '#ffffff',
        borderBottomWidth: 1,
        borderBottomColor: '#e9e9e9',
        justifyContent: 'space-between'
    },
    headerActions: {
        flexDirection: 'row',
    },
    panelRight: {
        flexDirection: 'row',
    },
    wrapLoading: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 2,
    },
    loadingPercent: {
        paddingLeft: 7,
        paddingRight: 5
    },
    sectionHeader: {
        paddingVertical: 20,
        alignItems: 'center',
    },
    label1: {
        fontSize: 15,
        fontWeight: '600',
    },
    titleText: {
        fontSize: 16,
        fontWeight: '600',
    },
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: 'white',
    },
    input:{
        width: '100%',
        padding: 10,
        marginVertical: 10,
        borderWidth: .5,
        borderColor: '#999',
        fontSize: 15,
        borderRadius: 4,
        backgroundColor: '#f0f5f5',
    },
    textarea:{
        minHeight: 150,
    },
    rowActions: {
        justifyContent: 'center',
    },
    btnAction: {
        borderColor: '#999',
        backgroundColor: '#f0f5f5',
        alignSelf: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderWidth: .5,
    },
    btnImage: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    mediaBox: {
        padding: 10,
        marginVertical: 10,
        backgroundColor:"#f9f9f9",
    },
    scrollImagesBox: {
        flexDirection: 'row',
    },
    imageBox: {
        marginHorizontal: 5,
    },
    imgClose: {
        position: 'absolute',
        right: 3,
        top: 3,
        width: 20,
        height: 20,
        alignContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'white',
        backgroundColor: 'rgba(0, 0, 0, .5)',
    },
    separate: {
        paddingTop: 15,
        marginBottom: 15,
        borderBottomWidth: .5,
        borderBottomColor: '#d0d0d0',
    },
});
