import React from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import UserAvatar from "react-native-user-avatar";
import Moment from 'react-moment';
import HTML from 'react-native-render-html';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ImageView from 'react-native-image-viewing';
import clip from "text-clipper";
import OptionsComment from "../OptionsComment";
import UserBadges from "../UserBadges";
import { getMediumThumb, getSmallThumb } from "../../utils/global";

export default class CommentItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            imageCurrentIndex: 0,
            isImageViewVisible: false,
            isContentCollapsed: false,
        };
        this.showImage = this.showImage.bind(this);
        this.setModalViewer = this.setModalViewer.bind(this);
        this._renderCommentContent = this._renderCommentContent.bind(this);
    }

    _onPressRow = () => {
        return this.props.navigation.navigate('StoryDetail', {postId: this.props.topicId});
    }

    _onPressCommentAuthor = () => {
        return this.props.navigation.navigate('UserProfiles', {userId: this.props.author.id});
    }

    showImage = (image) => {
        this.setState({
            imageCurrentIndex: this.props.images.findIndex(_item => _item.id == image.id),
            isImageViewVisible: true,
        })
    }

    setModalViewer = (visible) => {
        this.setState({isImageViewVisible: visible});
    }

    _onPressSetContentCollapsed = () => {
        this.setState((previousState) => ({
            isContentCollapsed: !previousState.isContentCollapsed
        }))
    }

    _renderCommentContent = (_content = this.props.content) => {
        const maxCharacters = 180;
        let clippedHtml =  clip( _content, maxCharacters, { html: true, maxLines: 3, indicator: '... (Read more)'} )

        if (this.state.isContentCollapsed || _content.length < maxCharacters) {
            return (
                <HTML
                    source={{ html: _content }}
                    emSize={15}
                    baseFontStyle={{ fontSize: 15 }}
                    tagsStyles={{
                        p: { marginBottom: 0 }
                    }}
                    parentWrapper={View}
                />
            )
        }
        return (
            <TouchableOpacity
                onPress={() => this._onPressSetContentCollapsed()}
                keyboardShouldPersistTaps={true}
            >
                <HTML
                    source={{ html: clippedHtml }}
                    emSize={15}
                    baseFontStyle={{ fontSize: 15 }}
                    tagsStyles={{
                        p: { marginBottom: 0 }
                    }}
                    parentWrapper={View}
                />
            </TouchableOpacity>
        )
    }

    render() {
        const isLikeButtonClicked = this.props.authenUser !== undefined && this.props.likes.findIndex(_item => _item.user_id == this.props.authenUser.id) > -1;
        let imagesViewer;

        if (this.props.images !== undefined && this.props.images.length > 0) {
            imagesViewer = this.props.images.map(_image => ({
                id: _image.id,
                uri: getMediumThumb(_image.url),
            }))
        }

        return (
            <TouchableOpacity onPress={this._onPressRow}>
                <View style={styles.row}>
                    <View style={styles.panelLeft}>
                        <TouchableOpacity onPress={this._onPressCommentAuthor} >
                            <UserAvatar size={40} name={this.props.author.name} src={getSmallThumb(this.props.author.avatar)} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.panelRight}>
                        <View style={styles.itemDetails}>
                            <UserBadges badgeList={this.props.badgeList} badgeIds={this.props.author.badge_id} />
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity onPress={this._onPressCommentAuthor} style={{marginRight: 5}}>
                                    <Text style={styles.itemName}>{this.props.author.name}</Text>
                                </TouchableOpacity>
                                {this.props.author.is_staff && (
                                    <View style={styles.wrapStaffBadge}>
                                        <Text style={styles.staffText}>{this.props.settingsForum.staff_text}</Text>
                                    </View>
                                )}
                            </View>
                            <View>
                                {this._renderCommentContent()}
                            </View>
                            {this.props.images.length ? (
                                <View style={{ marginTop: 16 }}>
                                    {
                                        this.props.images.map((image, index) => (
                                            <TouchableOpacity onPress={() => this.showImage(image)} key={'image_' + image.id}>
                                                <Image source={{ uri: image.url }} style={styles.commentImage} />
                                            </TouchableOpacity>
                                        ))
                                    }
                                    <ImageView
                                        images={imagesViewer}
                                        imageIndex={this.state.imageCurrentIndex}
                                        animationType={'slide'}
                                        visible={this.state.isImageViewVisible}
                                        onRequestClose={() => this.setModalViewer(false)}
                                    />
                                </View>
                            ) : null}
                        </View>

                        <View style={styles.rightWidget}>
                            <OptionsComment {...this.props} />
                        </View>

                        <View style={styles.itemFooter}>
                            <TouchableOpacity onPress={() => this.props.onPressPostLike(this.props.id)} >
                                <Text style={[ styles.likeButton, {color: isLikeButtonClicked ? '#09af00' : '#007bff'}]}>Like</Text>
                            </TouchableOpacity>
                            <Text style={styles.bullet}>•</Text>
                            <TouchableOpacity onPress={this._onPressRow} >
                                <Text style={styles.replyButton}>Reply</Text>
                            </TouchableOpacity>
                            <Text style={styles.bullet}>•</Text>
                            <Moment fromNow ago element={Text} style={[styles.itemTime]}>{this.props.created}</Moment>
                            {this.props.like_count ? (
                                <View style={styles.likeWrapper}>
                                    <View style={styles.likeIcon}>
                                        <View style={styles.likeBubble}>
                                            <FontAwesome5 name="thumbs-up" solid size={10} color="#fff" />
                                        </View>
                                        <Text>{this.props.like_count}</Text>
                                    </View>
                                </View>
                            ) : null}
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    row: {
        flex: 1,
        flexDirection: 'row',
        paddingHorizontal: 10,
        marginVertical: 5,
    },
    panelLeft: {
        height: 45,
        width: 45,
        paddingRight: 5,
    },
    panelRight: {
        flex: 1,
        flexDirection: 'column',
        position: 'relative',
    },
    wrapStaffBadge: {
        borderRadius: 4,
        backgroundColor: '#dce0e2',
        alignItems: 'center'
    },
    staffText: {
        fontSize: 12,
        color: '#5d707f',
        paddingHorizontal: 7,
        paddingVertical: 2
    },
    rightWidget: {
        position: 'absolute',
        top: 0,
        right: 0,
        zIndex: 99,
    },
    itemDetails: {
        paddingTop: 8,
        paddingBottom: 10,
        paddingHorizontal: 8,
        backgroundColor: 'rgba(0,0,0,.07)',
        borderRadius: 8,
    },
    postAuthor: {
        alignSelf: "flex-start",
    },
    itemName: {
        color: '#007bff',
        fontSize: 14,
        paddingBottom: 3,
        fontWeight: 'bold',
    },
    itemContent: {
        fontSize: 15,
    },
    itemTime: {
        fontSize: 15,
        color: '#999',
    },
    iconTrash: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    likeButton: {
        fontSize: 15
    },
    replyButton: {
        fontSize: 15,
        color: '#007bff',
    },
    bullet: {
        fontSize: 15,
        color: '#939393',
        paddingHorizontal: 5
    },
    itemFooter: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 5,
        paddingHorizontal: 5
    },
    likeWrapper: {
        flex: 1,
        alignItems: 'flex-end',
        textAlign: 'right',
        marginTop: -10,
    },
    likeIcon: {
        flexDirection: 'row',
        paddingVertical: 3,
        paddingHorizontal: 5,
        borderWidth: 1,
        borderColor: '#e0e0e0',
        borderRadius: 50,
        backgroundColor: "#fff",
    },
    likeBubble: {
        backgroundColor: "#007bff",
        paddingVertical: 4,
        paddingHorizontal: 5,
        marginRight: 5,
        borderRadius: 50,
        height: 20,
    },
    commentImage : {
        width: 180,
        height: 101,
    }
});
