import React from "react";
import {BASE_URL_API} from '../../../app.json';
import axios from 'axios';
import CommentItem from "./CommentItem";

export default class Comments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            post: null,
        }
    }

    _onPressPostLike = (_item_id) => {
        const configHeaders = { headers: { Shop: this.props.accessedForum.shop, token: this.props.accessToken } };
        axios.post(BASE_URL_API + '/posts/like.json', {post_id: _item_id, user_id: this.props.loggedUser.id}, configHeaders)
        .then(res => {
            let { data } = res;
            if (data.success) {
                let { comments } = this.props
                comments.map(_post => {
                    if (_post.id == _item_id) {
                        _post.like_count = data.like_count;
                        _post.likes = data.likes;
                    }
                    return _post;
                });
                this.props.refreshCommentsData(comments);
            }
        })
        .catch(error => {
            console.log('like error', error.response)
        })
    }

    render() {
        return this.props.comments.map(item => (
            <CommentItem
                key={'comment_' + item.id}
                {...item}
                {...this.props}
                onPressPostLike={(data) => this._onPressPostLike(data)}
            />
        ))
    }
}
