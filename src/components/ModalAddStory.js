import React, {Component} from 'react';
import {Modal, 
    Text, TextInput, 
    Image, TouchableOpacity, 
    KeyboardAvoidingView, 
    ActivityIndicator, 
    View, Alert, StyleSheet, 
    SafeAreaView, 
    StatusBar,
    ScrollView} from 'react-native';
import {BASE_URL_API, MAX_FILE_SIZE} from '../../app.json';
import axios from 'axios';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// import ImagePicker from 'react-native-image-picker';
import {actions, getContentCSS, RichEditor, RichToolbar} from 'react-native-new-rich-editor';
import ImagePicker from 'react-native-image-crop-picker';
import VideoPlayer from 'react-native-video-player';
import EmbedView from './EmbedView';
import Dialog from "react-native-dialog";

export default class ModalAddStory extends Component {
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            dialogVisible: false,
            content: null,
            progress: 0,
            localImages: [],
            localVideos: [],
            localPoll: null,
            videoURL: null,
            videoURLtmp: null,
        }
    }

    handleDialogOpen = (params) => {
        this.setState({ dialogVisible: true });
    }

    handleDialogCancel = (params) => {
        this.setState({ dialogVisible: false });
    }

    handleDialogOk = (params) => {
        this.setState(previousState => ({
            videoURL: previousState.videoURLtmp,
            dialogVisible: false
        }));
        this.setState({ dialogVisible: false });
    }

    uniqueImages = (_images) => {
        // array1.concat(array2).filter((value, pos, arr) =>  arr.findIndex(item => item.id == value.id) === pos )
        return _images.filter((value, pos, arr) =>  arr.findIndex(item => item.localIdentifier == value.localIdentifier) === pos )
    }

    _onPressPhoto (event) {
        ImagePicker.openPicker({
            multiple: true,
            maxFiles: 10,
        }).then(images => {
            // console.log(images);
            this.setState(previousState => ({
                localImages: this.uniqueImages(previousState.localImages.concat(images)),
            }));
        })
        .catch(error => {
            // E_PERMISSION_MISSING
            // console.log('imagePicker error', error.code, error.message)
            if (error.code == 'E_PERMISSION_MISSING') {
                Alert.alert('Cannot access your gallery', 'You have to go to Settings to enable it.');
            }
        })
    }

    _onPressVideo (event) {
        ImagePicker.openPicker({
            multiple: false,
            mediaType: "video",
        }).then((video) => {
            console.log(video);
            this.setState(previousState => ({
                localVideos: [ video ],
            }));
        })
        .catch(error => {
            // E_PERMISSION_MISSING
            // console.log('imagePicker error', error.code, error.message)
            if (error.code == 'E_PERMISSION_MISSING') {
                Alert.alert('Cannot access your gallery', 'You have to go to Settings to enable it.');
            }
        })
    }

    async submitPost () {
        this.setState({ isLoading: true });
        
        const form = new FormData();
        let { content, localImages, localVideos } = this.state;
        if (content !== null && content !== '') {
            content = content.trim()
        }

        // let _userData = {
        //     user_id: this.props.loggedUser.id,
        //     content: content,
        //     images: localImages,
        //     videos: localVideos,
        // }

        form.append('user_id', this.props.loggedUser.id);
        form.append('content', content);

        if (this.props.categoryId !== undefined) {
            form.append('category_id', this.props.categoryId);
        }

        localImages.forEach(item => {
            form.append('images[]', {
                uri: item.path,
                type: item.mime,
                name: item.filename,
                size: item.size,
            });
        });
    
        localVideos.forEach(item => {
            form.append('videos[]', {
                uri: item.path,
                type: item.mime,
                name: item.filename,
                size: item.size,
            });
        });

        if (this.state.videoURL) {
            form.append('embeds[]', JSON.stringify({
                url: this.state.videoURL,
            }));
        }

        const configHeaders = {
            headers: {
                'Content-Type': 'multipart/form-data',
                Shop: this.props.accessedForum.shop,
                token: this.props.accessToken
            },
            onUploadProgress: progressEvent => {
                var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                console.log('onUploadProgress', percentCompleted)
                this.setState({ progress: percentCompleted });
            },
        };

        axios.post(BASE_URL_API + '/topics/create.json', form, configHeaders)
        .then(res => {
            this.setState({ isLoading: false });
            
            // let data = res.data;
            if (res.data.topic !== undefined && !!res.data.topic) {
                this.setState({
                    content: null,
                    localImages: [],
                    localVideos: [],
                    videoURL: null,
                });
                this.props.updateTopicsData(res.data.topic);
                this.props.setModalVisible(false)
            } else {
                Alert.alert(res.data.error);
            }
        })
        .catch(error => {
            // Alert.alert(error.response.data.data.error);
            console.log('error', error)
            this.setState({
                isLoading: false,
            });
        });
    }

    removeImage(_localIdentifier) {
        this.setState(previousState => ({
            localImages: previousState.localImages.filter(_item => _item.localIdentifier !== _localIdentifier)
        }))
    }

    removeVideo(_localIdentifier) {
        this.setState(previousState => ({
            localVideos: previousState.localVideos.filter(_item => _item.localIdentifier !== _localIdentifier)
        }))
    }

    render() {
        let renderSendBtn;

        if ( ( (!!this.state.content && this.state.content.trim() ) || this.state.localImages.length > 0) && !this.state.isLoading) {
            renderSendBtn = (
                <TouchableOpacity onPress={() => this.submitPost() }>
                    <View style={{paddingLeft: 20 }}>
                        <Ionicons name="md-send" size={25} color="#0066ff" />
                    </View>
                </TouchableOpacity>
            )
        } else {
            renderSendBtn = (
                <View style={{paddingLeft: 20 }}>
                    <Ionicons name="md-send" size={25} color="gray" />
                </View>
            )
        }

        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
            }}>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={styles.header}>
                        <View style={styles.panelLeft}>
                            <TouchableOpacity onPress={() => { this.props.setModalVisible(false); }}>
                                <View>
                                    <FontAwesome5 name="times" size={25} color="gray" />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.panelRight}>
                            <View style={styles.headerActions}>
                                {this.state.isLoading && (
                                    <View style={{ padding: 5 }}><Text>{this.state.progress}%</Text></View>
                                )}
                                <ActivityIndicator animating={this.state.isLoading} />
                                {renderSendBtn}
                            </View>
                        </View>
                    </View>

                    <View style={styles.container}>
                        <ScrollView style={{ flex: 1, minHeight: 300, }} keyboardDismissMode={'none'} keyboardShouldPersistTaps="always">
                            {/* <TextInput
                                style={styles.inputContent}
                                placeholder="What's on your mind?"
                                value={this.state.content}
                                onChangeText={(content) => this.setState({ content })}
                                multiline={true}
                            /> */}

                            <View style={styles.inputWrapper}>
                                <RichEditor
                                    ref={(r) => this.richtext = r}
                                    placeholder={"What's on your mind?"}
                                    editorStyle={{ backgroundColor: '#ffffff' }}
                                    initialContentHTML={this.state.content}
                                    onChange={content => this.setState({ content })}
                                />
                            </View>

                            {this.state.localImages.length > 0 && (
                            <View style={styles.mediaBox}>
                                <ScrollView horizontal={true}>
                                    <View style={styles.scrollImagesBox}>
                                    {this.state.localImages.map((_item, _index) => (
                                        <View style={styles.imageBox} key={'image_' + _index}>
                                            <Image source={{ uri: _item.sourceURL}} style={{width: 60, height: 60}} />
                                            <View style={styles.imgClose}>
                                                <TouchableOpacity onPress={() => this.removeImage(_item.localIdentifier) }>
                                                    <View>
                                                        <Ionicons name="md-close" size={18} color="white" />
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    ))}
                                    </View>
                                </ScrollView>
                            </View>
                            )}

                            {this.state.localVideos.length > 0 && (
                                <View style={styles.mediaBox}>
                                    {this.state.localVideos.map((_item, _index) => (
                                    <View key={'video_' + _item.id} style={ { flex: 1, backgroundColor: 'black' }}>
                                        <VideoPlayer
                                            video={{ uri: _item.sourceURL }}
                                            videoWidth={600}
                                            videoHeight={400}
                                        />
                                        <View style={styles.imgClose}>
                                            <TouchableOpacity onPress={() => this.removeVideo(_item.localIdentifier) }>
                                                <View>
                                                    <Ionicons name="md-close" size={18} color="white" />
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    ))}
                                </View>
                            )}

                            {this.state.videoURL && (
                                <View style={styles.mediaBox}>
                                    <EmbedView url={this.state.videoURL} />
                                    <View style={styles.imgClose}>
                                            <TouchableOpacity onPress={() => this.setState({videoURL: null}) }>
                                                <View>
                                                    <Ionicons name="md-close" size={18} color="white" />
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                </View>
                            )}
                        </ScrollView>

                        <KeyboardAvoidingView behavior={'position'} keyboardVerticalOffset={50} >
                            <View style={styles.rowFooter}>
                                <TouchableOpacity style={styles.button} onPress={this._onPressPhoto.bind(this)} keyboardShouldPersistTaps={true}>
                                        <FontAwesome5 name="image" size={25} color="#44bc64" />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.button} onPress={this._onPressVideo.bind(this)} >
                                    <FontAwesome5 name="video" solid size={25} color="#9c6efb" />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.button} onPress={this.handleDialogOpen}>
                                    <FontAwesome5 name="youtube" brand size={25} color="#fe6324" color_old="#fe6324" />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.button} onPress={null}>
                                    <FontAwesome5 name="product-hunt" brand size={25} color="#d0d0d0" color_old="#1576f2" />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.button} onPress={null}>
                                    <FontAwesome5 name="poll" solid size={25} color="#d0d0d0" color_old="#f1bb22" />
                                </TouchableOpacity>
                            </View>
                        </KeyboardAvoidingView>

                    </View>
                </SafeAreaView>

                <Dialog.Container visible={this.state.dialogVisible} onBackdropPress={this.handleDialogCancel}>
                    <Dialog.Title>Title</Dialog.Title>
                    <Dialog.Description>
                        Please enter your youtube or vimeo URL then click 'OK' button.
                    </Dialog.Description>
                    <Dialog.Input 
                        placeholder={'https://vimeo //youtube...'}
                        onChangeText={(videoURLtmp) => this.setState({ videoURLtmp })}
                    />
                    <Dialog.Button label="Cancel" onPress={this.handleDialogCancel} />
                    <Dialog.Button label="Ok" onPress={this.handleDialogOk} />
                </Dialog.Container>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    header: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 15,
        backgroundColor: '#ffffff',
        borderBottomWidth: 1,
        borderBottomColor: '#e9e9e9',
    },
    panelLeft: {
        width: '50%',
        flexDirection: 'row',
    },
    panelRight: {
        // flex: 1,
        width: '50%',
        alignContent: 'flex-end',
        alignItems: 'flex-end',
    },
    headerActions: {
        flexDirection: 'row',
    },
    inputWrapper: {
        flex: 1,
        marginBottom: 25,
        backgroundColor: '#ff0000',
    },
    inputContent: {
        // flex: 1,
        // borderWidth: 0,
        // borderColor: '#99c2ff',
        fontSize: 16,
        // backgroundColor: '#f0f5f5',
    },
    rowFooter: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      backgroundColor: '#fff',
      borderTopWidth: 1,
      borderTopColor: '#d0d0d0',
    },
    mediaBox: {
        paddingVertical: 15,
        marginHorizontal: -5,
        backgroundColor:"#f9f9f9",
    },
    imageBox: {
        marginHorizontal: 5,
    },
    scrollImagesBox: {
        flexDirection: 'row',
    },
    button: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
        marginVertical: 8,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 10,
    },
    buttonText: {
        fontSize: 18,
        color:'#ffffff',
        textAlign: 'center',
    },
    buttonCancel: {
        fontSize: 18,
        color: '#666',
    },
    imgClose: {
        position: 'absolute',
        right: 3,
        top: 3,
        width: 20,
        height: 20,
        alignContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'white',
        backgroundColor: 'rgba(0, 0, 0, .5)',
    }
});
