import React, { Component } from "react";
import { Text, View, Image, StyleSheet, TouchableOpacity, Animated } from "react-native";
import Moment from 'react-moment';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default class PollView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: null,
        };
    }

    changeVote = (topic_id, poll_id, option_id) => {
        const { changeVote } = this.props;
        changeVote(topic_id, poll_id, option_id)

        this.setState({
            selectedOption: option_id,
        });
    }

    render() {
        const { data } = this.props;
        const totalVotes = data.options.reduce((total, item) => total + item.votes_count, 0);
        const today = new Date();
        const expiration_date = new Date(data.expiration_date);
        let highestValue = 0;

        for (i = 0; i < data.options.length; i++) {
            if (data.options[i].votes_count > highestValue) {
                highestValue = data.options[i].votes_count
            }
        }

        return (
            <View style={styles.poll}>
                {data.options.map((option, index) => (
                    <View style={styles.optionRow} key={'option_' + index}>
                        {(data.expiration_date && expiration_date <= today) ? (
                            <View style={{ width: 40, height: '100%', padding: 5 }}>
                                {(highestValue > 0) && highestValue == option.votes_count && (
                                    <FontAwesome5 name="check" size={16} color="#28a745" />
                                )}
                            </View>
                        ): (
                        <View style={{ width: 40, height: '100%', padding: 5 }}>
                            <TouchableOpacity style={styles.radioCircle}
                                onPress={() => this.changeVote(data.topic_id, data.id, option.id) }>
                                {this.state.selectedOption === option.id && <View style={ styles.selectedRadio }></View>}
                            </TouchableOpacity>
                        </View>
                        )}
                        <View style={{ flex: 1 }}>
                            <TouchableOpacity activeOpacity={0.7} key={index} onPress={ () => this.changeVote(data.topic_id, data.id, option.id) } disabled={data.expiration_date && expiration_date <= today}>
                                <View styles={styles.optionBar}>
                                    <View style={[StyleSheet.absoluteFill, styles.borderRadius, {backgroundColor: '#e9ecef'}]}/>
                                    <Animated.View
                                        style={[totalVotes && option.votes_count == totalVotes ? styles.borderRadius : styles.borderRadiusLeft, styles.progress, {
                                            width: totalVotes ? option.votes_count / totalVotes * 100 +'%' : 0,
                                        }]}
                                    />
                                    <Text style={ styles.pollAnswer }>{option.answer}</Text>
                                </View>
                            </TouchableOpacity>
                            <View style={{ marginTop: 3 }}>
                                <Text style={styles.textMuted}>{option.votes_count}&nbsp;{option.votes_count > 1 ? 'votes' : 'vote'}</Text>
                            </View>
                        </View>
                    </View>
                ))}

                <View style={{flex: 1, flexDirection: 'row', marginBottom: 10 }}>
                    <Text style={styles.textMuted}>Total:&nbsp;{totalVotes}&nbsp;{totalVotes > 1 ? 'votes' : 'vote'}</Text>
                    {data.expiration_date && (
                        (expiration_date <= today) ? (
                            <Text style={[styles.textMuted, styles.expirationDate]}>
                                This poll expired in <Moment fromNow element={Text}>{data.expiration_date}</Moment>
                            </Text>
                        ): (
                            <Text style={[styles.textMuted, styles.expirationDate]}>
                                This poll will expire <Moment fromNow element={Text}>{data.expiration_date}</Moment>
                            </Text>
                        )
                    )}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    poll: {
        flex: 1,
        flexDirection: 'column',
    },
    optionRow: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 10,
    },
    optionBar: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 5,
        position: 'relative',
    },

	radioCircle: {
        width: 20,
        height: 20,
		borderRadius: 100,
		borderWidth: 2,
		borderColor: '#999999',
		alignItems: 'center',
		justifyContent: 'center',
	},
	selectedRadio: {
		width: 10,
		height: 10,
		borderRadius: 50,
		backgroundColor: '#00cc99',
    },

    pollAnswer: {
        fontSize: 15,
        paddingVertical: 5,
        paddingHorizontal: 10
    },

    textMuted: {
        color: '#6c757d',
        fontWeight: '400',
        fontSize: 15,
    },
    borderRadius: {
        borderRadius: 5,
        borderColor: 'rgba(0, 0, 0, .2)',
    },
    borderRadiusLeft: {
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        borderColor: 'rgba(0, 0, 0, .2)',
    },
    progress: {
        position: "absolute",
        left: 0,
        top: 0,
        bottom: 0,
        backgroundColor: '#cadfff',
    },
    expirationDate: {
        flex: 1,
        justifyContent: 'flex-end',
        textAlign: 'right'
    }
});