import React, { Component } from "react";
import { Text, View, Image, StyleSheet, TouchableOpacity, Animated } from "react-native";

export default function EmbedView( props ) {
    let [_image, onChangeImage] = React.useState(null);

    // http://img.youtube.com/vi/F4rBAf1wbq4/mqdefault.jpg

    const youtube_parser = (url) => {
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }
    
    const vimeo_parser = (url) => {
        var vimeo_Reg = /https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/;
        var match = url.match(vimeo_Reg);
        return match ? match[3] : false;
    }
    
    if (props.url.indexOf('youtube.com') !== -1 || props.url.indexOf('youtu.be') !== -1) {
        var _videoId = youtube_parser(props.url);
        _image = 'http://img.youtube.com/vi/'+_videoId+'/mqdefault.jpg';
        // onChangeImage('http://img.youtube.com/vi/'+_videoId+'/mqdefault.jpg')
    } else if (props.url.indexOf('vimeo.com') !== -1) {
        var _videoId = vimeo_parser(props.url);
        _jsonUrl = 'https://vimeo.com/api/v2/video/'+_videoId+'.json';
        // console.log('_jsonUrl', _jsonUrl)

        fetch(_jsonUrl).then((response) => response.json())
        .then((json) => {
            onChangeImage(json[0]['thumbnail_large'])
        })
        .catch((error) => {
            console.log(error);
        });
    }


    return (
        <View style={{ flex: 1 }}>
            <TouchableOpacity onPress={() => props.viewUrlModal(props.url)}>
                <View style={ styles.row }>
                    <Image source={{ uri: _image }} style={{ width: '100%', height: 250 }} />
                    <View style={ styles.centerVideo}>
                        <Image style={{ tintColor: '#fff' }} source={require('../../assets/player.png')} />
                    </View>
                    
                    <View style={styles.titleBox}>
                        <Text style={ styles.mainText }>{props.url}</Text>
                    </View>

                </View>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    row: {
        borderWidth: .5,
        borderColor: '#d0d0d0',
        borderRadius: 0,
    },
    centerVideo: {
        position: 'absolute',
        marginLeft: -35,
        left: '50%',
        top: 100,
        backgroundColor: 'rgba(30, 30, 30, 0.7)',
        borderRadius: 100,
        padding: 5,
        borderWidth: 2,
        borderColor: '#fff',
    },
    titleBox: {
        padding: 10,
    },
    mainText: {
        fontSize: 15,
        color: '#909090',
    }

});
