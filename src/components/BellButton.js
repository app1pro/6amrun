import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

export default function BellButton (props) {
    const unreadNotifications = useSelector(state => state.unreadNotifications );
    const MAX_VALUE = 99;
    // console.log('unreadNotifications', unreadNotifications);

    return (
        <View style={{ flexDirection: 'row' }}>
            {props.icon}
            {(unreadNotifications > 0) && (
                <View style={ styles.badgeIcon }>
                    <Text style={ styles.badgeText }>{unreadNotifications > MAX_VALUE ? MAX_VALUE + '+' : unreadNotifications.toString()}</Text>
                </View>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    badgeIcon: {
        position: 'absolute',
        top: -7,
        right: -5,
        backgroundColor: 'red',
        borderRadius: 30,
        paddingVertical: 1,
        paddingHorizontal: 4,
        zIndex: 99,
    },
    badgeText: {
        color: 'white',
        fontSize: 11,
        textAlign: 'center'
    }
});
