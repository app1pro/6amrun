import React from "react";
import { Text, View, StyleSheet } from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default function UserLocation( props ) {
    const location = (_user) => {
        let location;
        switch (_user.location_display) {
            case 'full':
                location = [_user.city, _user.province, _user.country].join(', ');
                break;
            case 'city-country':
                location = [_user.city, _user.country].join(', ');
                break;
            case 'province-country':
                location = [_user.province, _user.country].join(', ');
                break;
            case 'city-only':
                location = _user.city;
                break;
            case 'country-only':
            default:
                location = _user.country;
                break;
        }
        return location;
    }

    if (
        !props.settingsForum
        || !props.settingsForum.user_location_enable
        || !props.currentUser
        || !props.currentUser.country
        || props.currentUser.location_display === 'none'
    ) {
        return null;
    }

    return (
        <View style={styles.rowProfileLocation}>
            <FontAwesome5 name="map-marker-alt" size={12} color="green" solid />
            <Text style={styles.locationLabel}>&nbsp;{location(props.currentUser)}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    rowProfileLocation: {
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 15,
    },
    locationLabel: {
        color: 'green',
        fontSize: 12,
    },
});