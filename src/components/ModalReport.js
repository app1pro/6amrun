import React, { Component } from "react";
import { Text, View, ScrollView, TextInput, SafeAreaView, TouchableOpacity, StyleSheet, Alert, Modal, StatusBar, ActivityIndicator } from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {name as APP_NAME, BASE_URL_API} from '../../app.json';
import axios from 'axios';

const reasonList = [
    'Harassment',
    'Spam',
    'Hate',
    'Plagiarism',
    'Joke Answer',
    'Poorly Written',
    'Impersonation',
    'Copyright violation',
    'Trademark violation',
    'Factually Incorrect',
    'Adult Content'
];

export default class ModalReport extends Component {
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            object_id: null,
            type: null,
            reason: null,
            message: null,
            title: '',
        }
    }

    async submitForm() {
        this.setState({ isLoading: true, })

        let { message } = this.state;
        if (message !== null && message !== '') {
            message = message.trim();
        }

        let formData = {
            user_id: this.props.loggedUser.id,
            object_id: this.props.object_id,
            type: this.props.type,
            reason: this.state.reason,
            message: message,
        }

        const configHeaders = {
            headers: {
                Shop: this.props.accessedForum.shop,
                token: this.props.accessToken,
            },
        };
        axios.post(BASE_URL_API + '/topics/report.json', formData, configHeaders)
        .then(res => {
            let {data} = res;

            if (data.success) {
                this.closeModal();
                Alert.alert('Thank you for reporting!');
                this.setState({
                    isLoading: false,
                })
            }
        })
        .catch(error => {
            this.setState({
                isLoading: false,
            })
            console.log('Send report error', error.response)
        })
    }

    closeModal = () => {
        this.setState({
            reason: '',
            message: '',
        })
        this.props.setModalVisible(false);
    }

    render() {
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.modalVisible}
            >
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={styles.header}>
                        <View style={styles.panelLeft}>
                            <TouchableOpacity onPress={() => this.closeModal()}>
                                <View>
                                    <FontAwesome5 name="times" size={25} color="gray" />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={styles.titleText}>Submit a report</Text>
                        </View>
                        <View style={styles.panelRight}>
                            {this.state.isLoading && (
                                <View style={styles.wrapLoading}>
                                    <ActivityIndicator size={16} color="gray" animating={this.state.isLoading} />
                                    <View style={{width: 10}}/>
                                </View>
                            )}
                        </View>
                    </View>
                    <View style={styles.container}>
                        <View style={styles.sectionContent}>
                            <View style={{ marginBottom: 20, flexDirection:'row', flexWrap:'wrap' }}>
                                {reasonList.map(item => (
                                    <TouchableOpacity key={'reason_' + Math.random()} style={[styles.radioCircle, item === this.state.reason ? styles.selectedRadio: null]}
                                        onPress={() => this.setState({
                                            reason: item,
                                        })}
                                    >
                                        <Text style={[ item === this.state.reason ? styles.selectedRadioText: null ]}>{item}</Text>
                                    </TouchableOpacity>
                                ))}
                            </View>

                            <View>
                                <Text style={styles.label1}>Explain this report (Optional)</Text>
                                <TextInput
                                    style={[ styles.input, styles.textarea ]}
                                    value={this.state.message}
                                    onChangeText={value =>
                                        this.setState({
                                            message: value,
                                        })
                                    }
                                    textAlignVertical={'top'}
                                    multiline={true}
                                    numberOfLines={4}
                                    underlineColorAndroid="transparent"
                                />
                            </View>
                        </View>

                        <View style={styles.sectionFooter}>
                            <TouchableOpacity onPress={() => this.submitForm()} style={styles.btnAction} disabled={!this.state.reason || this.state.isLoading}>
                                <View style={styles.btnSend}>
                                    <Text style={[styles.label1]}>Send report</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </SafeAreaView>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: '#ffffff',
        borderBottomWidth: 1,
        borderBottomColor: '#e9e9e9',
        justifyContent: 'space-between'
    },
    headerActions: {
        flexDirection: 'row',
    },
    panelRight: {
        flexDirection: 'row',
    },
    wrapLoading: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 2,
    },
    sectionHeader: {
        paddingVertical: 20,
        alignItems: 'center',
    },
    label1: {
        fontSize: 15,
        fontWeight: '600',
    },
    titleText: {
        fontSize: 16,
        fontWeight: '600',
    },
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: 'white',
    },
	radioCircle: {
        paddingVertical: 7,
        paddingHorizontal: 12,
		borderRadius: 50,
		borderWidth: 1,
		borderColor: '#999999',
		alignItems: 'center',
        marginBottom: 10,
        marginRight: 5,
	},
	selectedRadio: {
		backgroundColor: '#00cc99',
    },
	selectedRadioText: {
        color: '#fff',
    },
    input: {
        width: '100%',
        padding: 10,
        marginVertical: 10,
        borderWidth: .5,
        borderColor: '#999',
        fontSize: 15,
        borderRadius: 4,
        backgroundColor: '#f0f5f5',
    },
    textarea: {
        minHeight: 100,
    },
    sectionFooter: {
        marginTop: 20,
        borderColor: '#999',
        backgroundColor: '#f0f5f5',
        alignSelf: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderWidth: .5,
    },
    btnSend: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    btnSendDisabled: {
        color: '#999',
    },
});
