import React from 'react';
import {
  StyleSheet,
  View,
  Modal,
  ActivityIndicator
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: -120,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  activityIndicatorWrapper: {
    backgroundColor: '#ffffff',
    height: 50,
    width: 50,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: '#bbb',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
});

export default function LoadingIcon(props ) {
    return (
        <View style={styles.container}>
            <View style={styles.activityIndicatorWrapper}>
                <ActivityIndicator
                    animating={true} color="#00ed6b" />
            </View>
        </View>
    );
}
