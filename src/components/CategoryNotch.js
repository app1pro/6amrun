import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native';

export default function CategoryNotch ( props ) {
    const navigation = useNavigation();

    const categoryIcon = (category) => {
        let iconAttribute = category.icon.split(' ');
        let icon = iconAttribute[1].substring(3);

        switch (iconAttribute[0]) {
            case 'fas':
                return (
                    <FontAwesome5 name={icon} solid size={12} color={category.color || '#6c757d' } />
                )
            case 'fab':
                return (
                    <FontAwesome5 name={icon} brand size={12} color={category.color || '#6c757d'} />
                )
            default:
                return (
                    <FontAwesome5 name={icon} size={12} color={category.color || '#6c757d'} />
                )
        }
    }

    const onPressNotch = (category) => {
        return navigation.navigate('Categories', {
            screen: 'CategoriesDetail',
            initial: false,
            params: {
                categoryId: category.id,
                title: category.title
            }
        });
    }

    return (
        <TouchableOpacity onPress={() => onPressNotch(props)} style={styles.wrapNotch}>
            {props.icon ? (
                <View style={styles.wrapIcon}>
                    {categoryIcon(props)}
                </View>
            ) : null}
            <View>
                <Text style={styles.categoryTitle}>{props.title}</Text>
            </View>
            <View style={styles.notchCorner}></View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    wrapNotch: {
        flexDirection: 'row',
        alignSelf: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
        position: "relative",
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        paddingVertical: 4,
        paddingLeft: 16,
        paddingRight: 16,
        marginLeft: 8,
    },
    notchCorner: {
        position: 'absolute',
        top: 0,
        right: -12,
        borderStyle: 'solid',
        borderRightWidth: 15,
        borderBottomWidth: 25,
        borderRightColor: "transparent",
        borderBottomColor: "#fff"
    },
    wrapIcon: {
        marginRight: 8
    },
    categoryTitle: {
        fontSize: 12,
        color: '#6c757d'
    },
})