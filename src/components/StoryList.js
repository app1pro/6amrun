import React from "react";
import { View, Text, FlatList, Alert, StyleSheet, TouchableOpacity } from "react-native";
import {STORAGE_KEY, BASE_URL_API} from '../../app.json';
import axios from 'axios';
import StoryItem from './StoryItem';
import Clipboard from '@react-native-community/clipboard';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import { Portal } from 'react-native-portalize';
import { ModalLikesList } from './ModalLikesList';
import { FacebookWebView } from './FacebookWebView';
import ModalReport from './ModalReport';

export default class StoryList extends React.PureComponent {
    constructor(props){
        super(props);
        this.state = {
            likesList: [],
            modalVisible: false,
            report_object_id: null,
            report_type: null,
            webViewUrl: null,
        }

        this.modalizeRef = React.createRef(null).current;
        this.modalizeRef2 = React.createRef(null).current;
    }

    _onPressItem = (_post) => {
        return this.props.navigation.navigate('StoryDetail', {postId: _post.id});
    }

    _onPressUser = (_post) => {
        return this.props.navigation.navigate('UserProfiles', {userId: _post.author.id});
    }

    _copyURL = (_item) => {
        Clipboard.setString(_item.url)
        showMessage({
            message: "URL has been copied to Clipboard",
            type: "default",
            floating: true,
        });
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    _openSendReport(data) {
        this.setState({
            report_object_id: data.object_id,
            report_type: data.type,
        })

        setTimeout( () => this.setModalVisible(true), 100)
    }

    _viewLikesList = (_item_id) => {
        // Alert.alert('View LIKE list', _item_id + '');
        // console.log('modalizeRef.current', this.modalizeRef.current);

        const configHeaders = { headers: { Shop: this.props.accessedForum.shop, token: this.props.accessToken } };
        axios.get(BASE_URL_API + '/topics/likes.json?topic_id='+_item_id, configHeaders)
        .then(res => {
            let { data } = res;

            if (data.success) {
                this.setState({
                    likesList: data.likes
                });
                this.modalizeRef?.open();
            }
        })
        .catch(error => {
            console.log('likes error', error.response)
        })
    }

    _editTopic = (_item) => {
        // alert('clicked ' + _item.id)
        return this.props.navigation.navigate('PostForm', { post: _item });
    }

    _hideTopic = (_item) => {
        const configHeaders = { headers: { Shop: this.props.accessedForum.shop, token: this.props.accessToken } };
        axios.post(BASE_URL_API + '/topics/hide.json', {topic_id: _item.id, user_id: this.props.loggedUser.id}, configHeaders)
        .then(res => {
            let { data } = res;
            // console.log('hideTopic', data)

            if (data.success) {
                let dataList = this.props.data
                this.props.updateData(
                    dataList.filter(_topic => _topic.id != _item.id)
                );
            }
        })
        .catch(error => {
            console.log('hide topic error', error.response)
        })
    }

    _deleteTopic = (_item) => {
        const configHeaders = { headers: { Shop: this.props.accessedForum.shop, token: this.props.accessToken } };
        axios.post(BASE_URL_API + '/topics/delete.json', {topic_id: _item.id, user_id: this.props.loggedUser.id}, configHeaders)
        .then(res => {
            let { data } = res;

            if (data.success) {
                let dataList = this.props.data
                this.props.updateData(
                    dataList.filter(_topic => _topic.id != _item.id)
                );
            }
        })
        .catch(error => {
            console.log('delete topic error', error.response)
        })
    }

    _viewUrlModal = (_url) => {
        this.setState({
            webViewUrl: _url
        });
        this.modalizeRef2?.open();
    }

    _onPressLike(_item_id) {
        // Alert.alert('liked', _item_id + '')
        const configHeaders = { headers: { Shop: this.props.accessedForum.shop, token: this.props.accessToken } };
        axios.post(BASE_URL_API + '/topics/like.json', {topic_id: _item_id, user_id: this.props.loggedUser.id}, configHeaders)
        .then(res => {
            let { data } = res;
            // console.log('like', data)

            if (data.success) {
                let dataList = this.props.data
                dataList.map(_topic => {
                    if (_topic.id == _item_id) {
                        _topic.like_count = data.like_count;
                        _topic.likes = data.likes;
                    }
                    return _topic
                })
                this.props.updateData(dataList)
            }
        })
        .catch(error => {
            console.log('like error', error.response)
        })
    }

    _changeVote(topic_id, poll_id, option_id) {
        // alert(poll_id + ' - ' + option_id);
        const configHeaders = { headers: { Shop: this.props.accessedForum.shop, token: this.props.accessToken } };
        axios.post(BASE_URL_API + '/topics/poll-vote.json', {poll_id: poll_id, option_id: option_id, user_id: this.props.loggedUser.id}, configHeaders)
        .then(res => {
            let { data } = res;
            if (data.success) {
                let dataList = this.props.data
                dataList.map(_topic => {
                    if (_topic.id == topic_id) {
                        _topic.poll.options = data.options;
                    }
                    return _topic
                })
                this.props.updateData(dataList)
            }
        })
        .catch(error => {
            console.log('poll vote error', error.response)
        })
    }

    _editComment = (_data, _topic_id) => {
        let dataList = this.props.data;

        dataList.map(_topic => {
            if (_topic.id == _topic_id) {
                 _topic.posts = _topic.posts.map(comment => {
                    if (comment.id == _data.id) {
                        comment.content = _data.content;
                    }
                    return comment;
                });
            }
            return _topic;
        })

        this.props.updateData(dataList);
    }

    _deleteComment = (_item_id, _topic_id) => {
        let dataList = this.props.data;

        dataList.map(_topic => {
            if (_topic.id == _topic_id) {
                _topic.posts = _topic.posts.filter(comment => comment.id !== _item_id);
                _topic.no_posts -= 1;
            }
            return _topic;
        });

        this.props.updateData(dataList);
    }

    _renderItem = ({item}) => {
        if (item.products.length) {
            item.products.map(_product => {
                if (_product.product_url.indexOf('//') === -1) {
                    _product.product_url = 'https://' + this.props.accessedForum.domain + '/products/' + _product.product_url
                }

                return _product;
            })
        }

        return (
            <StoryItem
                id={item.id}
                content={item.content}
                created={item.created}
                author={item.author}
                url={item.url}
                likes={item.likes}
                like_count={item.like_count}
                no_posts={item.no_posts}
                comments={item.posts || []}
                images={item.images || []}
                products={item.products || []}
                embeds={item.embeds || []}
                poll={item.poll}
                videos={item.videos || []}
                pinned={item.pinned}
                locked={item.locked}
                category={item.category}
                showCategory={this.props.showCategory}
                authenUser={this.props.loggedUser}
                onPressItem={_user => this._onPressItem(_user)}
                onPressUser={_user => this._onPressUser(_user)}
                viewLikesList={_item => this._viewLikesList(_item)}
                copyURL={() => this._copyURL(item)}
                editTopic={() => this._editTopic(item)}
                hideTopic={() => this._hideTopic(item)}
                deleteTopic={() => this._deleteTopic(item)}
                viewUrlModal={(_item) => this._viewUrlModal(_item)}
                onPressLike={(_item) => this._onPressLike(_item)}
                openSendReport={_data => this._openSendReport(_data)}
                changeVote={(topic_id, poll_id, option_id) => this._changeVote(topic_id, poll_id, option_id)}
                badges={this.props.badges}
                settingsForum={this.props.settingsForum}
                editComment={(data) => this._editComment(data, item.id)}
                deleteComment={(data) => this._deleteComment(data, item.id)}
                navigation={this.props.navigation}
                accessedForum={this.props.accessedForum}
                accessToken={this.props.accessToken}
                loggedUser={this.props.loggedUser}
            />
        )
    };

    render() {
        return (
            <>
                <FlatList
                    { ...this.props }
                    keyExtractor={(item, index) => 'item_' + item.id}
                    renderItem={this._renderItem}
                />

                <ModalReport
                    modalVisible={this.state.modalVisible}
                    setModalVisible={_state => this.setModalVisible(_state)}
                    accessedForum={this.props.accessedForum}
                    accessToken={this.props.accessToken}
                    loggedUser={this.props.loggedUser}
                    object_id={this.state.report_object_id}
                    type={this.state.report_type}
                />

                <Portal>
                    <ModalLikesList ref={el => this.modalizeRef = el} data={this.state.likesList} />
                    <FacebookWebView ref={el => this.modalizeRef2 = el} url={this.state.webViewUrl} />
                </Portal>
                <FlashMessage position="bottom" />
            </>
        );
    }
}
