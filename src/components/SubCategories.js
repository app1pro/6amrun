import React,{ Component } from "react";
import { Text, View, TouchableOpacity, StyleSheet } from "react-native";
import { useNavigation } from '@react-navigation/native';
import SubCategoryItem from "./SubCategoryItem";

export default function SubCategories (props) {
    const navigation = useNavigation();

    const onPressItem = (category) => {
        navigation.replace('CategoriesDetail', { categoryId: category.id, title: category.title});
    }

    if (props.categoryList == null || !props.categoryList.length) {
        return null;
    }

    return (
        <View style={{ marginTop: 10, marginHorizontal: 10}}>
            {props.categoryList.map((item, index) => (
                 <SubCategoryItem
                    key={'category_item_' + item.id}
                    {...item}
                    onPressItem={category => onPressItem(category)}
                    borderTopRadius={index == 0 ? true : false}
                    borderBottomRadius={index == props.categoryList.length - 1 ? true : false}
                />
            ))}
        </View>
    )
}