import React from "react";
import { View, Text, Image, Alert, StyleSheet, TouchableOpacity } from "react-native";
// import EntypoIcon from 'react-native-vector-icons/Entypo';
import HTML from 'react-native-render-html';
import PhotoGrid from './PhotoGrid';
import ProductView from './ProductView';
import EmbedView from './EmbedView';
import PollView from './PollView';
import VideoPlayer from 'react-native-video-player';
import ImageView from 'react-native-image-viewing';
import clip from "text-clipper";
import { getMediumThumb } from "../utils/global";

export default class StoryItemData extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            imageCurrentIndex: 0,
            isImageViewVisible: false,
            isCollapsed: false,
            // authenUser: null,
        }
    }

    showImage(event, image) {
        // console.log('clicked', event, image)
        // Alert.alert('clicked', item.toString() )
        this.setState({
            imageCurrentIndex: this.props.images.findIndex(_item => _item.id == image.id),
            isImageViewVisible: true,
        })
    }

    setModalViewer(visible) {
        this.setState({isImageViewVisible: visible});
    }

    _onPressContent = () => {
        this.setState({ isCollapsed: true });
    }

    render() {
        let imagesViewer, renderContent;
        const tagsStyles = {
            p: { marginBottom: 5 }
        };
        const classesStyles = {
            'mention': { backgroundColor: 'rgba(96, 160, 206, 0.25)', borderRadius: 6, paddingVertical: 3 }
        };

        if (this.props.likes && this.props.likes.length > 0) {
            var likesList = this.props.likes.map(item => item.user_name);
            renderLikesList = likesList.join(', ');
            if (this.props.like_count > this.props.likes.length) {
                renderLikesList += ' and ' + (this.props.like_count - this.props.likes.length) + ' others';
            }
        }

        if (this.props.images !== undefined && this.props.images.length > 0) {
            imagesViewer = this.props.images.map(_image => ({
                id: _image.id,
                uri: getMediumThumb(_image.url),
            }))
        }

        let clippedHtml = clip(this.props.content, 180, { html: true, maxLines: 3, indicator: '... (Read more)' });

        if (!this.props.content) {
            renderContent = null;
        } else if (this.state.isCollapsed || clippedHtml === this.props.content) {
            renderContent = (
                <HTML
                    source={{ html: this.props.content }}
                    emSize={15}
                    baseFontStyle={{ fontSize: 15 }}
                    tagsStyles={tagsStyles}
                    classesStyles={classesStyles}
                />
            )
        } else {
            renderContent = (
                <TouchableOpacity onPress={this._onPressContent} keyboardShouldPersistTaps={true}>
                    <HTML
                        source={{ html: clippedHtml }}
                        emSize={15}
                        baseFontStyle={{ fontSize: 15 }}
                        tagsStyles={tagsStyles}
                        classesStyles={classesStyles}
                    />
                </TouchableOpacity>
            )
        }

        return (
            <View>
                <View style={styles.rowBody}>
                    { renderContent }
                </View>

                {this.props.embeds && (
                    <View style={styles.rowEmbeds}>
                        {this.props.embeds.map(_item => (
                            <EmbedView key={'embed_' + _item.id} {..._item} viewUrlModal={this.props.viewUrlModal} />
                        ))}
                    </View>
                )}

                {this.props.products && (
                    <View style={styles.rowProducts}>
                        {this.props.products.map(_item => (
                            <ProductView key={'product_' + _item.id} {..._item} viewUrlModal={this.props.viewUrlModal} />
                        ))}
                    </View>
                )}

                {this.props.poll && (
                    <View style={styles.rowPoll}>
                        <PollView data={this.props.poll} changeVote={this.props.changeVote} />
                        {/* <Text>{JSON.stringify(this.props.poll)}</Text> */}
                    </View>
                )}

                {this.props.images && (
                    <View style={styles.rowImages}>
                        <PhotoGrid source={this.props.images} onPressImage={(event, image) => this.showImage(event, image)} />
                        <ImageView
                            images={imagesViewer}
                            imageIndex={this.state.imageCurrentIndex}
                            animationType={'slide'}
                            visible={this.state.isImageViewVisible}
                            onRequestClose={() => this.setModalViewer(false)}
                        />
                    </View>
                )}

                {this.props.videos ? this.props.videos.map((item, key) => (
                        <View key={'video_' + item.id} style={ { flex: 1, backgroundColor: 'black', marginHorizontal: -10 }}>
                            <VideoPlayer
                                video={{ uri: item.url }}
                                videoWidth={1600}
                                videoHeight={900}
                            />
                        </View>
                    )
                ) : null}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    row: {
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: '#ffffff',
    },
    // body
    rowBody: {
        paddingTop: 10,
        marginBottom: 0,
    },
    rowImages: {
        marginLeft: -10,
        marginBottom: 5,
    },
    rowEmbeds: {
        marginBottom: 5,
    },
    rowProducts: {
        marginBottom: 5,
    },
    separator: {
        height: 1,
        width: '100%',
        backgroundColor: '#e9e9e9',
    },
});

