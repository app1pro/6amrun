import React, { Component } from "react";
import { View, Alert, StyleSheet } from "react-native";
import {BASE_URL_API} from '../../app.json';
import axios from 'axios';
import Clipboard from '@react-native-community/clipboard';
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import OptionsMenu from 'react-native-option-menu';

export default class OptionsTopic extends Component {
    constructor(props) {
        super(props);
    }

    _copyURL = () => {
        let _item = this.props.topicData;
        Clipboard.setString(_item.url)
        showMessage({
            message: "URL has been copied to Clipboard",
            type: "default",
            floating: true,
        });
    }

    showEditForm = () => {
        let _item = this.props.topicData;
        return this.props.navigation.navigate('PostForm', { post: _item });
    }

    reportPost = () => {
        return this.props.openSendReport({object_id: this.props.topicData.id, type: 'topic'});
    }

    hide = () => {
        let _item = this.props.topicData;
        const configHeaders = { headers: { Shop: this.props.accessedForum.shop, token: this.props.accessToken } };
        axios.post(BASE_URL_API + '/topics/hide.json', {topic_id: _item.id, user_id: this.props.loggedUser.id}, configHeaders)
        .then(res => {
            let { data } = res;
            if (data.success) {
                let { topics } = this.props;
                if (topics && topics.length) {
                    topics = topics.filter(topic => topic.id != _item.id);
                    return this.props.refreshTopicsData(topics);
                }

                this.props.refreshTopicsData();
            }
        })
        .catch(error => {
            console.log('hide topic error', error.response)
        })
    }

    hideAlert = () => {
        Alert.alert(
            "Do you want to hide this topic?",
            null,
            [
            {
                text: "Cancel",
                style: "cancel"
            },
            { text: "OK", onPress: this.hide }
            ],
            { cancelable: false }
        );
    }

    delete = () => {
        let _item = this.props.topicData;
        const configHeaders = { headers: { Shop: this.props.accessedForum.shop, token: this.props.accessToken } };
        axios.post(BASE_URL_API + '/topics/delete.json', {topic_id: _item.id, user_id: this.props.loggedUser.id}, configHeaders)
        .then(res => {
            let { data } = res;

            if (data.success) {
                let { topics } = this.props;
                if (topics && topics.length) {
                    topics = topics.filter(topic => topic.id != _item.id);
                    return this.props.refreshTopicsData(topics);
                }

                this.props.refreshTopicsData();
            }
        })
        .catch(error => {
            console.log('delete topic error', error.response)
        })
    }

    deleteAlert = () => {
        Alert.alert(
            "Do you want to delete this topic?",
            null,
            [
                { text: "Cancel", style: "cancel" },
                { text: "OK", onPress: this.delete }
            ],
            { cancelable: false }
        );
    }

    render() {
        const myOptionIcon = (
            <View style={styles.myOptionIcon}>
                <FontAwesome5 name="ellipsis-h" size={15} color="gray" solid />
            </View>
        );
        let optionsForMenu = {
            "Copy URL": this._copyURL,
            "Report": this.reportPost,
        };

        if (this.props.loggedUser && this.props.topicData && this.props.loggedUser.id == this.props.topicData.author.id) {
            optionsForMenu['Edit'] = this.showEditForm;
            optionsForMenu['Hide'] = this.hideAlert;
            optionsForMenu['Delete'] = this.deleteAlert;
        }

        return (
            <>
                <OptionsMenu
                    customButton={myOptionIcon}
                    destructiveIndex={Object.keys(optionsForMenu).indexOf('Delete')}
                    options={[...Object.keys(optionsForMenu), "Cancel"]}
                    actions={[...Object.values(optionsForMenu)]}
                />
                <FlashMessage position="center" />
            </>
        );
    }
}

const styles = StyleSheet.create({
    myOptionIcon: {
        paddingHorizontal: 10,
        paddingVertical: 8
    },
});