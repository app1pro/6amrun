
import React, { useState } from "react";
import {View, Text, TouchableOpacity, StyleSheet, Alert } from "react-native";
import UserAvatar from 'react-native-user-avatar';
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import HTML from 'react-native-render-html';
import clip from "text-clipper";
import { useNavigation } from '@react-navigation/native';
import ImageView from 'react-native-image-viewing';
import Moment from 'react-moment';
import OnlineUserDot from './OnlineUserDot';
import UserLocation from "./UserLocation";
import OptionsMenu from 'react-native-option-menu';
import { getSmallThumb } from "../utils/global";

export default function UserProfilesHeader( props ) {
    const [isContentCollapsed, setContentCollapsed] = useState(false);
    const [isImageViewVisible, setModalViewer] = useState(false);
    const navigation = useNavigation();

    if (!props.currentUser) {
        return null;
    }

    _onPressMessages = () => {
        return navigation.navigate('Messages', { screen: 'Conversations' });
    }

    _onPressSendMessage = () => {
        props.setModalVisible(true);
    }

    _onPressEditProfile = () => {
        return navigation.navigate('EditProfile');
    }

    _onPressNotification = () => {
        return navigation.navigate('Notifications');
    }

    _onReportUser = () => {
        return props.openSendReport({object_id: props.currentUser.id, type: 'user'});
    }

    _onBlockUser = () => {
        Alert.alert(
            "Do you want to block this user?",
            "This user will no longer be able to: - See your posts. - Start a conversation with you.",
            [
            {
                text: "Cancel",
                style: "cancel"
            },
            { text: "Block", onPress: props.onBlockUser }
            ],
            { cancelable: false }
        );
    }

    _renderAboutContent = (_content) => {
        const maxCharacters = 400;
        let clippedHtml =  clip( _content, maxCharacters, { html: true, maxLines: 8, indicator: '... (Read more)'} )

        if (isContentCollapsed || _content.length < maxCharacters) {
            return (
                <HTML
                    source={{ html: _content }}
                    emSize={15}
                    baseFontStyle={{ fontSize: 15 }}
                    tagsStyles={{
                        p: { marginBottom: 0 }
                    }}
                    parentWrapper={View}
                />
            )
        }
        return (
            <TouchableOpacity
                onPress={() => setContentCollapsed(!isContentCollapsed)}
                keyboardShouldPersistTaps={true}
            >
                <HTML
                    source={{ html: clippedHtml }}
                    emSize={15}
                    baseFontStyle={{ fontSize: 15 }}
                    tagsStyles={{
                        p: { marginBottom: 0 }
                    }}
                    parentWrapper={View}
                />
            </TouchableOpacity>
        )
    }

    const formatNumber = (_num) => {
        return _num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    const thousandsFormat = (_num) => {
        if( _num >= 1000 ) {
            let x = Math.round(_num);
            let x_number_format = formatNumber(x);
            let x_array = x_number_format.split(',');
            let x_parts = ['k', 'm', 'b', 't'];
            let x_count_parts = x_array.length - 1;
            let x_display = x;
            x_display = x_array[0] + (parseInt(x_array[1][0]) !== 0 ? '.' + x_array[1][0] : '');
            x_display += x_parts[x_count_parts - 1];

            return x_display;
        }

        return _num;
    }

    let imagesViewer;
    let imageCurrentIndex = 0;
    if (props.currentUser.avatar !== 'undefined' && props.currentUser.avatar !== '') {
        imagesViewer = [{
            id: imageCurrentIndex,
            uri: getSmallThumb(props.currentUser.avatar),
        }];
    }

    let optionsForMenu = {
        "Report": _onReportUser,
        "Block": _onBlockUser,
    }

    return (
        <View style={styles.container}>
            <LinearGradient colors={['#ffffff', '#ffffff']} style={styles.sectionHeader}>

                {props.currentUser && (
                    <TouchableOpacity onPress={() => setModalViewer(true) } >
                        <View>
                            <UserAvatar size={80} name={props.currentUser.name} src={getSmallThumb(props.currentUser.avatar)} />
                            <OnlineUserDot shopify_cid={props.currentUser.shopify_cid} offset={'79%'} />
                        </View>
                    </TouchableOpacity>
                )}

                <View>
                    {props.currentUser.avatar !== '' ? (
                        <ImageView
                            images={imagesViewer}
                            imageIndex={imageCurrentIndex}
                            animationType={'slide'}
                            visible={isImageViewVisible}
                            onRequestClose={() => setModalViewer(false)}
                        />
                    ) : null}
                </View>
                <View>
                    <Text style={styles.sectionName}>
                        {props.currentUser.name}
                    </Text>
                </View>
            </LinearGradient>
            <View style={styles.sectionBody}>
                <View style={styles.rowProfileMeta}>
                    <Text style={ styles.profileMetaText }>
                        Joined <Moment fromNow ago element={Text} style={{fontWeight: '700'}}>{props.currentUser.created}</Moment>
                        <Text>&nbsp;|&nbsp;</Text>
                        Last Activity <Moment fromNow ago element={Text} style={{fontWeight: '700'}}>{props.currentUser.last_access}</Moment>
                    </Text>
                </View>

                <UserLocation settingsForum={props.settingsForum} currentUser={props.currentUser} />

                <View style={styles.rowActions}>
                    {props.authenUser && props.currentUser.id == props.authenUser.id ? (
                        <TouchableOpacity onPress={() => _onPressEditProfile()} style={styles.action}>
                            <Text style={styles.actionLabel}>Edit profile</Text>
                        </TouchableOpacity>
                    ) : null}

                    {props.authenUser && props.currentUser.id == props.authenUser.id ? (
                        <TouchableOpacity onPress={() => _onPressMessages()} style={styles.action}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.actionLabel}>
                                    Messages&nbsp;
                                </Text>
                                {!!props.currentUser.total_unread_msg && (
                                    <Text style={styles.totalMessages}>{props.currentUser.total_unread_msg}</Text>
                                )}
                            </View>
                        </TouchableOpacity>
                    ) : null}

                    {props.authenUser && props.currentUser.id == props.authenUser.id ? (
                        <TouchableOpacity onPress={() => _onPressNotification()} style={[styles.action, {marginRight: 5}]}>
                            <Text style={styles.actionLabel}>Notifications</Text>
                        </TouchableOpacity>
                    ) : null}

                    {props.authenUser && props.currentUser.id != props.authenUser.id ? (
                        <TouchableOpacity onPress={() => _onPressSendMessage()} style={[styles.action, {marginRight: 5}]}>
                            <Text style={styles.actionLabel}>Send a message</Text>
                        </TouchableOpacity>
                    ) : null}
                    
                    {props.authenUser && props.currentUser.id != props.authenUser.id ? (
                        <OptionsMenu
                                customButton={(
                                    <View style={[styles.action, {marginRight: 0}]}>
                                    <FontAwesome5 name="ellipsis-h" size={16} color="white" solid />
                                </View>
                                )}
                                destructiveIndex={Object.keys(optionsForMenu).indexOf('Delete')}
                                options={[...Object.keys(optionsForMenu), "Cancel"]}
                                actions={[...Object.values(optionsForMenu)]}/>
                    ) : null}
                </View>
            </View>

            <View style={styles.sectionFooter}>
                <View style={styles.rowStats}>
                    <View style={{alignItems: 'center'}}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.readableNumber}>{thousandsFormat(props.currentUser.total_posts_count)}</Text>
                            {props.currentUser.total_posts_count && props.currentUser.last_24h_posts ? (
                                <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 5}}>
                                    <FontAwesome5 name="long-arrow-alt-up" size={16} color="green" solid />
                                    <Text style={{color: 'green', fontSize: 16}}>{props.currentUser.last_24h_posts}</Text>
                                </View>
                            ) : null}
                        </View>
                        <View>
                            <Text style={styles.statLabel}>Posts</Text>
                        </View>
                    </View>
                    <View style={{alignItems: 'center'}}>
                        <Text style={styles.readableNumber}>{thousandsFormat(props.currentUser.view_count)}</Text>
                        <View>
                            <Text style={styles.statLabel}>Profile Views</Text>
                        </View>
                    </View>
                    <View style={{alignItems: 'center'}}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.readableNumber}>{thousandsFormat(props.currentUser.like_count)}</Text>
                            {props.currentUser.like_count && props.currentUser.last_24h_likes ? (
                                <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 5}}>
                                    <FontAwesome5 name="long-arrow-alt-up" size={16} color="green" solid />
                                    <Text style={{color: 'green', fontSize: 16}}>{props.currentUser.last_24h_likes}</Text>
                                </View>
                            ) : null}
                        </View>
                        <View>
                            <Text style={styles.statLabel}>Likes</Text>
                        </View>
                    </View>
                </View>
                
                {props.currentUser.signature ? (
                    <View style={styles.sectionAbout}>
                        {_renderAboutContent(props.currentUser.signature)}
                    </View>
                ) : null }
            </View>
            <View style={styles.separate} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    sectionHeader: {
        paddingTop: 20,
        paddingBottom: 15,
        alignItems: 'center',
        // backgroundColor: '#cccccc',
    },
    sectionName: {
        paddingTop: 10,
        fontWeight: 'bold',
        fontSize: 16,
    },
    sectionBody: {
        backgroundColor: '#fff',
        borderBottomWidth: 0,
        borderColor: '#d0d0d0',
    },
    rowProfileMeta: {
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 15,
    },
    profileMetaText: {
        fontSize: 15,
    },
    rowActions: {
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: '#fff',
        paddingBottom: 20,
    },
    actionLabel: {
        color: '#fff',
        fontSize: 14
    },
    action: {
        backgroundColor: '#666f77',
        borderBottomWidth: 1,
        borderBottomColor: '#666f77',
        borderRadius: 4,
        paddingVertical: 4,
        paddingHorizontal: 8,
        marginRight: 15,
    },
    totalMessages: {
        color: '#fff',
        backgroundColor: '#dc3545',
        paddingHorizontal: 5,
        paddingVertical: 2,
        fontSize: 12,
        borderRadius: 4,
    },
    sectionFooter: {
        borderBottomWidth: 1,
        borderColor: '#e0e0e0',
    },
    rowStats: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: '#fff',
        paddingTop: 10,
        paddingBottom: 10,
    },
    statLabel: {
        color: '#626a71',
        fontSize: 11,
        fontWeight: '400',
        textTransform: 'uppercase',
    },
    readableNumber: {
        fontSize: 20,
        fontWeight: '600',
    },
    sectionAbout: {
        backgroundColor: '#fff',
        paddingHorizontal: 15,
        paddingVertical: 20,
    },
    separate: {
        backgroundColor: 'transparent',
        height: 15,
    }
});
