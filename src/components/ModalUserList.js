import React, { useState, useEffect } from 'react';
import {Modal,
    Text, TextInput,
    FlatList, TouchableOpacity,
    ActivityIndicator,
    View, Alert, StyleSheet,
    SafeAreaView,
    StatusBar
} from 'react-native';
import {BASE_URL_API, MAX_FILE_SIZE} from '../../app.json';
import axios from 'axios';
import UserAvatar from 'react-native-user-avatar';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { getSmallThumb } from '../utils/global';

export default function ModalUserList( props ) {
    const [refreshing, setRefreshing] = useState(false);
    const [searchString, setSearchString] = useState('');
    const [membersList, setMembersList] = useState([]);

    useEffect(() => {
        if (props.accessedForum && props.accessToken) {
            _onRefresh();
        }
    }, [])

    const _onRefresh = () => {
        setRefreshing(true);

        const configHeaders = { headers: { Shop: props.accessedForum.shop, token: props.accessToken } };
        console.log('configHeaders', configHeaders);

        const query = 'q=' + searchString;
        axios.get(BASE_URL_API + '/users/search.json?' + query, configHeaders)
        .then(res => {
            let data = res.data;
            setRefreshing(false);

            console.log('data', data.users);

            if (data && data.users !== undefined && data.users.length) {
                setMembersList(data.users)
            } else {
                setMembersList([]);
            }
        })
        .catch(error => {
            console.log(error.response);
            Alert.alert('ERROR!', error.response.data.message);
        });
    }

    const onChange = (_searchString) => {
        setSearchString(_searchString)

        setTimeout(() => {
            _onRefresh();
        }, 100)
    }

    const _renderItem = ({item}) => (
        <TouchableOpacity style={styles.row} key={'user_' + item.id} activeOpacity={0.8} onPress={() => props.onPressMention(item)} >

            <UserAvatar size={40} name={item.name} src={getSmallThumb(item.avatar)} />
            <View style={[ styles.panelRight, { marginLeft: 10, } ]}>
                <Text style={ styles.itemName }>{item.name}</Text>
            </View>
        </TouchableOpacity>
    );

    return (
        <Modal
            animationType="slide"
            transparent={false}
            visible={props.modalVisible}
            onRequestClose={() => {
                Alert.alert('Modal has been closed.');
        }}>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView style={{ flex: 1 }}>

                <View style={styles.header}>
                    <View style={ styles.inputWrapper }>
                        <TextInput
                            style={styles.inputContent}
                            placeholder="User name, city or country..."
                            value={searchString}
                            onChangeText={_searchString => onChange(_searchString)}
                            multiline={false}
                        />
                    </View>

                    <View style={styles.panelRight}>
                        <TouchableOpacity style={{ padding: 5, paddingLeft: 10, }} onPress={() => { props.setModalVisible(false); }}>
                            <FontAwesome5 name="times" size={25} color="gray" />
                        </TouchableOpacity>
                    </View>
                </View>

                <FlatList
                    data={membersList}
                    keyExtractor={(item, index) => 'item_' + item.id}
                    renderItem={_renderItem}
                    ListEmptyComponent={
                        refreshing ? (
                            <ActivityIndicator animating={true} style={{ padding: 20 }} />
                        ) : (
                            <View style={styles.notFound}>
                                <Text style={styles.textNotFound}>Member not found!</Text>
                            </View>
                        )
                    }
                />

            </SafeAreaView>
        </Modal>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    header: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: '#ffffff',
        borderBottomWidth: 1,
        borderBottomColor: '#e9e9e9',
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        minHeight: 60,
        borderBottomWidth: 1,
        borderBottomColor: '#e9e9e9',
        paddingHorizontal: 10,
    },
    itemName: {
        fontSize: 16,
    },
    panelLeft: {
        // width: '50%',
        flexDirection: 'row',
    },
    panelRight: {
        // width: '50%',
        alignContent: 'flex-end',
        alignItems: 'flex-end',
    },
    headerActions: {
        flexDirection: 'row',
        padding: 10,
    },
    inputWrapper: {
        flex: 1,
        paddingRight: 5,
    },
    inputContent: {
        borderWidth: 0.5,
        borderColor: '#ddd',
        padding: 9,
        fontSize: 16,
        borderRadius: 5,
        backgroundColor: '#f0f5f5',
    },
    notFound: {
        alignItems: 'center',
        paddingVertical: 15,
    },
    textNotFound: {
        fontSize: 16,
        fontWeight: '500',
        color: 'gray'
    }
});
