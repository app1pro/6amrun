import React, {Component} from 'react';
import {Modal,
    Text, TextInput,
    Image, TouchableOpacity,
    KeyboardAvoidingView,
    ActivityIndicator,
    View, Alert, StyleSheet,
    SafeAreaView,
    StatusBar,
    ScrollView} from 'react-native';
import {BASE_URL_API, MAX_FILE_SIZE} from '../../app.json';
import axios from 'axios';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {RichEditor} from 'react-native-new-rich-editor';

export default class ModalEditComment extends Component {
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            dialogVisible: false,
            content: null,
            progress: 0,
        }
    }

    async submitPost () {
        this.setState({ isLoading: true });

        const form = new FormData();
        let { content } = this.state;
        if (content !== null && content !== '') {
            content = content.trim()
        }

        // let _userData = {
        //     user_id: this.props.loggedUser.id,
        //     content: content,
        // }

        form.append('content', content);
        form.append('user_id', this.props.loggedUser.id);
        form.append('post_id', this.props.comment.id);

        const configHeaders = {
            headers: {
                Shop: this.props.accessedForum.shop,
                token: this.props.accessToken
            }
        };

        axios.post(BASE_URL_API + '/posts/edit.json', form, configHeaders)
        .then(res => {
            this.setState({ isLoading: false });

            // let data = res.data;
            if (res.data.post !== undefined && !!res.data.post) {
                this.setState({
                    content: null,
                });
                this.richtext.blurContentEditor();
                this.props.editComment(res.data.post);
                this.props.setModalVisible(false)
            } else {
                Alert.alert(res.data.error);
            }
        })
        .catch(error => {
            Alert.alert(error.response.data.data.error);
            this.setState({
                isLoading: false,
            });
        });
    }

    render() {
        let renderSendBtn;

        if ( (!!this.state.content && this.state.content.trim() ) && !this.state.isLoading) {
            renderSendBtn = (
                <TouchableOpacity onPress={() => this.submitPost() }>
                    <View style={{paddingLeft: 20 }}>
                        <Ionicons name="md-send" size={25} color="#0066ff" />
                    </View>
                </TouchableOpacity>
            )
        } else {
            renderSendBtn = (
                <View style={{paddingLeft: 20 }}>
                    <Ionicons name="md-send" size={25} color="gray" />
                </View>
            )
        }

        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
            }}>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={styles.header}>
                        <View style={styles.panelLeft}>
                            <TouchableOpacity onPress={() => { this.props.setModalVisible(false); }}>
                                <View>
                                    <FontAwesome5 name="times" size={25} color="gray" />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.panelRight}>
                            <View style={styles.headerActions}>
                                {this.state.isLoading && (
                                    <View style={{ padding: 5 }}><Text>{this.state.progress}%</Text></View>
                                )}
                                <ActivityIndicator animating={this.state.isLoading} />
                                {renderSendBtn}
                            </View>
                        </View>
                    </View>

                    <View style={styles.container}>
                        <ScrollView style={{ flex: 1, minHeight: 300, }} keyboardDismissMode={'none'} keyboardShouldPersistTaps="always">
                            {/* <TextInput
                                style={styles.inputContent}
                                placeholder="What's on your mind?"
                                value={this.state.content}
                                onChangeText={(content) => this.setState({ content })}
                                multiline={true}
                            /> */}

                            <View style={styles.inputWrapper}>
                                <RichEditor
                                    ref={(r) => this.richtext = r}
                                    placeholder={"What's on your mind?"}
                                    editorStyle={{ backgroundColor: '#ffffff' }}
                                    initialContentHTML={this.props.comment ? this.props.comment.content : null}
                                    onChange={content => this.setState({ content })}
                                />
                            </View>

                            {this.props.comment && this.props.comment.images && this.props.comment.images.length ? (
                            <View style={styles.mediaBox}>
                                <ScrollView horizontal={true}>
                                    <View style={styles.scrollImagesBox}>
                                    {this.props.comment.images.map((_item, _index) => (
                                        <View style={styles.imageBox} key={'image_' + _index}>
                                            <Image source={{ uri: _item.url}} style={{width: 60, height: 60}} />
                                            {/* <View style={styles.imgClose}>
                                                <TouchableOpacity onPress={() => this.removeImage(_item.localIdentifier) }>
                                                    <View>
                                                        <Ionicons name="md-close" size={18} color="white" />
                                                    </View>
                                                </TouchableOpacity>
                                            </View> */}
                                        </View>
                                    ))}
                                    </View>
                                </ScrollView>
                            </View>
                            ) : null}

                        </ScrollView>

                    </View>
                </SafeAreaView>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    header: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 15,
        backgroundColor: '#ffffff',
        borderBottomWidth: 1,
        borderBottomColor: '#e9e9e9',
    },
    panelLeft: {
        width: '50%',
        flexDirection: 'row',
    },
    panelRight: {
        // flex: 1,
        width: '50%',
        alignContent: 'flex-end',
        alignItems: 'flex-end',
    },
    headerActions: {
        flexDirection: 'row',
    },
    inputWrapper: {
        flex: 1,
        marginBottom: 25,
        backgroundColor: '#ff0000',
    },
    inputContent: {
        // flex: 1,
        // borderWidth: 0,
        // borderColor: '#99c2ff',
        fontSize: 16,
        // backgroundColor: '#f0f5f5',
    },
    rowFooter: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      backgroundColor: '#fff',
      borderTopWidth: 1,
      borderTopColor: '#d0d0d0',
    },
    mediaBox: {
        paddingVertical: 15,
        marginHorizontal: -5,
        backgroundColor:"#f9f9f9",
    },
    imageBox: {
        marginHorizontal: 5,
    },
    scrollImagesBox: {
        flexDirection: 'row',
    },
    button: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
        marginVertical: 8,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 10,
    },
    buttonText: {
        fontSize: 18,
        color:'#ffffff',
        textAlign: 'center',
    },
    buttonCancel: {
        fontSize: 18,
        color: '#666',
    },
    // imgClose: {
    //     position: 'absolute',
    //     right: 3,
    //     top: 3,
    //     width: 20,
    //     height: 20,
    //     alignContent: 'center',
    //     alignItems: 'center',
    //     borderRadius: 10,
    //     borderWidth: 1,
    //     borderColor: 'white',
    //     backgroundColor: 'rgba(0, 0, 0, .5)',
    // }
});
