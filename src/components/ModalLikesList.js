import React, { useRef, useState, forwardRef, useCallback } from 'react';
import { Text, View, FlatList, StyleSheet, ScrollView, SafeAreaView, TouchableOpacity } from 'react-native';
import UserAvatar from "react-native-user-avatar";
import { Modalize } from 'react-native-modalize';
import { useCombinedRefs } from '../utils/use-combined-refs';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Moment from 'react-moment';
import { getSmallThumb } from '../utils/global';

export const ModalLikesList = forwardRef((props, ref) => {
    const modalizeRef = useRef(null);
    const combinedRef = useCombinedRefs(ref, modalizeRef);

    return (
        <Modalize
          ref={combinedRef}
          scrollViewProps={{ showsVerticalScrollIndicator: false }}
          adjustToContentHeight={true}
        >
            <View style={styles.container}>
                <View style={{ marginBottom: 20, marginHorizontal: 15, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={{ fontSize: 18, fontWeight: '600' }}>Who like this post?</Text>

                    <TouchableOpacity style={{ padding: 5, }} onPress={() => combinedRef.current?.close() }>
                        <FontAwesome5 name="times" size={18} color="gray" />
                    </TouchableOpacity>
                </View>
                <SafeAreaView style={{flex: 1}}>
                    <ScrollView>
                    {props.data.map((item, index) => (
                        <View style={styles.row} key={'like_' + index}>
                            <View style={styles.panelLeft}>
                                <UserAvatar size={40} name={item.user.name} src={getSmallThumb(item.user.avatar)} />
                            </View>
                            <View style={styles.panelRight}>
                                <View style={ styles.itemDetails }>
                                    <Text style={ styles.itemName }>{item.user.name}</Text>
                                </View>

                                <View style={ styles.rightWidget }>
                                    <View>
                                        {item.created && (
                                            <Moment fromNow ago element={Text} style={[styles.itemDescriptions]}>{item.created}</Moment>
                                        )}
                                    </View>
                                </View>
                            </View>
                        </View>
                    ))}
                    </ScrollView>
                </SafeAreaView>
            </View>
        </Modalize>
    );
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        marginBottom: 20,
        minHeight: 350
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#e9e9e9',
    },
    panelLeft: {
        height: 50,
        width: 50,
        paddingTop: 5,
        paddingLeft: 10,
    },
    panelRight: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 10,
    },
    itemDetails: {
        paddingVertical: 13,
    },
    rightWidget: {
        position: 'absolute',
        top: 0,
        right: 10,
        paddingTop: 10,
        zIndex: 99,
        alignItems: 'flex-end',
    },
});
