import React from 'react'
import { Button, Alert } from 'react-native'
import { LoginManager, AccessToken } from 'react-native-fbsdk'
// import EntypoIcon from 'react-native-vector-icons/Entypo'


export default class FbLoginButton extends React.Component {
    constructor(props) {
        super(props);
    }

    handleFacebookLogin = () => {
        // Attempt a login using the Facebook login dialog,
        // asking for default permissions.
        LoginManager.logInWithPermissions(['public_profile']).then(result =>
            {
                if (result.isCancelled) {
                    console.log('Login was cancelled');
                } else {
                    console.log('Login was successful with permissions: '
                    + result.grantedPermissions.toString());

                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            const { accessToken } = data;
                            this.props.onPress(accessToken);

                            console.log(data.accessToken.toString());
                            console.log('result', result);
                        }
                    )
                }
            },
            function(error) {
                console.log('Login failed with error: ' + error);
            }
        )
    }

    render () {
        return (
            <Button
                onPress={this.handleFacebookLogin}
                title="Login with facebook"
                color="#4267B2"
            />
        )
    }
}