import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

export default function OnlineUserDot (props) {
    const onlineUsers = useSelector(state => state.onlineUsers );
    // console.log('onlineUsers', onlineUsers);
    
    const customStyles = {
        top: props.offset ? props.offset : 29,
        left: props.offset ? props.offset : 29,
        height: props.size ? props.size : 11,
        width: props.size ? props.size : 11,
    }

    if (props.shopify_cid && typeof onlineUsers !== 'undefined' && onlineUsers.length && onlineUsers.indexOf(props.shopify_cid.toString()) > -1) {
        return <View style={[styles.dot, customStyles]}></View>
    }

    return null;
}

const styles = StyleSheet.create({
    dot: {
        height: 11,
        width: 11,
        borderWidth: .5,
        borderColor: 'white',
        backgroundColor: '#00d84d',
        borderRadius: 10,
        position: 'absolute',
        top: 29,
        left: 29,
        zIndex: 99,
    },
});
