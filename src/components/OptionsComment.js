import React, { Component } from "react";
import { View, Alert, StyleSheet } from "react-native";
import {BASE_URL_API} from '../../app.json';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import OptionsMenu from 'react-native-option-menu';
import ModalEditComment from './ModalEditComment';
import axios from 'axios';

export default class OptionsComment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            comment: {},
        };
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    showEditModal = () => {
        this.setState({
            comment: this.props,
        });

        this.setModalVisible(true);
    }

    reportPost = () => {
        return this.props.openSendReport({object_id: this.props.id, type: 'post'});
    }

    hide = (_item_id) => {
        const configHeaders = { headers: { Shop: this.props.accessedForum.shop, token: this.props.accessToken } };
        axios.post(BASE_URL_API + '/posts/hide.json', {post_id: _item_id, user_id: this.props.loggedUser.id}, configHeaders)
        .then(res => {
            let { data } = res;

            if (data.success) {
                this.props.deleteComment(_item_id);
            }
        })
        .catch(error => {
            console.log('Hide comment error', error.response)
        })
    }

    hideAlert = () => {
        Alert.alert(
            "Do you want to hide this comment?",
            null,
            [
            {
                text: "Cancel",
                style: "cancel"
            },
            { text: "OK", onPress: () => this.hide(this.props.id) }
            ],
            { cancelable: false }
        );
    }

    delete = (_item_id) => {
        const configHeaders = { headers: { Shop: this.props.accessedForum.shop, token: this.props.accessToken } };
        axios.post(BASE_URL_API + '/posts/delete.json', {post_id: _item_id, user_id: this.props.loggedUser.id}, configHeaders)
        .then(res => {
            let { data } = res;

            if (data.success) {
                this.props.deleteComment(_item_id);
            }
        })
        .catch(error => {
            console.log('Delete post error', error.response)
        })
    }

    deleteAlert = () => {
        Alert.alert(
            "Do you want to delete this comment?",
            null,
            [
                { text: "Cancel", style: "cancel" },
                { text: "OK", onPress: () => this.delete(this.props.id) }
            ],
            { cancelable: false }
        );
    }

    render() {
        const myOptionIcon = (
            <View style={styles.myOptionIcon}>
                <FontAwesome5 name="ellipsis-h" size={15} color="gray" solid />
            </View>
        );
        let optionsForMenu = {
            "Report": this.reportPost
        };

        if (this.props.loggedUser && this.props.loggedUser.id == this.props.author.id) {
            optionsForMenu['Edit'] = this.showEditModal;
            optionsForMenu['Hide'] = this.hideAlert;
            optionsForMenu['Delete'] = this.deleteAlert;
        }

        return (
            <View>
                <OptionsMenu
                    customButton={myOptionIcon}
                    destructiveIndex={Object.keys(optionsForMenu).indexOf('Delete')}
                    options={[...Object.keys(optionsForMenu), "Cancel"]}
                    actions={[...Object.values(optionsForMenu)]}
                />

                <ModalEditComment
                    modalVisible={this.state.modalVisible}
                    setModalVisible={_state => this.setModalVisible(_state)}
                    editComment={(_data) => this.props.editComment(_data)}
                    comment={this.state.comment || {}}
                    accessedForum={this.props.accessedForum}
                    accessToken={this.props.accessToken}
                    loggedUser={this.props.loggedUser}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    myOptionIcon: {
        paddingHorizontal: 10,
        paddingVertical: 8
    },
});