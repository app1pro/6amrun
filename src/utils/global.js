/** GLOBAL FUNCTION HERE */

export const getSmallThumb = (filename) =>  filename?.replace(/(\.[\w\d_-]+)$/i, '_s$1');
export const getMediumThumb = (filename) => filename?.replace(/(\.[\w\d_-]+)$/i, '_m$1');
export const getHighThumb = (filename) => filename?.replace(/(\.[\w\d_-]+)$/i, '_h$1');

