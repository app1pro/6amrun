import { combineReducers } from 'redux';
import { createStore } from 'redux';
import { 
    emitJoin,
    onUserLogin, 
    socketClose,
    checkConnect, 
    onUserJoined,
    onUserLeft,
    onNewNotification,
    onReconnect,
    onReconnectError
} from './Realtime';

// A very simple reducer
function onlineUsers(state = [], action) {
    if (typeof state === 'undefined') {
      return [];
    }

    switch (action.type) {
        case 'EMIT_USER':
            checkConnect();
            emitJoin(action.user_id.toString(), action.group_id.toString());
            return state;
        case 'ADD_USER':
            return state.concat([action.data]);
        case 'DISCONECT':
            socketClose();
            return [];
        case 'UPDATE_USERS':
            return action.data;
        default:
            return state;
    }
}

function setCurrentUser(state = null, action) {
    if (typeof state === 'undefined') {
      return null;
    }
  
    switch (action.type) {
        case 'ADD_CURRENT_USER':
            return action.data;
        case 'DELETE_CURRENT_USER':
            return null;
        default:
            return state;
    }
}

function unreadNotifications(state = null, action) {
    if (typeof state === 'undefined') {
      return 0;
    }
  
    switch (action.type) {
        case 'UPDATE_NOTIFICATION':
            return action.data;
        case 'DELETE_NOTIFICATION':
            return 0;
        default:
            return 0;
    }
}

const onlineReducer = combineReducers({
    onlineUsers,
    currentUser: setCurrentUser,
    unreadNotifications
})

// A very simple store
const store = createStore(onlineReducer);

onUserLogin(data => {
    store.dispatch({
        type: 'UPDATE_USERS',
        data: data.listUsers,
    })
})

onUserJoined(data => {
    store.dispatch({
        type: 'UPDATE_USERS',
        data: data.listUsers,
    })
})

onUserLeft(data => {
    store.dispatch({
        type: 'UPDATE_USERS',
        data: data.listUsers,
    })
})

onNewNotification(data => {
    console.log('onNewNotification', data)
})

onReconnect(() => {
    const { currentUser } = store.getState();
    store.dispatch({
        type: 'EMIT_USER',
        user_id: currentUser.shopify_cid.toString(),
        group_id: currentUser.id_shop.toString(),
    })
})

onReconnectError()

export default store;

