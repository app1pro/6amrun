/**
* Open new socket and handle realtime
*/

import io from 'socket.io-client';
import {WEBSOCKET} from '../../app.json';

const socket = io(WEBSOCKET);

export function checkConnect() {
    console.log('checkConnect', socket.disconnected);
    if (socket.disconnected) {
        socket.open();
    }
}

export function emitJoin(customer_id, id_shop) {
    socket.emit('add_user', {id: customer_id, group: id_shop});
}

export function onUserLogin(cb) {
    socket.on('login', (data) => cb(data));
}

export function onUserJoined(cb) {
    socket.on('user_joined', (data) => cb(data));
}

export function onUserLeft(cb) {
    socket.on('user_left', (data) => cb(data));
}

export function onNewNotification(cb) {
    socket.on('new_notification', (data) => cb(data));
}

export function onReconnect(cb) {
    socket.on('reconnect', () => cb() );
}

export function onReconnectError() {
    socket.on('reconnect_error', () => console.log('attempt to reconnect has failed') );
}

export function emitCall(key, message, room) {
    socket.emit(key, message, room);
}

export function onReceivedCall(key, cb) {
    socket.on(key, (data) => cb(data));
}

export function socketClose(cb) {
    socket.disconnect(cb);
}
