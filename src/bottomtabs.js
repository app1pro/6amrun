
import * as React from 'react';
import MessagesStack from './screens/MessagesStack';
import CategoriesStack from './screens/CategoriesStack';
import StoriesStack from './screens/StoriesStack';
import SettingsStack from './screens/SettingsStack';
import ForumInfoStack from './screens/ForumInfoStack';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { AuthContext } from './utils/authContext';

// const AuthContext = React.createContext();
const Tab = createBottomTabNavigator();

function AppTabStack() {
    const { signOut } = React.useContext(AuthContext);

    return (
        <Tab.Navigator>
            <Tab.Screen name="Stories" component={StoriesStack}
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color, size }) => (
                        <SimpleLineIcons name="home" color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen name="Categories" component={CategoriesStack}
                options={{
                    tabBarLabel: 'Categories',
                    tabBarIcon: ({ color, size }) => (
                        <SimpleLineIcons name="list" color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen name="Info" component={ForumInfoStack}
                options={{
                    tabBarLabel: 'Info',
                    tabBarIcon: ({ color, size }) => (
                        <SimpleLineIcons name="info" color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen name="Messages" component={MessagesStack}
                options={{
                    tabBarIcon: ({ color, size }) => (
                        <SimpleLineIcons name="bubbles" color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen name="Settings" component={SettingsStack}
                options={{
                    tabBarIcon: ({ color, size }) => (
                        <SimpleLineIcons name="settings" color={color} size={size} />
                    ),
                }}
            />
        </Tab.Navigator>
    );
}

export default AppTabStack;